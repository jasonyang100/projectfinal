﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class RestroomPuzzleScript : MonoBehaviour
{

    [SerializeField] Camera RestroomCamera;
    [SerializeField] Camera MainCamera;
    [SerializeField] GameObject[] LightGameObjects;
    [SerializeField] GameObject Player;
    [SerializeField] GameObject Portal;
    [SerializeField] GameObject ShiningOrb;
    [SerializeField] Renderer PortalRenderer;
    Material PortalMaterial;
    Collider PortalCollider;
    CharacterController m_characterController;
    [SerializeField] Light[] Lights;
    [SerializeField] Renderer GameMainObjectRenderer;
    [SerializeField] Material GameMainObjectMaterial;
    [SerializeField] Light GameMainObjectLight;
    [SerializeField] Canvas MiniGameCanvas;
    [SerializeField] GameObject MiniGameTimerInteractPanel;
    [SerializeField] GameObject[] TimeInfoLightGroup;
    //[SerializeField] GameObject MiniGameTimeBarPanel;
    [SerializeField] Text TargetNameText;
    //[SerializeField] Image TimeBarImage;
    [SerializeField] bool isGameOn;
    [SerializeField] float PlayerSelectingTime = 15;
    [SerializeField] bool isClear;
    int GameFlag;
    bool isAnswerCorrect;
    bool isSelecting;
    Dictionary<string, string> dict_orbname = new Dictionary<string, string>
    {
        { "GreenOrb", "綠色寶珠"},
        { "RedOrb", "紅色寶珠"},
        { "BlueOrb", "藍色寶珠"}
    };

    [SerializeField] int GamePhaseNow;

    [SerializeField] bool istest;
    [SerializeField] bool[] PlayersAnswer;
    [SerializeField] bool[][] TheAnswer;
    GameObject CorrectVFX;
    [SerializeField] GameObject FailureVFX;
    [SerializeField] Material M;

    delegate void del();
    





    // Start is called before the first frame update
    void Start()
    {
        PlayersAnswer = new bool[] {false, false , false };
        TheAnswer = new bool[][]{new bool[]{ false,true,false},new bool[]{ true, false, true },new bool[]{ true, true, true } };

        GamePhaseNow = -1;
        GameMainObjectMaterial = GameMainObjectRenderer.material;
        m_characterController = Player.GetComponent<CharacterController>();
        PortalCollider = Portal.GetComponent<Collider>();
        PortalMaterial = PortalRenderer.material;
        PortalMaterial.SetInt("_IsActive", 0); ;

        MainCamera = Camera.main;
        RestroomCamera.enabled = false;
        ResetGameMainObjectColor();
        FailureVFX.SetActive(false);
        MiniGameCanvas.enabled = false;
        //MiniGameTimeBarPanel.SetActive(false);
        ResetTimeInfoLightGroup(false);




    }

    // Update is called once per frame
    void Update()
    {
        GameOn();

        if (istest)
        {
           
        }
    }


    void GameOn()
    {
        if (isGameOn)
        {
            if (GamePhaseNow == 0)
            {
                isGameOn = false;
                StartingPhase();
                
            }

            if(GamePhaseNow == 1)
            {
                isGameOn = false;
                GamePhase1();
                
            }

            if (GamePhaseNow == 2)
            {
                isGameOn = false;
                GamePhase2();

            }
            if (GamePhaseNow == 3)
            {
                isGameOn = false;
                StartCoroutine(GameClearCameraMove());
                ClearPhase();
            }

            if (GamePhaseNow == 9)
            {
                isGameOn = false;
                GamePhase9();
            }
        }

    }
    void StartingPhase()
    {
        MiniGameCanvas.enabled = true;
        GamePhaseNow = 0;
               
        StartCoroutine(ChangingRestRoomCameraPosition(6f));
        //StartCoroutine(StopPlayerMove());
        ResetGameMainObjectColor();
        StartCoroutine(DisplayFunction());      
       
    }

    void ClearPhase()
    {
        StartCoroutine(GameClearCameraMove());
        StartCoroutine(PortalOpeningPhase());
        // camera face to portal

        ClearSetGameMainObjectColor();// reset all minigame object
        TargetNameText.text = " ";
        FailureVFX.transform.parent = this.transform;
        for (int i = 0; i < Lights.Length; i++)
        {
            Lights[i].enabled = false;
        }
        ShiningOrb.SetActive(false);
        isClear = true;
        MiniGameCanvas.enabled = false;
        RestLightObjectMaterial(true);
    }
    


    void GamePhase1()
    {

        ShiningOrb.SetActive(false);
        RestLightObjectMaterial(false);
        //MainCamera.enabled = true;
        //RestroomCamera.enabled = false;
        GameMainObjectLight.intensity = 40;
        GameMainObjectLight.color = new Color(0.65f, 0.96f, 0.43f, 1f);
        GameMainObjectMaterial.SetColor("_EmissionColor", Color.green);
        for(int i = 0; i < Lights.Length; i++)
        {
            Lights[i].enabled = false;
        }

        //Mainobject show example
        StartCoroutine(GameProcessing(0));
        
        //first set 

        // wait player assign an answer
    }

    void GamePhase2()
    {
        MainCamera.enabled = true;
        RestroomCamera.enabled = false;
        RestLightObjectMaterial(false);
        GameMainObjectLight.intensity = 30;
        StartCoroutine(ChangingGameMainObjcet2Color(Lights[0],Lights[2],Color.red,Color.blue,2));
        //Mainobject show example
        for (int i = 0; i < Lights.Length; i++)
        {
            Lights[i].enabled = false;
        }
        StartCoroutine(GameProcessing(1));//second set 

    }

    void GamePhase3()
    {
        GameMainObjectLight.intensity = 30;
        GameMainObjectLight.color = Color.white;
        GameMainObjectMaterial.SetColor("_EmissionColor", Color.white);
        //Mainobject show example
        StartCoroutine(GameProcessing(2));//first set 
    }

    void GamePhase9()
    {
        GameMainObjectLight.intensity = 30;
        GameMainObjectLight.color = new Color(1f,0.8f,0.66f,1f) ;
        RestLightObjectMaterial(true);
        GameMainObjectMaterial.SetColor("_EmissionColor", Color.grey);
        GamePhaseNow = -1;
        for (int i = 0; i < Lights.Length; i++)
        {
            Lights[i].enabled = false;
        }
        //teleportPlayerToPosition(new Vector3(-88f,51f,435f));// -88 ,51,435
        FailureVFX.transform.position = Player.transform.position+ Vector3.up*6.5f;
        FailureVFX.transform.parent = Player.transform;
        StartCoroutine(DeployFailureVFX());
        MainCamera.enabled = true;
        RestroomCamera.enabled = false;
        ShiningOrb.SetActive(true);
        TargetNameText.text = " ";
        MiniGameCanvas.enabled = false;

    }







    
    

    IEnumerator ChangingGameMainObjcetColor(Light _light,Color _color)
    {
        do
        {
            GameMainObjectLight.intensity = Mathf.Lerp(GameMainObjectLight.intensity, _light.intensity, 0.1f);
            GameMainObjectLight.color = Color.Lerp(GameMainObjectLight.color, _light.color, 0.1f);
            GameMainObjectMaterial.SetColor("_EmissionColor", Color.Lerp(GameMainObjectMaterial.GetColor("_EmissionColor"), _color, 0.1f));


            yield return null;

        } while (_light.intensity- GameMainObjectLight.intensity>2f);

        
    }

    IEnumerator ChangingGameMainObjcet2Color(Light _light1, Light _light2,Color _color1, Color _color2,float Timer)
    {
        float _Timer = Timer;
        do
        {
            if (_Timer > Timer / 2)
            {
                GameMainObjectLight.intensity = Mathf.Lerp(GameMainObjectLight.intensity, _light1.intensity, 0.1f);
                GameMainObjectLight.color = Color.Lerp(GameMainObjectLight.color, _light1.color, 0.1f);
                GameMainObjectMaterial.SetColor("_EmissionColor", Color.Lerp(GameMainObjectMaterial.GetColor("_EmissionColor"), _color1, 0.1f));
            }

            else
            {
                GameMainObjectLight.intensity = Mathf.Lerp(GameMainObjectLight.intensity, _light2.intensity, 0.1f);
                GameMainObjectLight.color = Color.Lerp(GameMainObjectLight.color, _light2.color, 0.1f);
                GameMainObjectMaterial.SetColor("_EmissionColor", Color.Lerp(GameMainObjectMaterial.GetColor("_EmissionColor"), _color2, 0.1f));

            }


            _Timer -= Time.deltaTime;
            yield return null;

        } while (_Timer>0);


    }




    void ResetGameMainObjectColor()
    {
        GameMainObjectLight.intensity = 0f;
        GameMainObjectLight.color = Color.grey;
        GameMainObjectMaterial.SetColor("_EmissionColor", Color.white);
    }

    void ClearSetGameMainObjectColor()
    {
        GameMainObjectLight.intensity = 30f;
        GameMainObjectLight.color = new Color(1f, 0.88f, 0.67f, 1f);
        GameMainObjectMaterial.SetColor("_EmissionColor", new Color(1.88f, 2f, 0.77f, 1f));
    }



    IEnumerator StopPlayerMove()
    {
        m_characterController.enabled = false;
        yield return new WaitForSeconds(5);
        m_characterController.enabled = true;

    }

    IEnumerator IncreseSelfColor(Light _light)
    {
        _light.enabled = true;
        var _color = _light.color;// temp
        var _intensity = _light.intensity;// temp
        _light.color = Color.white;
        _light.intensity = 0f;
        do
        {
            _light.color = Color.Lerp(_light.color, _color, 0.1f);
            _light.intensity = Mathf.Lerp(_light.intensity, _intensity, 0.1f);
            yield return null;
        } while (_intensity-_light.intensity>0.1f);
    }

    IEnumerator ChangingRestRoomCameraPosition(float _time)
    {
        MainCamera.enabled = false;
        RestroomCamera.enabled = true;
        float _timer = 0;
        Vector3 _originPosition = RestroomCamera.transform.position;
        RestroomCamera.transform.position = MainCamera.transform.position;
        
        do
        {
            _timer+=Time.deltaTime;
            RestroomCamera.transform.position = Vector3.Lerp(RestroomCamera.transform.position, _originPosition, 2.2f*Time.deltaTime);
            yield return null;
        } while (_timer< _time);

        //while (_timer > 0)//lerp back
        //{
        //    _timer -= Time.deltaTime;
        //    RestroomCamera.transform.position = Vector3.Lerp(RestroomCamera.transform.position, MainCamera.transform.position, 0.1f);
        //    yield return null;
        //}



        MainCamera.enabled = true;
        RestroomCamera.enabled = false;
        RestroomCamera.transform.position = _originPosition;


    }


    IEnumerator DisplayFunction()// automatic showing
    {
        yield return new WaitForSeconds(1f);
        for (int i = 0; i < Lights.Length; i++)
        {
            Lights[i].enabled = false;
        }
        StartCoroutine(IncreseSelfColor(Lights[0]));
        StartCoroutine(ChangingGameMainObjcetColor(Lights[0], Lights[0].color));
        yield return new WaitForSeconds(2f);
        ResetGameMainObjectColor();
        StartCoroutine(IncreseSelfColor(Lights[1]));
        StartCoroutine(ChangingGameMainObjcetColor(Lights[1], Color.green));
        yield return new WaitForSeconds(2f);
        ResetGameMainObjectColor();
        StartCoroutine(IncreseSelfColor(Lights[2]));
        StartCoroutine(ChangingGameMainObjcetColor(Lights[2], Color.blue));
        yield return new WaitForSeconds(2f);
        ResetGameMainObjectColor();
        yield return StartCoroutine(PutToNextPhase());
    }

    IEnumerator  GameProcessing(int PhaseNumber)
    {
        //yield return StartCoroutine(ChangingRestRoomCameraPosition(1f)); 
        isSelecting = true;
        MainCamera.enabled = true;
        RestroomCamera.enabled = false;
        StartCoroutine(PlayerSelectingPhase());
        //MiniGameTimeBarPanel.SetActive(true);
        TimeInfoLightFunction();
        yield return new WaitForSeconds(PlayerSelectingTime);
        //MiniGameTimeBarPanel.SetActive(false);
        isSelecting = false;

        isAnswerCorrect=PhaseGameSet(TheAnswer[PhaseNumber]);
        
        if (!isAnswerCorrect)
        {
            isSelecting = false;
            GamePhaseNow = 9;//failure;
            isGameOn = true;
        }
        else
        {
            yield return StartCoroutine(PutToNextPhase());
        } 
        
    }

    IEnumerator PlayerSelectingPhase()
    {
        while (isSelecting)
        {
            int index=-1;
            for (int i=0;i< LightGameObjects.Length;i++) 
            {
                //if (Vector3.Distance(LightGameObjects[i].transform.position,Player.transform.position) < 3)
                if (Vector3.Distance(new Vector3(LightGameObjects[i].transform.position.x, 0f, LightGameObjects[i].transform.position.z)
                                   , new Vector3(Player.transform.position.x, 0f, Player.transform.position.z) )< 3)
                    {                                    
                    Lights[i].enabled = true;
                    StartCoroutine(PopSeletingInteractPanel(LightGameObjects[i]));                    
                    StartCoroutine(CatchLightBool(i));//if getkeydown
                    index = i;
                    break ;
                    
                }
                else if(PlayersAnswer[i]!=true)
                {
                    Lights[i].enabled = false;
                    index = -1;

                }
                
            }
            
            
            yield return null;
        }
    }
    IEnumerator CatchLightBool(int number)
    {

        //for (int i = 0; i < Lights.Length; i++)
        //{
        //    PlayersAnswer[i] = Lights[i].enabled;
        //}
        //yield return null;
        
            if (Input.GetKeyDown(KeyCode.C))
            {
                PlayersAnswer[number] = !PlayersAnswer[number];
            AudioManager.instance.Play("7_UI_MagicSelectedButton");
                Lights[number].enabled = PlayersAnswer[number];
            StartCoroutine(ActinngLightObject(LightGameObjects[number], PlayersAnswer[number]));

            }
            yield return null;
        
        


    }


    bool PhaseGameSet(bool[] i_bool)
    {
       // Debug.Log(i_bool);
      //  Debug.Log(PlayersAnswer);
        if (i_bool[0] == (PlayersAnswer[0]) && i_bool[1] == (PlayersAnswer[1]) && i_bool[2] == (PlayersAnswer[2]))
        {
            for (int i = 0; i < PlayersAnswer.Length; i++)
            {
                PlayersAnswer[i] = false;
            }//reset playersanswer
            
            return true;
        }
        else
        {
            for (int i = 0; i < PlayersAnswer.Length; i++)
            {
                PlayersAnswer[i] = false;//reset playersanswer
            }
            return false;
        }

    }

    IEnumerator PutToNextPhase()
    {
        yield return new WaitForSeconds(0.3f);       
        GamePhaseNow++;
        isGameOn = true;
    }
    void teleportPlayerToPosition(Vector3 position)
    {
        CharacterController c = Player.GetComponent<CharacterController>();
        c.enabled = false;
        c.transform.position = position;
        c.enabled = true;

    }

    IEnumerator DeployFailureVFX()
    {
        FailureVFX.SetActive(true);
        yield return new WaitForSeconds(4);
        FailureVFX.SetActive(false);
    }

    IEnumerator GameClearCameraMove()
    {
        float _Timer = 5f;
        RestroomCamera.enabled = true;
        MainCamera.enabled = false;
        Vector3 DirToPortal = Portal.transform.position-RestroomCamera.transform.position;
        DirToPortal.Normalize();


        do
        {
            RestroomCamera.transform.forward = Vector3.Lerp(RestroomCamera.transform.forward
                , DirToPortal, 0.1f);
            _Timer -= Time.deltaTime;
            yield return null;
        } while (_Timer>0);
        MainCamera.transform.forward = DirToPortal;
        RestroomCamera.enabled = false;
        MainCamera.enabled = true;
    }


    IEnumerator PopGameStartInteractPanel(GameObject _target)
    {
        while (Vector3.Distance(new Vector3(_target.transform.position.x,0f, _target.transform.position.z)
            , new Vector3(Player.transform.position.x,0f, Player.transform.position.z)) < 5)
        {
            MiniGameCanvas.enabled = true;
            MiniGameTimerInteractPanel.SetActive(true);

            if (Input.GetKeyDown(KeyCode.C))
            {
                isGameOn = true;
                GamePhaseNow = 0;
                MiniGameTimerInteractPanel.SetActive(false);
                yield break;
            }
            yield return null;
        }
        MiniGameTimerInteractPanel.SetActive(false);
        MiniGameCanvas.enabled = false;

    }

    IEnumerator PopSeletingInteractPanel(GameObject _target)
    {
        while (Vector3.Distance(new Vector3(_target.transform.position.x, 0f, _target.transform.position.z)
            , new Vector3(Player.transform.position.x, 0f, Player.transform.position.z)) < 5)
        {
            MiniGameTimerInteractPanel.SetActive(true);
            TargetNameText.text = dict_orbname[_target.name];
            
            yield return null;
        }
        MiniGameTimerInteractPanel.SetActive(false);

    }
    IEnumerator PortalOpeningPhase()
    {

        PortalMaterial.SetFloat("_LightIntensity", 27);
        PortalMaterial.SetInt("_IsActive", 1);
        AudioManager.instance.Play("48_Portal");
        while (PortalMaterial.GetFloat("_LightIntensity")-4f>0.05f)
        {
            PortalMaterial.SetFloat("_LightIntensity", Mathf.Lerp(PortalMaterial.GetFloat("_LightIntensity"),4,1f*Time.deltaTime));
            yield return null;
        }
        PortalCollider.isTrigger = true;
    }

    //IEnumerator TimeBarColorLerp()
    //{
    //    while (TimeBarImage.fillAmount > 0)
    //    {
    //        if (TimeBarImage.fillAmount > 0.66)
    //        {
    //            TimeBarImage.color = Color.Lerp(TimeBarImage.color, Color.blue, 0.1f);
    //        }
    //        else if (TimeBarImage.fillAmount > 0.33)
    //        {
    //            TimeBarImage.color = Color.Lerp(TimeBarImage.color, Color.yellow, 0.1f);
    //        }
    //        else
    //        {
    //            TimeBarImage.color = Color.Lerp(TimeBarImage.color, Color.red, 0.1f);
    //        }
    //        yield return null;
    //    }

    //}

    IEnumerator TimeInfoLightDec(float Timer)
    {
        float _Timer = Timer;
        _Timer = 14;
        while (_Timer > 0)
        {
            if (_Timer < Timer * 0.8F)
            {
                TimeInfoLightGroup[0].SetActive(false);
                if (_Timer < Timer * 0.6F)
                {
                    TimeInfoLightGroup[1].SetActive(false);
                    if (_Timer < Timer * 0.4F)
                    {
                        TimeInfoLightGroup[2].SetActive(false);
                        if (_Timer < Timer * 0.2F)
                        {
                            TimeInfoLightGroup[3].SetActive(false);
                        }
                    }
                }
            }


            _Timer -= Time.deltaTime;
            yield return null;
        }
        foreach (GameObject go in TimeInfoLightGroup)
        {
            go.SetActive(false);
        }
    }
    

    void TimeInfoLightFunction()
    {
        foreach(GameObject go in TimeInfoLightGroup)
        {
            go.SetActive(true);
        }
        //MiniGameTimeBarPanel.SetActive(true);
        
        StartCoroutine(TimeInfoLightDec(PlayerSelectingTime));
        
    }

    IEnumerator TimeInfoLightsIdle(bool _IsActive)
    {
        foreach (GameObject go in TimeInfoLightGroup)
        {
            go.SetActive(false);
        }

        while (_IsActive)
        {
            foreach(GameObject go in TimeInfoLightGroup)
            {
                go.SetActive(!go.activeSelf);
                yield return new WaitForSeconds(0.1f);
            }


            yield return null;
        }
    }
    IEnumerator ActinngLightObject(GameObject _Go,bool _b)
    {
        Material m = M=_Go.GetComponent<Renderer>().material;
        if (_b)
        {
            m.SetColor("_BaseColor",Color.white);

        }
        else
        {
            m.SetColor("_BaseColor", Color.grey);
        }
        yield return null;


    }

    void RestLightObjectMaterial(bool _b)
    {
        Material m;
        foreach (GameObject go in LightGameObjects)
        {
            m= go.GetComponent<Renderer>().material;
            if (_b)
            {
                m.SetColor("_BaseColor", Color.white);

            }
            else
            {
                m.SetColor("_BaseColor", Color.grey);
            }

        }
    }



    private void OnTriggerEnter(Collider other)
    {
       
        if (other.CompareTag("Player") && GamePhaseNow == -1 && !isClear)
        {
            StartCoroutine(PopGameStartInteractPanel(this.gameObject));
            if (Input.GetKeyDown(KeyCode.C))
            {
                isGameOn = true;
                GamePhaseNow = 0;
            }
            
        }


    }

    private void ResetTimeInfoLightGroup(bool _b)
    {
        foreach (GameObject go in TimeInfoLightGroup)
        {
            go.SetActive(_b);
        }
    }

    private void LateUpdate()
    {

    }

}
