﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TheEndTriggler : MonoBehaviour
{
    public bool isFinal = false;
    
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            isFinal = true;
            Camera.main.SendMessage("StartFinalCutScene");
            GetComponent<Collider>().enabled = false;
        }
    }
}
