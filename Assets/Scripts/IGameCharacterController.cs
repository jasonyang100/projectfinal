﻿using EnumsManager;
public interface IGameCharacterController
{
    GameCharacter ReturnGameCharacter();
}