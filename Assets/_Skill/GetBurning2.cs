﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
public class GetBurning2 : MonoBehaviour
{
    //List<Material> MaterialList;
    //Material StatusMaterial;
    [SerializeField] GameObject Burning_Go;//template
    [SerializeField] float Timer = 5f;
    GameObject go; // dummy goes destoyed if time out




    float _Timer;

    // Start is called before the first frame update
    void Start()
    {
        _Timer =Timer;
        Burning_Go = Resources.Load<GameObject>("GetBurning_Go");
        go = Instantiate(Burning_Go);
        go.transform.parent = this.gameObject.transform;
        go.transform.position = this.GetComponent<Collider>().bounds.center;
        Destroy(go, 5f);


    }

    // Update is called once per frame
    void Update()
    {
        _Timer -= Time.deltaTime;
        if (_Timer < 0)
        {
            EndBurning();
        }
        
    }

    void EndBurning()
    {
        Destroy(go);
        Destroy(this);

    }

    private void OnDisable()
    {
        _Timer = -1;
    }

    private void OnEnable()
    {
        Burning_Go = Resources.Load<GameObject>("Getburning_Go");
        GameObject go = Instantiate(Burning_Go);
        go.transform.parent = this.gameObject.transform;
    }
}
