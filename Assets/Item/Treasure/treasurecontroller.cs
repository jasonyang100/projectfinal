﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EnumsManager;

public class treasurecontroller : MonoBehaviour, IGameCharacterController
{
    private Item treasure;

    private void Awake()
    {
        treasure = new Item(gameObject);
        treasure.characterName = CharacterName.Treasure;
    }

    // Start is called before the first frame update
    void Start()
    {
        treasure.m_animation = GetComponent<Animation>();
        treasure.ItemSprite = Resources.Load<Sprite>("Sprites/runes");
        treasure.ItemName = "法力符石";
        treasure.Instruction = "隱約發著微光，蘊含著許多人渴望獲得的強大力量，而狂獵戰士瑞丁就是其中之一";
    }

    public GameCharacter ReturnGameCharacter()
    {
        return treasure;
    }
}
