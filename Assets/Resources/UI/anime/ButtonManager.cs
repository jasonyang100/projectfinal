﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonManager : MonoBehaviour
{
   public TextAnimator[] textAnimators;
    
    [SerializeField]private int _index=0;
    public int index
    {
        get { return _index; }
        set { _index = value >= 0 ? value% totalbutton : totalbutton-1; }
    }
    public int totalbutton;
    //public bool[] isSelecteds;




    // Start is called before the first frame update
    void Start()
    {
        
        textAnimators = GetComponentsInChildren<TextAnimator>();
        //isSelecteds = new bool[textAnimators.Length];
        totalbutton = textAnimators.Length;
        index = 0;
    }

    // Update is called once per frame
    void Update()
    {
        
            
            keyboardcontrol();
            selectabutton();
            textAnimators[index].IsSelected = true;

        
       
    }
    
        
       void selectabutton()
    {
        for(int i = 0; i < totalbutton ; i++)
        {
            if (i != index && textAnimators[i].IsSelected == true)
            {
                textAnimators[index].IsSelected = false;
                index = i;
            }                      
        }
    }
    

    void keyboardcontrol()
    {
        if(Input.GetKeyDown(KeyCode.DownArrow))
        {
            textAnimators[index++].IsSelected = false;
            
        }
        else if (Input.GetKeyDown(KeyCode.UpArrow))
        {
            textAnimators[index--].IsSelected = false;
        }
        
    }
    bool DontUpdateEveryTime()
    {
        if (Input.anyKeyDown)
        {
            return true;
        }
        return false;
    }


}
