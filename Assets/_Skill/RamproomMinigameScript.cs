﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RamproomMinigameScript : MonoBehaviour
{
    public GameObject Player;
    public Renderer MaskerRenderer;
    public Material MaskerMaterial;
    public Canvas MaskCanvas;
    public  Image MaskImage;
    public Material MagicTile;

    public GameObject[] Lights;

    public Transform StartObject;
    public Transform GoalObject;
    public float LimitDistance;


    public bool IsGameOn;
    private bool IsOutOfDistance=false;
    
    public float Timer=3f;
    [SerializeField]float _Timer;

    [SerializeField] CharacterController PlayerCharacterController;
    bool IsTeleporting=false;
    bool IsMurmured=false;





    // Start is called before the first frame update
    void Start()
    {
        //LimitDistance = 10f;
        _Timer = Timer;
        IsGameOn = false;
        Player.TryGetComponent<CharacterController>(out PlayerCharacterController);
        MaskCanvas.enabled = false;
    }

    // Update is called once per frame
    void Update()
    {
        IsGameStart();
        if (IsGameOn)
        {
            GameOn();
        }
        TPbackStart();


        




    }


    void TPbackStart()// teleport to start postion
    {
        
        if (_Timer<0)
        {
            if (!IsTeleporting)
            {
                StartCoroutine(PlayerTeleporting(StartObject.position + new Vector3(0f, 1f, 8f)));
            }
            _Timer = Timer;
            
        }    


        
        
        

    }


    void IsGameStart()
    {
        if (!IsGameOn)
        {
            if (this.transform.parent.name.Equals(Player.name))
            {

                IsGameOn = true;
            }
            else
            {
                IsGameOn = false;
            }
        }
    }



    void GameOn()
    {
        

        foreach(GameObject go in Lights)
        {
            //Debug.Log(Vector3.Distance(go.transform.position, Player.transform.position) > LimitDistance);
            if (Vector3.Distance(new Vector3(go.transform.position.x,0f, go.transform.position.z)
                , new Vector3(Player.transform.position.x,0f, Player.transform.position.z)) > LimitDistance)
            {
                IsOutOfDistance=true;
                

            }
            else
            {
                IsOutOfDistance = false;
                StretchFloorRaius();
                
                _Timer = Timer;
                return;
            }
            
        }

        if (IsOutOfDistance)
        {
            ShirnkFloorRaius();
            _Timer -= Time.deltaTime;
        }
        else
        {
            StretchFloorRaius();
        }

        
            
       
        


    }

    void PlayerTeleport(Vector3 TpPosition)// no used;
    {
        PlayerCharacterController.enabled = false;
        Player.transform.position = TpPosition;
        PlayerCharacterController.enabled = true;
    }

    IEnumerator PlayerTeleporting(Vector3 TpPosition)
    {

        IsTeleporting = true;
        MaskImage.color = new Color(0f, 0f, 0f, 0f);
        MaskCanvas.enabled = true;
        PlayerCharacterController.enabled = false;
        Vector3 _NowPosition = Player.transform.position;
        
        

        while (1f - MaskImage.color.a > 0.05f)//fade out
        {
            Player.transform.position = _NowPosition;
            MaskImage.color = Color.Lerp(MaskImage.color, new Color(0f, 0f, 0f, 1f),1f*Time.deltaTime);
            yield return null;
        }
        MaskImage.color = new Color(0f, 0f, 0f, 1f);

        
        Player.transform.position = TpPosition;
        PlayerCharacterController.enabled = true;
        PlayerCharacterController.Move(new Vector3(0, -100f, 0));

        yield return new WaitForSeconds(0.5f);
        yield return StartCoroutine(MaskCanvasFadingIn());
        //while (MaskImage.color.a > 0.01f)//fade in
        //{
        //    Player.transform.position = TpPosition;
        //    MaskImage.color = Color.Lerp(MaskImage.color, new Color(0f, 0f, 0f, 0f), 1f * Time.deltaTime);
        //    yield return null;
        //}
        MaskImage.color = new Color(0f, 0f, 0f, 0f);

        MaskCanvas.enabled = false;        
        _Timer = Timer;
        IsTeleporting = false;
        yield return new WaitForSeconds(0.5f);

        if (!IsMurmured)
        {
            NPCinteraction.Instance.OnSituationTalk("該死的幻術，應該是要跟著光嗎？",2f);
            IsMurmured = !IsMurmured;
        }




    }
    IEnumerator MaskCanvasFadingIn()
    {
        
        while (MaskImage.color.a > 0.05f)//fade in
        {
            
            MaskImage.color = Color.Lerp(MaskImage.color, new Color(0f, 0f, 0f, 0f), 5f * Time.deltaTime);
            yield return null;
        }
    }

    void ShirnkFloorRaius()
    {
       // while (MagicTile.GetFloat("_Radius") - 5f > 0.1f)
        {
            MagicTile.SetFloat("_Radius", Mathf.Lerp(MagicTile.GetFloat("_Radius"), 1f, 1.1f * Time.deltaTime));
        }

        
    }

    void StretchFloorRaius()
    {
        // while (MagicTile.GetFloat("_Radius") - 5f > 0.1f)
        {
            MagicTile.SetFloat("_Radius", Mathf.Lerp(MagicTile.GetFloat("_Radius"), 19f, 1.2f * Time.deltaTime));
        }

       
    }

}
