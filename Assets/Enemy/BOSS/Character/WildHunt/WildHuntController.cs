﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EnumsManager;
using System.Linq;

public class WildHuntController : MonoBehaviour, IGameCharacterController, IAttackableCharacter
{
    private Type controller;
    private Mob wildHunt;
    private MobAI wildHuntai;
    private List<Collider> weaponcollider = new List<Collider>(); 
    public GameObject WeaponEffect;
    private Queue<int> SwordShockAmountQueue = new Queue<int>();

    public SkinnedMeshRenderer wildHuntmeshRenderer;
    public MeshRenderer swordmeshRenderer;

    private WildHuntAttackForm attackForm = WildHuntAttackForm.FlashFiveChop;
    private Dictionary<WildHuntAttackForm, float> dict_attackRadius;
    private Dictionary<WildHuntAttackForm, Func<Action<float>, float, IEnumerator>> dict_attackDamage;

    private MobAlertBehaviour alertbehaviour = MobAlertBehaviour.STAND;
    private Dictionary<MobAlertBehaviour, Action> dict_alertbehaviour;
    private Vector3 AlertmovePos;

    private delegate MobState stateAction();

    private Dictionary<MobState, stateAction> stateActionDict;

    private Dictionary<MobState, Action> AnimActionDict;

    private Animator animator;
    private AnimatorStateInfo currentAnimState;

    private GameObject TargetSystemGO;
    private GameObject bossFloatingBar;
    private GetDieing GetDieingComponent;
    private List<GameObject> BurnigGos = new List<GameObject>();

    private int _underattackcounter = 0;
    private int underAttackCounter
    {
        get => _underattackcounter;
        set
        {
            if (value > 6) value = 6;
            if (value < 0) value = 0;
            _underattackcounter = value;
        }
    }
    private float _animSpeedParam = 0f;
    public float animSpeedParam
    {
        get
        {
            switch (wildHuntai.currentState)
            {
                case MobState.IDLE:
                case MobState.DEFENSE:
                case MobState.DAMAGED:
                case MobState.FIGHT:
                case MobState.ALERT:
                    _animSpeedParam = 0f;
                    break;
               
            }
            return _animSpeedParam;
        }
        set
        {
            switch (wildHuntai.currentState)
            {
                case MobState.CHASE:
                case MobState.RETURN:
                    value = Mathf.Clamp(value, 0.2f, 2f);
                    break;
            }
            _animSpeedParam = value;
        }
    }
    private float AlertTimer = 0f;
    private float DefenseTimer = 0f;
    private Camera cam;
    public Collider[] invisibleWalls;
    public GameObject Sealbound;
    private Transform[] SwordShockTransforms;
    private TeleportSkillScript teleportSkill;

    private void Awake()
    {
        controller = this.GetType();
        dict_attackRadius = new Dictionary<WildHuntAttackForm, float>
        {
            { WildHuntAttackForm.NormalFiveChop, 4.8f},
            { WildHuntAttackForm.FlashFiveChop, 20f},
            { WildHuntAttackForm.SpinFlashLeap, 20f},
            { WildHuntAttackForm.SwordShock, 1000f}
        };

        dict_attackDamage = new Dictionary<WildHuntAttackForm, Func<Action<float>, float, IEnumerator>>
        {
            { WildHuntAttackForm.NormalFiveChop, NormalFiveChop },
            { WildHuntAttackForm.FlashFiveChop, FlashFiveChop },
            { WildHuntAttackForm.SpinFlashLeap, SpinFlashLeap },
            { WildHuntAttackForm.SwordShock, SwordShock }
        };

        dict_alertbehaviour = new Dictionary<MobAlertBehaviour, Action>
        {
            { MobAlertBehaviour.STAND, AlertStand },
            { MobAlertBehaviour.MOVEBACK, AlertMoveback },
            { MobAlertBehaviour.THREATEN, AlertThreaten },
        };
        animator = GetComponent<Animator>();

        wildHunt = new Mob(gameObject);
        wildHunt.InitialHpMana(30f, 0f);
        wildHunt.characterName = CharacterName.WildHunt;
        wildHunt.Ccollider = GetComponent<Collider>();
        wildHunt.gravity = 3f;
        wildHunt.baseAD = 2f;
        wildHunt.Ccontroller = GetComponent<CharacterController>();
        wildHunt.moveSpeedDict = new Dictionary<MobState, float>()
        {
            { MobState.CHASE, 8f },
            { MobState.RETURN, 8f },
        };
        wildHunt.m_strDefenseSoundEffect = "19_Player_SwordHitMetal2";
        wildHunt.m_strUnderAttackSoundEffect = "19_Player_SwordHitMetal2";
        wildHunt.talks = new string[3]
        {
            "瑞丁: 法力符石不屬於你，交出來，不然就死吧! 獵魔人",
            "瑞丁: 哦，死到臨頭還挺有能耐的嗎",
            "瑞丁: 繼續阿，讓我看看你那些花拳繡腿的動作，我很想看看",
        };

        wildHuntai = new MobAI(wildHunt);
        wildHuntai.opposingGoTag = new HashSet<string> { "Player" };
        wildHuntai.mobLocomotionType = MobNormalStateType.IdleOnly;
        wildHuntai.initialPosition = wildHunt.CgameObject.transform.position;
        wildHuntai.idleRestTime = 20f;
        wildHuntai.wanderRadius = 0f;
        wildHuntai.SightRangeAngle = 120f;
        wildHuntai.attackRadius = dict_attackRadius[attackForm];
        wildHuntai.returnRadius = 100f;
        wildHuntai.alertRadius = 200f;

        wildHuntai.maxAttackTimeSpan = 2.2f;
        wildHuntai.minAttackTimeSpan = 0.1f;
        wildHuntai.attackTimeSpan = 1.6f;

        wildHuntai.neverFallback = true;

        stateActionDict = new Dictionary<MobState, stateAction>()
        {
            { MobState.IDLE, wildHuntai.Idle },
            { MobState.ALERT, wildHuntai.Alert },
            { MobState.CHASE, wildHuntai.Chase },
            { MobState.FIGHT, wildHuntai.Fight },
            { MobState.DEFENSE, wildHuntai.Defense },
            { MobState.DAMAGED, wildHuntai.Damaged },
            { MobState.RETURN, wildHuntai.Return },
            { MobState.DIE, wildHuntai.Die }
        };
        AnimActionDict = InitializeAnimActionDict();

        //WeaponEffect = GameObject.Find("WeaponEffect");
        TargetSystemGO = GameObject.Find("EnemySearchSphere");
        bossFloatingBar = GameObject.Find("BossFloatingBar");
        bossFloatingBar.SetActive(false);

        Sealbound = GameObject.Find("Sealbound");

        SwordShockTransforms = Sealbound.GetComponentsInChildren<Transform>(true).Where(go => go.gameObject != Sealbound).ToArray();

        cam = Camera.main;

        teleportSkill = GetComponent<TeleportSkillScript>();
        GetDieingComponent = transform.GetComponent<GetDieing>();

    }
    // Start is called before the first frame update
    void Start()
    {
        Sealbound.GetComponent<MeshRenderer>().enabled = false;
    }
    // Update is called once per frame
    void Update()
    {
        if (!wildHunt.Ccontroller.isGrounded) wildHunt.Ccontroller.Move(new Vector3(0f, -wildHunt.gravity * 2f, 0f) * Time.deltaTime);
        DoStateMachine();
    }
    private void DoStateMachine()
    {
        currentAnimState = animator.GetCurrentAnimatorStateInfo(0);
        AnimActionDict[wildHuntai.currentState]();
        MobState nextState = stateActionDict[wildHuntai.currentState]();
        wildHuntai.lastState = wildHuntai.currentState;
        if (wildHuntai.cutinState == MobState.NOTSTATE) wildHuntai.currentState = nextState;
        else
        {
            wildHuntai.currentState = wildHuntai.cutinState;
            wildHuntai.cutinState = MobState.NOTSTATE;
        }
        //Debug.Log(wildHuntai.currentState);
    }
    private IEnumerator NormalFiveChop(Action<float> attact, float reduction)
    {
        float originaldamage = wildHunt.baseAD;
        float damage = Mathf.Clamp(0f, originaldamage * (1 - reduction), originaldamage);
        attact(damage);
        yield break;
    }
    private IEnumerator FlashFiveChop(Action<float> attact, float reduction)
    {
        float originaldamage = wildHunt.baseAD;
        float damage = Mathf.Clamp(0f, originaldamage * (1 - reduction), originaldamage);
        attact(damage);
        yield break;
    }
    private IEnumerator SpinFlashLeap(Action<float> attact, float reduction)
    {
        float originaldamage = wildHunt.baseAD;
        float damage = Mathf.Clamp(0f, originaldamage * (1 - reduction), originaldamage);
        attact(damage);
        yield break;
    }
    private IEnumerator SwordShock(Action<float> attact, float reduction)
    {
        float originaldamage = wildHunt.baseAD * 1.5f;
        float damage = Mathf.Clamp(0f, originaldamage * (1 - reduction), originaldamage);
        attact(damage);
        yield break;
    }

    private void AlertStand()
    {
        if (wildHuntai.currentState != MobState.ALERT) return;

        if (AlertTimer >= wildHuntai.attackTimeSpan) wildHuntai.isActionDone = true;
    }
    private void AlertMoveback()
    {
        if (wildHuntai.currentState != MobState.ALERT) return;

        if (AlertTimer == 0)
        {
            animator.SetBool("Moveback", true);
            AlertTimer += 0.001f; //加一個微小時間避免animator偵測狀態卡住
            AlertmovePos = transform.position - transform.forward * 5f;
            //FlashEffect();
        }

        if (currentAnimState.IsName("Moveback") && !animator.IsInTransition(0))
        {
            if (Mathf.Sign(Vector3.Dot(transform.forward, AlertmovePos - transform.position)) >= 0 )
            {
                animator.SetBool("Moveback", false);
            }
            else wildHunt.Ccontroller.Move(-transform.forward * wildHunt.moveSpeedDict[MobState.CHASE]* 10 * Time.deltaTime);
        }

        if (AlertTimer >= wildHuntai.attackTimeSpan) wildHuntai.isActionDone = true;
    }
    private void AlertThreaten()
    {
        if (wildHuntai.currentState != MobState.ALERT) return;

        if (AlertTimer == 0)
        {
            animator.SetTrigger("Berserker");
            AlertTimer += 0.001f; //加一個微小時間避免animator偵測狀態卡住
        }
        if (AlertTimer >= wildHuntai.attackTimeSpan) wildHuntai.isActionDone = true;
    }

    private Dictionary<MobState, Action> InitializeAnimActionDict()
    {
        return new Dictionary<MobState, Action>()
        {
            { MobState.IDLE, () =>
                {
                    if(wildHuntai.lastState != wildHuntai.currentState)
                    {
                        AuxiliaryTool.Instance.ResetAnimatorTriggers(animator,"Berserker", "FiveChop","SwordShock", "SpinFlashLeap", "getHit","OnPlayerDead");
                        animator.SetBool("Defense",false);
                        animator.SetBool("Flash",false);
                        animator.SetBool("SubFlash", false);
                        animator.SetBool("Moveback",false);
                        animator.SetFloat("Speed", animSpeedParam);
                    }
                }
            },
            { MobState.ALERT, () =>
                {
                    animator.SetFloat("Speed", animSpeedParam);
                    animator.SetBool("Flash", false);
                    animator.SetBool("SubFlash", false);
                    if(wildHuntai.lastState != wildHuntai.currentState)
                    {
                        AuxiliaryTool.Instance.ResetAnimatorTriggers(animator,"Berserker", "FiveChop","SwordShock", "SpinFlashLeap");
                        AlertTimer = 0f;

                        if(wildHuntai.aimingGo != null)
                        {
                            SelectAttackMoves();
                            SelectAlertForms();
                        }
                        else alertbehaviour = MobAlertBehaviour.STAND;
                    }
                    dict_alertbehaviour[alertbehaviour]();
                    if (currentAnimState.IsName("Idle") && !animator.IsInTransition(0))
                        AlertTimer += Time.deltaTime;
                }
            },
            { MobState.CHASE, () =>
                {
                    if(wildHuntai.lastState != wildHuntai.currentState)
                    {
                         AuxiliaryTool.Instance.ResetAnimatorTriggers(animator,"getHit","Berserker");
                         animator.SetBool("Defense", false);
                    }
                    animSpeedParam += 0.2f;
                    animator.SetFloat("Speed", animSpeedParam);
                }
            },
            { MobState.FIGHT, () => 
                {
                    animator.SetFloat("Speed", animSpeedParam);
                    if(wildHuntai.lastState != wildHuntai.currentState)
                    {
                        wildHuntai.attackTimeSpan = wildHuntai.maxAttackTimeSpan;
                        AuxiliaryTool.Instance.ResetAnimatorTriggers(animator,"getHit","Berserker");

                        if(currentAnimState.IsName("Idle") || currentAnimState.IsName("Move"))
                        {
                            StartCoroutine( LaunchAttack(attackForm) );
                        }
                    }
                }
            },
            { MobState.DEFENSE, () => //Defense
                {
                    if(wildHuntai.lastState != wildHuntai.currentState)
                    {
                        EnableWeapon(false);
                        AuxiliaryTool.Instance.ResetAnimatorTriggers(animator,"Berserker", "getHit");
                        DefenseTimer = 0f;
                        animator.SetBool("Defense", true);
                        animator.SetFloat("Speed", animSpeedParam);
                        animator.SetBool("Flash",false);
                        animator.SetBool("SubFlash", false);
                        animator.SetBool("Moveback",false);
                        wildHuntai.attackTimeSpan -= 0.8f;
                    }
                    if (currentAnimState.IsName("Defense") && !animator.IsInTransition(0))
                    {
                        DefenseTimer += Time.deltaTime;
                        if(DefenseTimer >= 3f)
                        {
                            wildHuntai.isActionDone = true;
                            animator.SetBool("Defense", false);
                        }
                    }
                }
            },
            { MobState.DAMAGED,  () => //Damaged
                {
                    if(wildHuntai.lastState != wildHuntai.currentState)
                    {
                        AuxiliaryTool.Instance.ResetAnimatorTriggers(animator,"Berserker");
                        EnableWeapon(false);
                        animator.SetBool("Defense",false);
                        animator.SetBool("Flash",false);
                        animator.SetBool("SubFlash", false);
                        animator.SetBool("Moveback",false);
                        animator.SetTrigger("getHit");
                        wildHuntai.attackTimeSpan -= 0.8f;
                    }
                }
            },
            { MobState.RETURN, () => //Return
                {
                    AuxiliaryTool.Instance.ResetAnimatorTriggers(animator,"Berserker", "FiveChop","SwordShock", "SpinFlashLeap", "getHit","OnPlayerDead");
                    animator.SetBool("Defense",false);
                    animator.SetBool("Flash",false);
                    animator.SetBool("SubFlash", false);
                    animator.SetBool("Moveback",false);
                    animSpeedParam += 0.2f;
                    animator.SetFloat("Speed", animSpeedParam);
                }
            },
            { MobState.DIE, () => //Die
                {
                    if(wildHuntai.lastState != wildHuntai.currentState)
                    {
                        EnableWeapon(false);
                        AuxiliaryTool.Instance.ResetAnimatorTriggers(animator,"getHit");
                        animator.SetBool("Defense",false);
                        animator.SetBool("Flash",false);
                        animator.SetBool("SubFlash", false);
                        animator.SetBool("Moveback",false);
                        animator.SetBool("isDead",true);
                        AudioManager.instance.FadeOutStop("2_BGM_FinalBattle");
                        TargetSystemGO.SendMessage("RemoveMob", wildHunt);
                        removeBurning(1f);
                        Destroy(bossFloatingBar, 1f);
                        WeaponEffect.SetActive(false);
                        StopAllCoroutines();
                        StartCoroutine( AuxiliaryTool.Instance.CoroutineWaiter(1f, () => GetDieingComponent.enabled = true) );
                        Destroy(Sealbound, 3f);
                        foreach(var wall in invisibleWalls)
                        {
                            Destroy(wall, 3f);
                        }
                        wildHunt.Ccollider.enabled = false;
                        wildHunt.Ccontroller.enabled = false;
                        this.enabled = false;
                    }
                }
            },
        };
    }
    private void OnTriggerEnter(Collider other)
    {
    }
    private void UnderAttack(object[] obj)
    {
        Vector3 HitDir = (obj[0] as Transform).position;
        Func<Action<float>, float, IEnumerator> HitDel = obj[1] as Func<Action<float>, float, IEnumerator>;
        float reduction = 0f;
        if (wildHuntai.currentState != MobState.DEFENSE)
        {
            wildHuntai.currentState = MobState.DAMAGED;
            if (underAttackCounter == 0)
            {
                underAttackCounter++;
            }
            else
            {
                int diceNo = UnityEngine.Random.Range(underAttackCounter, 7);

                if (diceNo == underAttackCounter)
                {
                    wildHuntai.currentState = MobState.DEFENSE;
                    underAttackCounter = 0;
                }
                else underAttackCounter += 2;
            }
        }
        else reduction = 0.8f;

        StartCoroutine(HitDel(damage => wildHunt.GetHit(damage), reduction));
        wildHuntai.GetHitDir = HitDir - gameObject.transform.position;

        if (wildHunt.talks.Length != 0)
        {
            float HpRate = wildHunt.currentHP / wildHunt.MaxHP;
            if (wildHunt.talks.Length == 2 && HpRate < 0.7f) OnWildHuntTalk(wildHunt.talks[0]);
            if (wildHunt.talks.Length == 1 && HpRate < 0.35f) OnWildHuntTalk(wildHunt.talks[0]);
        }
        //  Debug.Log(wildHunt.currentHP);
    }
    private void OnWildHuntTalk(string talk)
    {
        NPCinteraction.Instance.OnSituationTalk(talk, 2.5f);
        wildHunt.talks = wildHunt.talks.Skip(1).ToArray();
    }
    void OnAnimationAttackStart()
    {
        EnableWeapon(true);
    }
    void OnAnimationAttackEnd()
    {
        EnableWeapon(false);
    }
    private void OnAnimationDone(string state)
    {
        if (state != wildHuntai.currentState.ToString()) return;

        if (currentAnimState.IsTag("Attack"))
        {
            if (currentAnimState.IsName("Shock3") || currentAnimState.IsName("Leap") || currentAnimState.IsName("Attack5"))
            {
                wildHuntai.isActionDone = true;
            }
            else if (currentAnimState.IsName("SpinAttack"))
            {
                StartCoroutine(SubFlashAttack());
            }
        }
        else wildHuntai.isActionDone = true;
    }
    private void OnHitEffect(Collider col)
    {
        string HitSoundEffect = col.GetComponent<IAttackableCharacter>().UnderAttackSoundEffect();
        AudioManager.instance.Play(HitSoundEffect);
    }
    private void AttackOnHit(object[] obj)
    {
        Collider col = obj[0] as Collider;
        string Hittag = obj[1] as string;

        if (!wildHuntai.opposingGoTag.Contains(col.tag)) return;
        if (Hittag == "Weapon")
        {
            col.SendMessage("UnderAttack", new object[2] { transform, dict_attackDamage[attackForm] });
            OnHitEffect(col);
            //EnableWeapon(false);
        }
    }
    private void SelectAlertForms()
    {
        if (wildHuntai.aimingGo == null) return;

        if (wildHuntai.lastState == MobState.IDLE)
        {
            alertbehaviour = MobAlertBehaviour.THREATEN;
            return;
        }
        else if (wildHuntai.lastState == MobState.DEFENSE)
        {
            alertbehaviour = MobAlertBehaviour.MOVEBACK;
            return;
        }
        else if (wildHuntai.lastState == MobState.DAMAGED)
        {
            int rnd = UnityEngine.Random.Range(0, 5);

            switch (rnd)
            {
                case 0:
                    alertbehaviour = MobAlertBehaviour.MOVEBACK;
                    break;
                default:
                    alertbehaviour = MobAlertBehaviour.STAND;
                    break;
            }            
        }
        else alertbehaviour = MobAlertBehaviour.STAND;      
    }
    private void SelectAttackMoves()
    {
        if (wildHuntai.aimingGo == null) return;

        if(SwordShockAmountQueue.Count != 0) SwordShockAmountQueue.Clear();

        float distance = Vector3.Distance(transform.position, wildHuntai.aimingGo.transform.position);

        //避免順移到結界外,當玩家太靠近結界邊緣的時候,強制使用SwordShock
        float distanceFromCenter = Vector3.Distance(Sealbound.transform.position, wildHuntai.aimingGo.transform.position);

        if(distanceFromCenter > 40f) attackForm = WildHuntAttackForm.SwordShock;
        else
        {
            if (distance <= 5f)
            {
                int rnd = UnityEngine.Random.Range(1, 8);
                if (rnd >= 4) attackForm = WildHuntAttackForm.NormalFiveChop;
                else attackForm = (WildHuntAttackForm)rnd;
            }
            else
            {
                int rnd = UnityEngine.Random.Range(0, 5);
                switch (rnd)
                {
                    case 0:
                        attackForm = WildHuntAttackForm.SwordShock;
                        break;
                    case 1:
                    case 2:
                        attackForm = WildHuntAttackForm.FlashFiveChop;
                        break;
                    default:
                        attackForm = WildHuntAttackForm.SpinFlashLeap;
                        break;
                }
            }
        }
        wildHuntai.attackRadius = dict_attackRadius[attackForm];
    }
    public void CallCharacterRegistrar(object[] obj)
    {
        GameCharacter gc = obj[0] as GameCharacter;
        bool bo = (bool)obj[1];
        if (bo) wildHunt.Register(gc);
        else wildHunt.Unregister(gc);
    }
    private void AddWeaponGameObject(Collider col) => weaponcollider.Add(col);
    private void EnableWeapon(bool bo)
    {
        foreach (Collider col in weaponcollider)
        {
            col.enabled = bo;
        }
    }
    private void OnOpposingGoDie(GameObject go)
    {
        StartCoroutine(AuxiliaryTool.Instance.CoroutineWaiter(1f,
            () =>
            {
                if (wildHuntai.aimingGo == go) wildHuntai.aimingGo = null;
                animator.SetTrigger("OnPlayerDead");
                AudioManager.instance.FadeOutStop("2_BGM_FinalBattle");
                wildHuntai.currentState = MobState.IDLE;
            }));
    }
    private IEnumerator SubFlashAttack()
    {
        wildHunt.Ccontroller.detectCollisions = false;

        animator.SetBool("SubFlash", true);

        removeBurning();

        yield return new WaitForSeconds(0.15f);

        FlashEffect();//播Flash特效

        yield return new WaitForSeconds(0.35f);

        MeshControl(false); //關掉MESH

        yield return new WaitForSeconds(0.3f);

        FlashToPosition();

        yield return new WaitForSeconds(0.3f);

        MeshControl(true); //打開MESH

        FlashEffect();//播Flash特效

        wildHunt.Ccontroller.detectCollisions = true;

        yield return new WaitForSeconds(0.15f);

        Vector3 aimingGodir = wildHuntai.aimingGo.transform.position - transform.position;
        Vector3 lookatdir = new Vector3(aimingGodir.x, 0f, aimingGodir.z).normalized;
        transform.rotation = Quaternion.LookRotation(lookatdir, Vector3.up);

        animator.SetBool("SubFlash", false);

        yield break;
    }
    private IEnumerator LaunchAttack(WildHuntAttackForm AttackTriggerType)
    {
        string triggertype;

        if (AttackTriggerType == WildHuntAttackForm.NormalFiveChop || AttackTriggerType == WildHuntAttackForm.FlashFiveChop)
        {
            triggertype = "FiveChop";
        }
        else
        {
            triggertype = AttackTriggerType.ToString();
        }

        if (AttackTriggerType != WildHuntAttackForm.NormalFiveChop)
        {
            wildHunt.Ccontroller.detectCollisions = false;

            animator.SetBool("Flash", true);

            animator.SetTrigger(triggertype);

            removeBurning();

            yield return new WaitForSeconds(0.15f);

            FlashEffect();//播Flash特效

            yield return new WaitForSeconds(0.35f);

            MeshControl(false); //關掉MESH

            yield return new WaitForSeconds(0.3f);

            FlashToPosition();

            yield return new WaitForSeconds(0.3f);

            MeshControl(true); //打開MESH

            FlashEffect();//播Flash特效

            wildHunt.Ccontroller.detectCollisions = true;

            yield return new WaitForSeconds(0.15f);

            Vector3 aimingGodir = wildHuntai.aimingGo.transform.position - transform.position;
            Vector3 lookatdir = new Vector3(aimingGodir.x, 0f, aimingGodir.z).normalized;
            transform.rotation = Quaternion.LookRotation(lookatdir, Vector3.up);

            animator.SetBool("Flash", false);

            // 攻擊COMBO打完 delaytime
        }
        else
        {
            animator.SetTrigger(triggertype);
        }
        yield break;
    }
    private void OnAnimationSwordShock() //發射劍波
    {     
        if(SwordShockAmountQueue.Count == 0)
        {
            for(int i = 1; i <= 5; i += 2)
            {
                SwordShockAmountQueue.Enqueue(i);
            }
        }

        int Amount = SwordShockAmountQueue.Dequeue();

        float rotationspan = 180 / (Amount + 1);

        GameObject SwordShock = EffectManager.Instance.WildHuntEffectdict["SwordShock"];

        SwordShock.transform.position = transform.position + transform.forward * 2f;

        Vector3 TargetDir = wildHuntai.aimingGo.transform.position - transform.position;

        Vector3 FirstDir = Quaternion.Euler(0, rotationspan - 90, 0) * TargetDir;

        transform.forward = TargetDir;

        for (int i = 0; i < Amount; i++)
        {
            Vector3 ShockDir = Quaternion.Euler(0, rotationspan * i, 0) * FirstDir;
            SwordShock.transform.forward = ShockDir;
            Instantiate(SwordShock);
        }
    }
    private void FlashEffect() 
    {
        teleportSkill.enabled = true;
        AudioManager.instance.Play("50_Wildhunt_Flash");
        _ = AuxiliaryTool.Instance.AsyncWaiter(0.5f, () => teleportSkill.enabled = false);
    }
    private void MeshControl(bool b)
    {
        swordmeshRenderer.enabled = b;
        wildHuntmeshRenderer.enabled = b;
        WeaponEffect.SetActive(b);
    }
    private void FlashToPosition()
    {
        Vector3 firstMove;
        Vector3 SecondMove;
        Vector3 pos;
        if (attackForm == WildHuntAttackForm.SwordShock)
        {
            int rnd = 0;
            int len = SwordShockTransforms.Length;
            for (int i = 0; i < len; ++i)
            {
                rnd = UnityEngine.Random.Range(0, len);

                if (Vector3.Distance(SwordShockTransforms[rnd].position, wildHuntai.aimingGo.transform.position) > 6f)
                {
                    break;
                }
            }
            pos = SwordShockTransforms[rnd].position;

            firstMove = (pos + new Vector3(0, 20f, 0)) - transform.position;

            wildHunt.Ccontroller.Move(firstMove);

            SecondMove = new Vector3(0, -100f, 0);

            wildHunt.Ccontroller.Move(SecondMove);
        }
        else
        {
            pos = wildHuntai.aimingGo.transform.position;

            float nexttheta = UnityEngine.Random.Range(0, 181) * (2 * Mathf.PI / 360);

            float flashx = pos.x + 4.6f * Mathf.Cos(nexttheta);

            float flashz = pos.z - 4.6f * Mathf.Sin(nexttheta);

            firstMove = (pos + new Vector3(0, 20f, 0)) - transform.position;

            wildHunt.Ccontroller.Move(firstMove);

            SecondMove = new Vector3(flashx - transform.position.x, -100f, flashz - transform.position.z);

            wildHunt.Ccontroller.Move(SecondMove);
        }
    }
    private void Engage(Collider col)
    {
        wildHuntai.aimingGo = col.gameObject;

        wildHuntai.aimingGo.SendMessage("CallCharacterRegistrar", new object[2] { wildHunt ,true});

        MeshControl(true);
        
        FlashEffect();

        foreach (var wall in invisibleWalls)
        {
            wall.enabled = true;
        }
        bossFloatingBar.SetActive(true);

        StartCoroutine(AuxiliaryTool.Instance.CoroutineWaiter(0.35f, () => wildHuntai.cutinState = MobState.ALERT));

        StartCoroutine(AuxiliaryTool.Instance.CoroutineWaiter(0.85f, () =>
        {
            cam.SendMessage("StartShake", new object[2] { 1.5f, 0.7f });
            AudioManager.instance.Play("49_Earthquake");
            StartCoroutine(AuxiliaryTool.Instance.CoroutineWaiter(1.5f, () =>
            {
                NPCinteraction.Instance.OnSituationTalk(wildHunt.talks[0], 2.5f);
                wildHunt.talks = wildHunt.talks.Skip(1).ToArray();
                AudioManager.instance.Play("2_BGM_FinalBattle",false);
            }));
            Sealbound.GetComponent<MeshRenderer>().enabled = true;
        }));
    }
    private void GetBurning()
    {
        GameObject go = Instantiate(EffectManager.Instance.GetBurning);
        BurnigGos.Add(go);
        go.transform.parent = transform;
        go.transform.position = wildHunt.Ccollider.bounds.center;

        StartCoroutine(AuxiliaryTool.Instance.CoroutineWaiter(5f, () =>
        {
            if (BurnigGos.Contains(go))
            {
                BurnigGos.Remove(go);
                Destroy(go);
            }
        }));
    }
    private void removeBurning(float destorytime = 0f)
    {
        if (BurnigGos.Count > 0)
        {
            foreach (var go in BurnigGos)
            {
                if (go != null) Destroy(go);
            }
            BurnigGos.Clear();
        }
    }
    public GameCharacter ReturnGameCharacter()
    {
        return wildHunt;
    }
    public string UnderAttackSoundEffect()
    {
        if (wildHuntai.currentState == MobState.DEFENSE) return wildHunt.m_strDefenseSoundEffect;
        else return wildHunt.m_strUnderAttackSoundEffect;
    }
}