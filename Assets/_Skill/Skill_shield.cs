﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Skill_shield : MonoBehaviour
{
    public KeyCode m_keycode;//49~57 = 1~9
    [SerializeField] bool IsActive;
    [SerializeField] bool IsRecastable;
    [SerializeField] GameObject TragetGameObject;
    [SerializeField] GameObject MeGameObject;
    [SerializeField] GameObject SkillCarrierGameObject;

    public float DeployDelayTime = 10f;
    [SerializeField] float _DeployDelayTime = 0;



    public float InputDelayTime = 5;
    [SerializeField] float _InputDelayTime = 0;
    public float DeployTime = 10;
    [SerializeField] float _DeployTime = 10;

    // Start is called before the first frame update
    void Start()
    {
        m_keycode = KeyCode.Q;
        //if (m_keycode == null)
        //{
        //    m_keycode = KeyCode.Alpha9;// have to search a list of unused key
        //}

        MeGameObject = transform.gameObject;
        IsActive = false;
        SkillCarrierGameObject.SetActive(false);

        
        SkillCarrierGameObject.SetActive(false);




    }

    // Update is called once per frame
    void Update()
    {
        //Clock += Time.deltaTime;
        
        deploySkill(triggered());
        

    }

    bool triggered()
    {
        if (_InputDelayTime <= 0)
        {
            if (Input.GetKeyDown(m_keycode))
            {
                if (!IsRecastable)
                {
                    IsActive = !IsActive;
                }
                else if(IsRecastable)
                {
                    _DeployTime = DeployTime;
                    IsActive = true;
                    
                }
                
                _InputDelayTime = InputDelayTime;
            }
            
        }
        _InputDelayTime -= Time.deltaTime;
        return IsActive;
    }


    void deploySkill(bool _isActive)
    {
        if (_isActive)
        {

            _DeployTime -= Time.deltaTime;
            if (_DeployTime < 0)
            {
                IsActive = _isActive;

            }
        }
        else _DeployTime = DeployTime;
        SkillCarrierGameObject.SetActive(DeployDelayControl(IsActive));

    }
    void setTarget()
    {
        // select a target to do something
    }

    
    bool DeployDelayControl(bool _bool)
    {
        if (_bool)
        {
            _DeployTime -= Time.deltaTime;
            if (_DeployTime < 0)
            {
                
                return true;
                

            }
            
        }
        else _DeployDelayTime = DeployDelayTime;
        return false;
    }






}
