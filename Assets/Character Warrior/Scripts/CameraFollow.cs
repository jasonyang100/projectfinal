﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.Rendering.Universal;

public class CameraFollow : MonoBehaviour
{
    [SerializeField] Transform followObj;


    [SerializeField] float followDistance = 7f;
    [SerializeField] float mouseSensitivity = 3.5f;


    private float mouseX, mouseY;
    public float horizontal = -2f;
    public float vertical = 30f;
    private float targetCameraDis;
    private Vector3 camPos, preCam;
    private Vector3 test;


    Volume globalVolume;
    public MotionBlur motionBlur;
    PlayerMove playerMove;
    GameObject player;
    private void Start()
    {
        if (GameObject.Find("FollowObj"))
            followObj = GameObject.Find("FollowObj").transform;
        if(GameObject.Find("Character Warrior"))
            player = GameObject.Find("Character Warrior");


        playerMove = player.GetComponent<PlayerMove>();
        globalVolume = GetComponent<Volume>();

        globalVolume.profile.TryGet<MotionBlur>(out motionBlur);
        motionBlur.active = false;

        globalVolume.weight = 1f;

        Vector3 resetAngle;
        resetAngle = followObj.eulerAngles;
        mouseX = resetAngle.y ;
        mouseY = resetAngle.x + 17;
    }

    private void LateUpdate()
    {
        float camDistance = followDistance;

        if (playerMove.attackMode)
        {
            //Vector3 attVec = AttackModeCameraPon();
            targetCameraDis = 14f;
        }
        else if (!playerMove.attackMode)
        { 
            targetCameraDis = 7f;
        }

        Vector3 nomVec = NormalModeCameraPon();
        if (Input.GetMouseButton(1))
        {
            mouseX += Input.GetAxis("Mouse X") * mouseSensitivity;
            mouseY -= Input.GetAxis("Mouse Y") * mouseSensitivity;
        }
   
        nomVec = Vector3.SmoothDamp(transform.position, nomVec, ref test, 0.1f);
        camDistance = Mathf.Lerp(camDistance, targetCameraDis, 3f * Time.deltaTime);
        followDistance = camDistance;

        transform.position = nomVec;        //不能刪
        transform.position = CheckCollision(nomVec);
        ResetCamera();
        transform.LookAt(followObj);
    }
    //==============普通模式攝影機位置==============
    private Vector3 NormalModeCameraPon()
    {
        mouseY = Mathf.Clamp(mouseY, -30.0f, 60.0f);
        Quaternion rotationEuler = Quaternion.Euler(mouseY, mouseX, 0);
        camPos = rotationEuler * new Vector3(0, 0, -followDistance) + followObj.position;

        return camPos;
    }

    //==============重置攝影機==============
    private void ResetCamera()
    {
        if (Input.GetKeyDown(KeyCode.V))
        {
            Vector3 resetAngle;
            resetAngle = followObj.eulerAngles;
            mouseX = resetAngle.y ;
            mouseY = resetAngle.x + 17;
        }
    }

    //==============攝影機特效==============
    public void CameraEffect()
    {
        if (Input.GetKey(KeyCode.E))
        {   //景深
            globalVolume.weight += 0.01f;
            if (globalVolume.weight >= 1f)
                globalVolume.weight = 1f;
        }
        else
        {   //景深
            globalVolume.weight -= 0.01f;
            if (globalVolume.weight <= 0f)
                globalVolume.weight = 0f;
        }
    }

    //==============障礙物偵測===============
    Vector3 CheckCollision(Vector3 vCheckPos)
    {
        Vector3 vRet = vCheckPos;
        Vector3 vec = transform.position - followObj.position;
        vec.Normalize();
        RaycastHit rh;
        if (Physics.SphereCast(followObj.position, 0.1f, vec, out rh, followDistance, 1 << 8))
        {
            Vector3 vpos = followObj.position + vec * (rh.distance - 0.01f);
            vRet = vpos;
        }
        return vRet;
    }
    public void MotionBlurEffect()
    {
        motionBlur.active = true;
    }
    public void MotionBlurEffect1()
    {
        motionBlur.active = false;
    }
}
