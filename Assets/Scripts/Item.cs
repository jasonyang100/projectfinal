﻿using EnumsManager;
using System.Collections;
using UnityEngine;

public class Item : NonPlayer
{
    public Animation m_animation;
    public Sprite ItemSprite;
    public string ItemName;
    public string Instruction;
    public Item(GameObject go) : base(go)
    {
        base.npcType = NPCType.Item;
    }
}