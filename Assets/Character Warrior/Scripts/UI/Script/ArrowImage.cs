﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ArrowImage : MonoBehaviour
{
    public Image arrow;
    public RectTransform arrowRectTransform;
    public PlayerController playerController;
    AudioManager audioManager;

    private Vector2 MousePosition;
    private Vector2 fromVector2x = new Vector2(0.5f, 0.1f);
    private Vector2 centerCircle = new Vector2(0.5f, 0.5f);
    private Vector2 toVector2M;

    private int imageRotation;
    public int skillSwitch; 

    // Start is called before the first frame update

    void Start()
    {
        audioManager = FindObjectOfType<AudioManager>();
        playerController = FindObjectOfType<PlayerController>();
        arrow = GetComponent<Image>();
        arrowRectTransform = GetComponent<RectTransform>();
    }

    // Update is called once per frame
    void Update()
    {
        GetCurrentMenuItem();
        arrowRectTransform.rotation = Quaternion.Euler(0, 0, -imageRotation);
    }
    public void GetCurrentMenuItem()
    {
        MousePosition = new Vector2(Input.mousePosition.x, Input.mousePosition.y - 20);
        toVector2M = new Vector2(MousePosition.x / Screen.width, MousePosition.y / Screen.height);

        float angle =(
            Mathf.Atan2(fromVector2x.y - centerCircle.y, fromVector2x.x - centerCircle.x) -
            Mathf.Atan2(-toVector2M.y + centerCircle.y, -toVector2M.x + centerCircle.x) * Mathf.Rad2Deg);
        if (angle < 0)
            angle += 360;
        //print(angle);
        if (angle > 0 && angle < 60)
        { imageRotation = 30; skillSwitch = 0; }

        else if (angle > 60 && angle < 120)
        { imageRotation = 90; skillSwitch = 1; }

        else if (angle > 120 && angle < 180)
        { imageRotation = 150; skillSwitch = 2; }

        else if (angle > 180 && angle < 240)
        { imageRotation = 210; skillSwitch = 3; }

        else if (angle > 240 && angle < 300)
        { imageRotation = 270; skillSwitch = 4; }

        else if (angle > 300 && angle < 360)
        { imageRotation = 330; skillSwitch = 5; }

        if (arrowRectTransform.rotation != Quaternion.Euler(0, 0, -imageRotation))
            audioManager.Play("7_UI_MagicSelectedButton", false);
    }
}
