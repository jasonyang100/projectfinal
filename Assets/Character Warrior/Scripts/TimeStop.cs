﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimeStop : MonoBehaviour
{
    [SerializeField] float duration = 0.05f;
    [SerializeField] float timeScale = 0.1f;

    bool isFreeze = false;

    public void Freeze()
    {
        if (!isFreeze)
        {
            StartCoroutine(doFreeze());
        }
    }
    IEnumerator doFreeze()
    {
        isFreeze = true;
        var original = Time.timeScale;
        Time.timeScale = timeScale;

        yield return new WaitForSecondsRealtime(duration);

        Time.timeScale = original;
        isFreeze = false;
    }
}
