﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SealedGateScript : MonoBehaviour
{
    public Canvas InfoUI;
    public RamproomScript ramproomScript;
    public Renderer SealedGateRenderer;
    public Material SealedGateMaterial;
    public Collider SealedGateCollider;
    public bool isGateOpen;
    public int GateNumer;

    public string text;

    public bool isPlayerClose;



    // Start is called before the first frame update
    void Start()
    {
        isPlayerClose = true;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void BreakThisSeal()
    {

    }

    private void GoInvisible()
    {
        
    }


    private void OnTriggerEnter(Collider other)
    {
        if(other.CompareTag("Player"))
        {
            isPlayerClose = true;

            //Debug.Log("A");
        }
        
    }


    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            isPlayerClose = false;

            //Debug.Log("B");
        }
    }




    void OpenGate()
    {

    }

}
