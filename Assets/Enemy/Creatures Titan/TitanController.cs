﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Reflection;
using System.Linq;
using UnityEngine;
using EnumsManager;

public class TitanController : MonoBehaviour, IGameCharacterController, IAttackableCharacter
{
    private Type controller;
    private Mob titan;
    private MobAI titanai;
    private List<Collider> weaponcollider = new List<Collider>(); //左手&右手
    private GameObject WeaponEffect;

    private TitanAttackForm attackForm = TitanAttackForm.Smash;
    private Dictionary<TitanAttackForm, float> dict_attackRadius;
    private Dictionary<TitanAttackForm, Func<Action<float>, float, IEnumerator>> dict_attackDamage;

    private MobAlertBehaviour alertbehaviour = MobAlertBehaviour.STAND;
    private Dictionary<MobAlertBehaviour, Action> dict_alertbehaviour;
    private Vector3 AlertmovePos;
    private bool isTwinsAnottherDead = false;

    private delegate MobState stateAction();

    private Dictionary<MobState, stateAction> stateActionDict;

    private Dictionary<MobState, Action> AnimActionDict;

    private Animator animator;
    private AnimatorStateInfo currentAnimState;

    public GameObject TwinsAnotherGO;
    private GameObject TargetSystemGO;
    private GameObject triggerGo;
    private GetDieing GetDieingComponent;
    private List<GameObject> BurnigGos = new List<GameObject>();
    private GameObject RageEffectGo;

    private int _underattackcounter = 0;
    private int underAttackCounter
    {
        get => _underattackcounter;
        set
        {
            if (value > 6) value = 6;
            if (value < 0) value = 0;
            _underattackcounter = value;
        }
    }
    private float _animSpeedParam = 0f;
    public float animSpeedParam
    {
        get
        {
            switch (titanai.currentState)
            {
                case MobState.IDLE:
                case MobState.FIGHT:
                    _animSpeedParam = 0f;
                    break;
                case MobState.ALERT:
                    _animSpeedParam = 0.1f;
                    break;
            }
            return _animSpeedParam;
        }
        set
        {
            switch (titanai.currentState)
            {
                case MobState.CHASE:
                case MobState.RETURN:
                    value = Mathf.Clamp(value, 0.2f, 2f);
                    break;
            }
            _animSpeedParam = value;
        }
    }
    private float AlertTimer;

    private void Awake()
    {
        controller = this.GetType();
        dict_attackRadius = new Dictionary<TitanAttackForm, float>
        {
            { TitanAttackForm.Smash, 4.2f},
            { TitanAttackForm.DoubleStrike, 4.2f},
            { TitanAttackForm.TripleStrike, 4.2f}
        };

        dict_attackDamage = new Dictionary<TitanAttackForm, Func<Action<float>, float, IEnumerator>>
        {
            { TitanAttackForm.Smash, Smash },
            { TitanAttackForm.DoubleStrike, DoubleStrike  },
            { TitanAttackForm.TripleStrike, TripleStrike  }
        };

        dict_alertbehaviour = new Dictionary<MobAlertBehaviour, Action>
        {
            { MobAlertBehaviour.STAND, AlertStand },
            { MobAlertBehaviour.MOVEBACK, AlertMoveback },
            { MobAlertBehaviour.THREATEN, AlertThreaten },
        };

        titan = new Mob(gameObject);
        titan.InitialHpMana(16f, 0f);
        titan.characterName = CharacterName.Titan;
        titan.Ccollider = GetComponent<Collider>();
        titan.baseAD = 1f;
        titan.gravity = 5f;
        titan.Ccontroller = GetComponent<CharacterController>();
        titan.moveSpeedDict = new Dictionary<MobState, float>()
        {
            { MobState.CHASE, 6.6f },
            { MobState.RETURN, 6.6f },
        };
        titan.m_strUnderAttackSoundEffect = "16_Player_SwordHitBody2";

        titanai = new MobAI(titan);
        titanai.opposingGoTag = new HashSet<string> { "Player" };
        titanai.mobLocomotionType = MobNormalStateType.IdleOnly;
        titanai.initialPosition = titan.CgameObject.transform.position;
        titanai.idleRestTime = 20f;
        titanai.wanderRadius = 0f;
        titanai.SightRangeAngle = 110f;
        titanai.attackRadius = dict_attackRadius[attackForm];
        titanai.returnRadius = 60f;
        titanai.alertRadius = 0f;

        titanai.maxAttackTimeSpan = 1.5f; 
        titanai.minAttackTimeSpan = 0.2f;
        titanai.attackTimeSpan = 0.5f;

        titanai.TurnOnCompaniesAviodance = true;


        stateActionDict = new Dictionary<MobState, stateAction>()
        {
            { MobState.IDLE, titanai.Idle },
            { MobState.ALERT, titanai.Alert },
            { MobState.CHASE, titanai.Chase },
            { MobState.FIGHT, titanai.Fight },
            { MobState.DAMAGED, titanai.Damaged },
            { MobState.RETURN, titanai.Return },
            { MobState.DIE, titanai.Die }
        };
        AnimActionDict = InitializeAnimActionDict();
        GetDieingComponent = transform.GetComponent<GetDieing>();
        TargetSystemGO = GameObject.Find("EnemySearchSphere");
        animator = GetComponent<Animator>();

    }
    // Start is called before the first frame update
    void Start()
    {
    }
    // Update is called once per frame
    void Update()
    {
        if (!titan.Ccontroller.isGrounded) titan.Ccontroller.Move(new Vector3(0f, -titan.gravity * 2f, 0f) * Time.deltaTime);
        DoStateMachine();
    }
    private void DoStateMachine()
    {
        currentAnimState = animator.GetCurrentAnimatorStateInfo(0);
        AnimActionDict[titanai.currentState]();
        MobState nextState = stateActionDict[titanai.currentState]();
        titanai.lastState = titanai.currentState;
        if (titanai.cutinState == MobState.NOTSTATE) titanai.currentState = nextState;
        else
        {
            titanai.currentState = titanai.cutinState;
            titanai.cutinState = MobState.NOTSTATE;
        }
        //Debug.Log(gameObject.name + titanai.currentState);
    }
    private IEnumerator Smash(Action<float> attact, float reduction)
    {
        float originaldamage = titan.baseAD;
        float damage = Mathf.Clamp(0f, originaldamage * (1 - reduction), originaldamage);
        attact(damage);
        yield break;
    }
    private IEnumerator DoubleStrike(Action<float> attact, float reduction)
    {
        float originaldamage = titan.baseAD;
        float damage = Mathf.Clamp(0f, originaldamage * (1 - reduction), originaldamage);
        attact(damage);
        yield break;
    }
    private IEnumerator TripleStrike(Action<float> attact, float reduction)
    {
        float originaldamage = titan.baseAD;
        float damage = Mathf.Clamp(0f, originaldamage * (1 - reduction), originaldamage);
        attact(damage);
        yield break;
    }
    private void AlertStand()
    {
        if (titanai.currentState != MobState.ALERT) return;
        if (AlertTimer >= titanai.attackTimeSpan) titanai.isActionDone = true;
    }
    private void AlertMoveback()
    {
        if (titanai.currentState != MobState.ALERT) return;

        if (AlertTimer == 0)
        {
            animator.SetBool("Moveback", true);
            AlertTimer += 0.001f; //加一個微小時間避免animator偵測狀態卡住
            AlertmovePos = transform.position - transform.forward * 4f;
        }

        if (currentAnimState.IsName("Moveback") && !animator.IsInTransition(0))
        {
            if (Mathf.Sign(Vector3.Dot(transform.forward, AlertmovePos - transform.position)) >= 0 || titanai.isHitWall)
            {
                animator.SetBool("Moveback", false);
            }
            else titan.Ccontroller.Move(-transform.forward * titan.moveSpeedDict[MobState.CHASE] * Time.deltaTime);
        }

        if (AlertTimer >= titanai.attackTimeSpan) titanai.isActionDone = true;
    }

    private void AlertThreaten()
    {
        if (titanai.currentState != MobState.ALERT) return;

        if (AlertTimer == 0)
        {
            animator.SetTrigger("Threaten");
            AlertTimer += 0.001f; //加一個微小時間避免animator偵測狀態卡住
        }
        if (AlertTimer >= titanai.attackTimeSpan) titanai.isActionDone = true;
    }

    private Dictionary<MobState, Action> InitializeAnimActionDict()
    {
        return new Dictionary<MobState, Action>()
        {
            { MobState.IDLE, () =>
                {
                    if(titanai.lastState != titanai.currentState)
                    {
                        animator.SetFloat("Speed", animSpeedParam);
                    }
                }
            },
            { MobState.ALERT, () =>
                {
                    if(titanai.lastState != titanai.currentState)
                    {
                        AuxiliaryTool.Instance.ResetAnimatorTriggers(animator,"Smash", "DoubleStrike","TripleStrike", "getHit", "Rage");
                        animator.SetFloat("Speed", animSpeedParam);
                        AlertTimer = 0f;

                        if(titanai.aimingGo != null)
                        {
                            SelectAttackMoves();
                            SelectAlertForms();
                        }
                        else alertbehaviour = MobAlertBehaviour.STAND;
                    }
                    dict_alertbehaviour[alertbehaviour]();

                    if (currentAnimState.IsName("Locomotion") && !animator.IsInTransition(0))  
                        AlertTimer += Time.deltaTime;
                }
            },
            { MobState.CHASE, () =>
                {
                    animSpeedParam += 0.2f;
                    animator.SetFloat("Speed", animSpeedParam);
                }
            },
            { MobState.FIGHT, () => //Fight
                {
                    animator.SetFloat("Speed", animSpeedParam);
                    if(titanai.lastState != titanai.currentState)
                    {
                        titanai.attackTimeSpan = titanai.maxAttackTimeSpan;
                         AuxiliaryTool.Instance.ResetAnimatorTriggers(animator,"getHit");
                         if(!currentAnimState.IsTag("Attack") && !animator.IsInTransition(0))
                            animator.SetTrigger(attackForm.ToString());
                    }
                }
            },
            { MobState.DAMAGED, () => //Damaged
                {
                    if(titanai.lastState != titanai.currentState)
                    {
                        titanai.attackTimeSpan -= 0.8f;
                        animator.SetBool("Moveback", false);
                        animator.SetTrigger("getHit");
                        EnableWeapon(false);
                    }
                }
            },
            { MobState.RETURN, () => //Return
                {
                    AuxiliaryTool.Instance.ResetAnimatorTriggers(animator,"getHit");
                     animator.SetBool("Moveback", false);
                    animSpeedParam += 0.1f;
                    animator.SetFloat("Speed", animSpeedParam);
                }
            },
            { MobState.DIE, () => //Die
                {
                    if(titanai.lastState != titanai.currentState)
                    {
                        EnableWeapon(false);
                        AuxiliaryTool.Instance.ResetAnimatorTriggers(animator,"getHit", "Rage");
                        animator.SetBool("Moveback", false);
                        animator.SetBool("isDead",true);
                        TargetSystemGO.SendMessage("RemoveMob", titan);
                        if (isTwinsAnottherDead) OnTwinsDie();
                        else TwinsAnotherGO.SendMessage("OnAnotherTwinsDead");
                        if(RageEffectGo != null) Destroy(RageEffectGo);
                        removeBurning(1);
                        StopAllCoroutines();
                        StartCoroutine( AuxiliaryTool.Instance.CoroutineWaiter(1f,() => GetDieingComponent.enabled = true));
                        titan.Ccollider.enabled = false;
                        titan.Ccontroller.enabled = false;
                        this.enabled = false;
                    }
                }
            },
        };
    }
    private void OnTriggerEnter(Collider other)
    {
    }
    private void UnderAttack(object[] obj)
    {
        Vector3 HitDir = (obj[0] as Transform).position;
        Func<Action<float>, float, IEnumerator> HitDel = obj[1] as Func<Action<float>, float, IEnumerator>;
        titanai.currentState = MobState.DAMAGED;

        StartCoroutine(HitDel(damage => titan.GetHit(damage), 0));
        titanai.GetHitDir = HitDir - gameObject.transform.position;
        //Debug.Log(troll.baseHP);
    }
    void OnAnimationAttackStart()
    {
        EnableWeapon(true);
    }
    void OnAnimationAttackEnd()
    {
        EnableWeapon(false);
    }
    private void OnAnimationDone(string state)
    {
        if (state != titanai.currentState.ToString()) return;
        titanai.isActionDone = true;
    }
    private void OnHitEffect(Collider col)
    {
        string HitSoundEffect = col.GetComponent<IAttackableCharacter>().UnderAttackSoundEffect();
        AudioManager.instance.Play(HitSoundEffect);
    }

    private void AttackOnHit(object[] obj)
    {
        Collider col = obj[0] as Collider;
        string Hittag = obj[1] as string;

        if (!titanai.opposingGoTag.Contains(col.tag)) return;
        if (Hittag == "Weapon")
        {
            col.SendMessage("UnderAttack", new object[2] { transform, dict_attackDamage[attackForm] });
            OnHitEffect(col);
            EnableWeapon(false);
        }
    }
    private void SelectAlertForms()
    {
        if (titanai.aimingGo == null) return;

        if (titanai.lastState == MobState.IDLE)
        {
            alertbehaviour = MobAlertBehaviour.THREATEN;
            return;
        }

        int rnd = UnityEngine.Random.Range(0, 6);

        switch (rnd)
        {
            case 0:
                alertbehaviour = MobAlertBehaviour.THREATEN;
                break;
            case 1:
                alertbehaviour = MobAlertBehaviour.STAND;
                break;
            default:
                alertbehaviour = MobAlertBehaviour.MOVEBACK;
                break;
        }
    }

    private void SelectAttackMoves()
    {
        if (titanai.aimingGo == null) return;
        attackForm = (TitanAttackForm)UnityEngine.Random.Range(0, 3);
        titanai.attackRadius = dict_attackRadius[attackForm];
    }
    public void CallCharacterRegistrar(object[] obj)
    {
        GameCharacter gc = obj[0] as GameCharacter;
        bool bo = (bool)obj[1];
        if (bo) titan.Register(gc);
        else titan.Unregister(gc);
    }
    private void AddWeaponGameObject(Collider col) => weaponcollider.Add(col);

    private void EnableWeapon(bool bo)
    {
        foreach (Collider col in weaponcollider)
        {
            col.enabled = bo;
        }
    }
    private void OnOpposingGoDie(GameObject go)
    {
        StartCoroutine(AuxiliaryTool.Instance.CoroutineWaiter(1f,
            () =>
            {
                if (titanai.aimingGo == go) titanai.aimingGo = null;
                titanai.currentState = MobState.RETURN;
                AuxiliaryTool.Instance.ResetAnimatorTriggers(animator, "getHit");
            }));
    }
    private void Ambush(object[] obj)
    {
        triggerGo = obj[0] as GameObject;
        StartCoroutine(AmbushMove(obj[1] as GameObject));
        titanai.alertRadius = 30f;
        AudioManager.instance.Play("37_Titan_Scream");
    }
    private IEnumerator AmbushMove(GameObject aimGo)
    {
        float moveSpeed = 0f;
        for (int i = 0; i < 70; i++)
        {
            moveSpeed += 0.2f;
            animator.SetFloat("Speed", moveSpeed);
            titan.Ccontroller.Move(transform.forward * 20f * Time.deltaTime);
            yield return null;
        }
        titanai.alertRadius = 25f;
        titanai.aimingGo = aimGo;
        titanai.cutinState = MobState.ALERT;
    }
    private void OnTwinsDie()
    {
        if(triggerGo != null) triggerGo.SendMessage("DisableSealGate");
    }
    private void GetBurning()
    {
        GameObject go = Instantiate(EffectManager.Instance.GetBurning);
        BurnigGos.Add(go);
        go.transform.parent = transform;
        go.transform.position = titan.Ccollider.bounds.center;

        StartCoroutine(AuxiliaryTool.Instance.CoroutineWaiter(5f, () =>
        {
            if (BurnigGos.Contains(go))
            {
                BurnigGos.Remove(go);
                Destroy(go);
            }
        }));
    }
    private void removeBurning(float destorytime = 0f)
    {
        if (BurnigGos.Count > 0)
        {
            foreach (var go in BurnigGos)
            {
                if (go != null) Destroy(go);
            }
            BurnigGos.Clear();
        }
    }
    private void OnAnotherTwinsDead()
    {
        isTwinsAnottherDead = true;
        titan.baseAD = 2f;
        AlertTimer = -10f; //先進入Alert等待狂暴
        titanai.maxAttackTimeSpan = 0.8f;
        titanai.cutinState = MobState.ALERT;

        StartCoroutine( AuxiliaryTool.Instance.CoroutineWaiter(0.5f,
            () => {
                animator.SetTrigger("Rage");
                AudioManager.instance.Play("38_Titan_Rage");
                AlertTimer = 0;
                titan.RestoreHP(8f);
                titanai.attackTimeSpan = titanai.minAttackTimeSpan;
                RageEffectGo = Instantiate( EffectManager.Instance.TitanRageEffect, transform.position, Quaternion.identity, transform);
            })) ;
    }

    public GameCharacter ReturnGameCharacter()
    {
        return titan;
    }
    public string UnderAttackSoundEffect()
    {
        return titan.m_strUnderAttackSoundEffect;
    }
}