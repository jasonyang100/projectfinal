﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class SceneChange : MonoBehaviour
{
    private string mainMenu = "Main_Menu";
    private string openingScene = "OpeningScene";
    private string scene_A = "Scene_A";
    private string scene_B = "Scene_B";
    private string gameOver = "GameOver";
    private bool isMenuOpen = false;
    private string scene;

    GameObject sceneManager;
    SSceneManager sSceneManager;
    SaveDate saveData;
    PlayerMove playMove;
    [SerializeField] GameObject mainOptionMenu;
    [SerializeField] GameObject sceneAB_OptionMenu;
    private void Awake()
    {
        string scene = SceneManager.GetActiveScene().name;
        if (scene == mainMenu)
        {
            if (GameObject.Find("MainOptionMenu"))
                mainOptionMenu = GameObject.Find("MainOptionMenu");
            sceneAB_OptionMenu = null;
        }
        else if (scene == scene_A || scene == scene_B)
        {
            if (GameObject.Find("SceneAB_OptionMenu"))
                sceneAB_OptionMenu = GameObject.Find("SceneAB_OptionMenu");
            mainOptionMenu = null;
        }
        else
        {            
            sceneAB_OptionMenu = null;
            mainOptionMenu = null;
        }
    }
    private void Start()
    {
        if (GameObject.Find("SceneManager"))
            sceneManager = GameObject.Find("SceneManager");
        sSceneManager = sceneManager.GetComponent<SSceneManager>();
        if(mainOptionMenu!=null)
            mainOptionMenu.SetActive(false);
        if(sceneAB_OptionMenu!=null)
            sceneAB_OptionMenu.SetActive(false);

        scene = SceneManager.GetActiveScene().name;
    }
    public void NewGame()
    {
        AudioManager.instance.Play("6_UI_OptionSelectedButton");
        //sSceneManager.FadeToScene(scene_A);
        GameManager.g_instance.isGameStart = true;
        sSceneManager.FadeToScene(openingScene);
    }
    public void SaveGame()
    {
        saveData.SaveGame();
    }
    public void LoadGame()
    {
        AudioManager.instance.Play("6_UI_OptionSelectedButton");
        sSceneManager.FadeToScene(scene_B);
        GameManager.g_instance.isGameStart = true;
    }
    
    public void BackToMenu()
    {
        AudioManager.instance.Play("6_UI_OptionSelectedButton");
        sSceneManager.FadeToScene(mainMenu);
        GameManager.g_instance.isGameStart = true;
        Time.timeScale = 1f;
    }
    public void OptionButton()
    {
        mainOptionMenu.SetActive(true);
    }
    public void BackButton()
    {
        mainOptionMenu.SetActive(false);
    }
    public void ExitGame()
    {
        Application.Quit();
    }
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape)) 
        {
            if (scene == scene_A || scene == scene_B)
            {
                ControlOptionMenu(isMenuOpen);
            }
        }           
    }
    private void ControlOptionMenu(bool b)
    {
        if (!b)
        {
            sceneAB_OptionMenu.SetActive(true);
            Time.timeScale = 0f;
            isMenuOpen = true;
        }
        else
        {
            sceneAB_OptionMenu.SetActive(false);
            Time.timeScale = 1f;
            isMenuOpen = false;
        }
    }
    public void BackToGame()
    {
        ControlOptionMenu(true);
        //sceneAB_OptionMenu.SetActive(false);
        //Time.timeScale = 1;
    }
    public void GameOver()
    {
        sSceneManager.FadeToScene(gameOver);
        GameManager.g_instance.isGameStart = true;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            GameManager.g_instance.isGameStart = false;
            GameObject.Find("Character Warrior").GetComponent<PlayerMove>().enabled = false;
            sSceneManager.FadeToScene(scene_B);
        }
    }
}
