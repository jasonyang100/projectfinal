﻿using EnumsManager;
using UnityEngine;

public class NonPlayer : GameCharacter
{
    public string[] talks;
    public NPCType npcType { protected set; get; } = NPCType.FriendlyNPC;
    public bool CanInteraction = true;
    public bool CanInteractionRepeat = true;
    public Animator CAnimator;
    public NonPlayer(GameObject go) : base(go)
    {
    }
}