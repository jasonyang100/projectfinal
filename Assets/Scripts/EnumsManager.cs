﻿namespace EnumsManager
{
    public enum CharacterName
    {
        Player,
        Troll,
        Titan,
        Golem,
        WildHunt,
        Aerith,
        Treasure
    }

    public enum EquipmentType
    {
        WEAPON,
        SUIT,
    }
    public enum NPCType
    {
        Mob,
        FriendlyNPC,
        Item,
    }
    public enum InteractionState
    {
        Start,
        Interacting,       
    }

    public enum TargetingMode
    {
        Normal,
        Fight,
        NonMobNPCInteractive
    }

    public enum MobState
    {
        IDLE,
        WANDER,
        ALERT,
        CHASE,
        FIGHT,
        DEFENSE,
        DAMAGED,
        RETURN,
        DIE,
        NOTSTATE = 100
    }
    public enum MobAlertBehaviour
    {
        STAND,
        MOVEAROUND,
        MOVEBACK,
        MOVETO,
        THREATEN,
    }

    public enum TrollAttackForm
    {
        JumpAttack,
        Smash,
        DoubleStrike,
    }
    public enum TitanAttackForm
    {
        Smash,
        DoubleStrike,
        TripleStrike,
    }
    public enum GolemAttackForm
    {
        RocketFist,
        Smash,
        DoubleStrike,
        GroundImpact
    }
    public enum WildHuntAttackForm
    {
        NormalFiveChop,
        FlashFiveChop,
        SpinFlashLeap,
        SwordShock,
    }
    public enum PlayerAttackForm
    {
        NormalAttack,
        Flame,
        DashAttack,
    }
    public enum PlayerSkillType 
    {
        Shield,
        Flame,
        Flash,
    }
    public enum MobNormalStateType
    {
        IdleOnly,
        Partol,
        Wander
    }
}