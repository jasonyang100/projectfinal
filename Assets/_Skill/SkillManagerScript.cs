﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkillManagerScript : MonoBehaviour
{
    private enum m_enum
    {
        shield = 0,
        ignis = 1,
        flash = 2,
    }

    public GameObject PlayerGameObject;

    public KeyCode m_KeyCode;
    public bool IsActive;
    public bool IsRecastable=false;//unused in this project
    public Vector3 PositionOffset;
    public int NowSkillId;
    [SerializeField] private float DeployTime;
    [SerializeField] private float _DeployTime;
    [SerializeField] private float InputDelayTime;
    [SerializeField] private float _InputDelayTime;
    public float DeployDelayTime;
    [SerializeField] float _DeployDelayTime;
    [SerializeField] private GameObject SkillCarrierGameObject;


    private List<SkillData> SkillsList;
    private static AllSkillsData allSkillsData;
    private int TotalSkillNumber;

    private void Awake()
    {
        transform.position = PlayerGameObject.transform.position;
        transform.parent = PlayerGameObject.transform;//stick on player GO;
        transform.position += PositionOffset;
        allSkillsData = new AllSkillsData();
        SkillsList = allSkillsData.AllData;
        TotalSkillNumber = SkillsList.Count;

        foreach (SkillData sd in SkillsList)// spawn skill carriers
        {
            GameObject go;
            go = Instantiate(sd.SkillCarrierGameObject);
            go.transform.position = transform.position;
            go.transform.position += sd.SkillPositionOffset;
            go.transform.parent = transform;
            go.name = sd.SkillName;
            go.SetActive(false);
        }

    }
    // Start is called before the first frame update
    void Start()
    {
        //transform.position = PlayerGameObject.transform.position;
        //transform.parent = PlayerGameObject.transform;//stick on player GO;
        //transform.position += PositionOffset;        
        //allSkillsData = new AllSkillsData();
        //SkillsList = allSkillsData.AllData;
        //TotalSkillNumber=SkillsList.Count;

        //foreach (SkillData sd in SkillsList)// spawn skill carriers
        //{
        //    GameObject go;
        //    go = Instantiate(sd.SkillCarrierGameObject);
        //    go.transform.position = transform.position;
        //    go.transform.position += sd.SkillPositionOffset;
        //    go.transform.parent = transform;
        //    go.name = sd.SkillName;
        //    go.SetActive(false);
        //}
        SwitchSkill(NowSkillId);

        IsActive = false;
        SkillCarrierGameObject.SetActive(false);


    }

    // Update is called once per frame
    void Update()
    {
        deploySkill(OnDeploySkillKey());//triggered
        onSwitchSkillKey();

    }



    void SwitchSkill(int _nowSkillId)
    {
        DeployTime = SkillsList[_nowSkillId].DeployTime;
        InputDelayTime = SkillsList[_nowSkillId].InputDelayTime;
        DeployDelayTime = SkillsList[_nowSkillId].DeployDelayTime;
        SkillCarrierGameObject = transform.GetChild(_nowSkillId).gameObject;
        //Debug.Log(transform.GetChild(_nowSkillId).gameObject.name==transform.Find(SkillsList[_nowSkillId].SkillName).gameObject.name);
    }

    void onSwitchSkillKey()
    {
        if (Input.GetKeyDown(KeyCode.Y))
        {
            if (!IsActive)
            {
                NowSkillId++;
                NowSkillId = (NowSkillId >= 0) ? (NowSkillId < TotalSkillNumber) ? NowSkillId : 0 : TotalSkillNumber - 1;//remap nowskillId
                SwitchSkill(NowSkillId);//change

                if (!false) //should it be reset after changeing skill?
                {
                    _InputDelayTime = 0;
                }
            }
        }
    }


    public bool OnDeploySkillKey()
    {
        if (_InputDelayTime <= 0)
        {
            if (Input.GetKeyDown(m_KeyCode))
            {
                if (!IsRecastable)
                {
                    IsActive = !IsActive;
                }
                else if (IsRecastable)
                {
                    _DeployTime = DeployTime;// refresh remained time
                    IsActive = true;

                }

                _InputDelayTime = InputDelayTime;//reset timer if the  skill is not actived
            }

        }
        _InputDelayTime -= Time.deltaTime;
        return IsActive;
    }
    public void deploySkill(bool _isActive)// turn on the skill
    {
        if (_isActive)
        {

            _DeployTime -= Time.deltaTime;
            if (_DeployTime < 0)
            {
                IsActive = !_isActive;//turn off the skill

            }
        }
        else _DeployTime = DeployTime; //reset the timer if skill is not triggered
        SkillCarrierGameObject.SetActive(DeployTimeDelayControl(IsActive));

    }

    bool DeployTimeDelayControl(bool _bool)
    {
        if (_bool)
        {
            _DeployDelayTime -= Time.deltaTime;
            if (_DeployDelayTime < 0)
            {

                return true;


            }

        }
        else _DeployDelayTime = DeployDelayTime;
        return false;
    }



}
     
public class SkillData
{
    public string SkillName;
    public int SkillId;
    public float InputDelayTime;
    public float DeployTime;
    public float DeployDelayTime;
    public GameObject SkillCarrierGameObject;
    public Vector3 SkillPositionOffset;
    
}

class AllSkillsData
{
    public  List<SkillData> AllData;
    

    public AllSkillsData()
    {
        AllData = new List<SkillData>();
        AllData.Add(SetskillUp("Shield", 0, 3f, 2f,0.2f, Resources.Load<GameObject>("Shield_Go"), new Vector3(0f, 0f, 0f)));
        AllData.Add(SetskillUp("Ignis", 1, 3f, 2f, 0.2f, Resources.Load<GameObject>("Ignis_Go"), new Vector3(0f, 1f, 4f)));
        AllData.Add(SetskillUp("Flash", 2, 3f, 1f, 0.5f, Resources.Load<GameObject>("Flash_Go"), new Vector3(0f, -3f, 0f)));
    }


    SkillData SetskillUp(string _skillName,int _skillId,float _inputDelayTime,float _deployTime, float _deploydelayTime,GameObject _gO,Vector3 _skillPositionOffset)
    {
        SkillData skilldata = new SkillData();
        skilldata.SkillName = _skillName;
        skilldata.SkillId = _skillId;
        skilldata.DeployTime = _deployTime;
        skilldata.InputDelayTime = _inputDelayTime;
        skilldata.DeployDelayTime = _deploydelayTime;
        skilldata.SkillCarrierGameObject = _gO;
        skilldata.SkillPositionOffset = _skillPositionOffset;


        return skilldata;
    }
}
    

 



