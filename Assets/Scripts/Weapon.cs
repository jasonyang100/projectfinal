﻿using UnityEngine;
using EnumsManager;
using System.Collections.Generic;

public class Weapon : MonoBehaviour
{
    Collider Weaponcollider;
    private void Awake()
    {
        Weaponcollider = gameObject.GetComponent<Collider>();
        transform.root.SendMessage("AddWeaponGameObject", Weaponcollider);
    }
    private void Start() => Weaponcollider.enabled = false;
    private void OnTriggerEnter(Collider other) => transform.root.SendMessage("AttackOnHit", new object[2] { other, gameObject.tag });
}
