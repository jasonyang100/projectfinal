﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TitanTrigger : MonoBehaviour
{
    private List<GameObject> Titans = new List<GameObject>();
    private GameObject sealgate;
    private Material SealGateMaterial;

    private void Awake()
    {
        Titans.Add( GameObject.Find("Titan1") );
        Titans.Add( GameObject.Find("Titan2") );
        sealgate = GameObject.Find("SealArea_Cube_Titan");
        SealGateMaterial = sealgate.GetComponent<Renderer>().material;
    }
    private void Start()
    {
        foreach (var tit in Titans)
        {
            tit.SetActive(false);
        }

        SealGateMaterial.SetFloat("_Radius", 12f);
        SealGateMaterial.SetInt("_IsActive", 1);
    }
    private void OnTriggerEnter(Collider other)
    {
        if (!other.CompareTag("Player")) return;

        foreach(var tit in Titans)
        {
            tit.SetActive(true);
            tit.SendMessage("Ambush", new object[2] { gameObject, other.gameObject});
        }
        StartCoroutine(AuxiliaryTool.Instance.CoroutineWaiter(0.4f, () => NPCinteraction.Instance.OnSituationTalk("該死，是魔法結界", 2f)));
        sealgate.GetComponent<Collider>().isTrigger = false;
        GetComponent<Collider>().enabled = false;
        StartCoroutine(AuxiliaryTool.Instance.CoroutineWaiter(1f, () => SealGateMaterial.SetInt("_IsActive", 0)));
    }
    private void DisableSealGate()
    {
        Destroy(sealgate);
        Destroy(this);
    }
}
