﻿using System.Collections;
using System.Collections.Generic;
using System.Data.SqlTypes;
using System.Security.Permissions;
using UnityEngine;
using UnityEngine.UI;

public class CircleMenu : MonoBehaviour
{
    public List<MenuButton> buttons = new List<MenuButton>();
    private Vector2 MousePosition;
    private Vector2 fromVector2x = new Vector2(0.5f, 0.1f);
    private Vector2 centerCircle = new Vector2(0.5f, 0.5f);
    private Vector2 toVector2M;

    public int menuItems;
    public int curMenuItem;
    private int oldMenuItem;

    void Start()
    {
        menuItems = buttons.Count;
        foreach(MenuButton button in buttons)
        {
            button.sceneImage.color = button.normalColor;
        }
        curMenuItem = 0;
        oldMenuItem = 0;
    }
    void Update()
    {
        GetCurrentMenuItem();
        if (Input.GetMouseButtonDown(0))
        {
            ButtonAction();
        }

    }
    public void GetCurrentMenuItem()
    {
        MousePosition = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
        toVector2M = new Vector2(MousePosition.x / Screen.width, MousePosition.y / Screen.height);

        float angle =
            (Mathf.Atan2(fromVector2x.y - centerCircle.y, fromVector2x.x - centerCircle.x) -
            Mathf.Atan2(toVector2M.y - centerCircle.y, toVector2M.x - centerCircle.x) * Mathf.Rad2Deg);
        if (angle < 0)
            angle += 360;

        curMenuItem = (int)(angle / (360 / menuItems));
        if (curMenuItem != oldMenuItem)
        {
            buttons[oldMenuItem].sceneImage.color = buttons[oldMenuItem].normalColor;
            oldMenuItem = curMenuItem;
            buttons[curMenuItem].sceneImage.color = buttons[curMenuItem].hightlightColor;
        }
        Debug.Log(angle);
    }
    public void ButtonAction()
    {
        buttons[curMenuItem].sceneImage.color = buttons[curMenuItem].pressColor;
        if (curMenuItem == 0)
        {
            print("123123132");
        }
    }
}
[System.Serializable]
public class MenuButton
{
    public string name;
    public Image sceneImage;
    public Color normalColor = Color.white;
    public Color hightlightColor = Color.gray;
    public Color pressColor = Color.gray;
}
