﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class GetDieing : MonoBehaviour
{
    [SerializeField] GameObject Dieing_Go;//template
    [SerializeField] GameObject targetGo;
    [SerializeField] float Timer = 6f;
    float _Timer;
    [SerializeField] string Targetname;
    [SerializeField] Material dissolveMaterial;
    [SerializeField] bool Isactive;

    [SerializeField] Renderer[] TargetRenderers;
    [SerializeField] Material[] TargetMaterials;


    [SerializeField] Material[] dummyMaterials;
    [SerializeField] Material _dissolveMaterial;

    [SerializeField] float dissolveSpeed = 0.3f;


    Vector3 PostionOffset;
    
    // Start is called before the first frame update
    void Awake()
    {
        _Timer = Timer;
        Targetname = AnalysisMobName(targetGo);



        GetMobsMaterials();


        _dissolveMaterial = new Material(dissolveMaterial);

        dissolveMaterial.SetFloat("_MaskHeight",0f);

    }

    // Update is called once per frame
    void Update()
    {
        //_Timer -= Time.deltaTime;
        //if (_Timer < 0)
        //{
        //    EndBurning();
        //}

        if (Isactive)
        {
            _Timer -= Time.deltaTime;
            if (_Timer >= 0)
            {
                SkinGettingDark();
            }
            
            
            if (_Timer < 0)
            {
               DeployDissloveEffect();
            }
           
        }


    }



    string AnalysisMobName(GameObject inputGo)
    {
        string s = inputGo.gameObject.name;
        string[] ss = s.Split(' ');


        foreach(var sv in ss)
        {

            return sv;
        }
        return "failure";
    }



    void GetMobsMaterials()
    {
        if (TargetRenderers != null)
        {
            TargetRenderers = this.gameObject.GetComponentsInChildren<Renderer>();
            var temp = new List<Material>();
            foreach (Renderer rd in TargetRenderers)
            {
                temp.AddRange(rd.materials);
            }
            TargetMaterials = temp.ToArray();
        }
       
        
        
        



        

    }

    void ReplaceMaterials()
    {
        
    }


    void SkinGettingDark()
    {
        //var material = TargetMaterials[TargetMaterials.Length - 1];

        //float maskheight = material.GetFloat("MaskHeight");

        //material.SetInt("isActive",1);// 0=false, 1 = true
        //var material = TargetMaterials[0];
        //Vector4 color = material.GetColor("_BaseColor");

        //color = Vector4.Lerp(color, Vector4.zero,0.1f*Time.deltaTime);//
        //material.SetColor("_BaseColor",color);

        foreach(var m in TargetMaterials)
        {
            if (m.HasProperty("_BaseColor"))
            {
                Vector4 color = m.GetColor("_BaseColor");
                color = Vector4.Lerp(color, Vector4.zero, 0.75f * Time.deltaTime);
                m.SetColor("_BaseColor", color);
            }

            if(m.HasProperty("_EmissionColor"))
                m.SetColor("_EmissionColor", Color.Lerp(m.GetColor("_EmissionColor"), new Color(0f, 0f, 0f), 0.75f * Time.deltaTime));

            if (m.HasProperty("_Smoothness"))
                m.SetFloat("_Smoothness", Mathf.Lerp(m.GetFloat("_Smoothness"),0f, dissolveSpeed * Time.deltaTime));
        }
        //attachSomke();
    }
    void DeployDissloveEffect()
    {
        _dissolveMaterial.SetFloat("_MaskHeight", Mathf.Lerp(_dissolveMaterial.GetFloat("_MaskHeight"), 1f, dissolveSpeed * Time.deltaTime));
        if (_dissolveMaterial.GetFloat("_MaskHeight") > 0.78f)
        {
            _dissolveMaterial.SetFloat("_MaskHeight", 1f);
        }
        foreach (var rd in TargetRenderers)
            {
                var ms = rd.materials;
                var temp = new List<Material>();
                foreach (var m in ms)
                {
                    temp.Add(_dissolveMaterial);
                }
                rd.materials = temp.ToArray();
        }
        
    }

    void attachSomke()
    {
        if (Dieing_Go != null)
        {
            var temp = Instantiate(Dieing_Go, transform.position, Quaternion.identity, transform);
        }

        Dieing_Go = null;
    }


    void SmallScaleMob()
    {

    }
    void MediumScaleMob()
    {

    }

    void EndBurning()
    {
        //Destroy(go);
        Destroy(this);

    }
}
