﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BloodEffect : MonoBehaviour
{
    public GameObject bloodEffect;
    public GameObject timeStop;

    int layerMask = 1 << 8;
    Animator father_Animator;
    AnimatorStateInfo stateInfo;
    TimeStop freeze;

    void Start()
    {
        if (GameObject.Find("TimeStop"))
            timeStop = GameObject.Find("TimeStop");


        freeze = timeStop.GetComponent<TimeStop>();

        father_Animator = GetComponentInParent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        stateInfo = father_Animator.GetCurrentAnimatorStateInfo(0);
    }
    private void OnTriggerEnter(Collider other)
    {
        bool attack = Attack();
        if (other.CompareTag("Mob"))
        {

            Ray ray = new Ray(transform.position, -transform.forward);
            RaycastHit hit;

            if (attack)
            {
                if (Physics.Raycast(ray, out hit, layerMask))
                {
                    freeze.Freeze();
                    Vector3 pos = hit.point;
                    GameObject blood = Instantiate(Resources.Load<GameObject>("BloodEffect"));
                    blood.gameObject.transform.position = pos;
                    blood.transform.LookAt(transform.position);
                    blood.transform.rotation = Quaternion.Euler(blood.transform.rotation.eulerAngles * -1f);
                    Destroy(blood, 1.5f);
                }
            }
        }
    }
    private bool Attack()
    {
        if (stateInfo.IsName("Attack1") || stateInfo.IsName("Attack2")
            || stateInfo.IsName("Attack3") || stateInfo.IsName("Attack5")
            || stateInfo.IsName("DashAttack") || stateInfo.IsName("DashAttack 0"))
        {
            return true;
        }
        else
            return false;
    }
}
