﻿using JetBrains.Annotations;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using Random = UnityEngine.Random;


public class PlayerMove : MonoBehaviour
{
    [SerializeField] Transform groundCheck;                   //角色 著地點
    [SerializeField] Transform followObj;
    [SerializeField] Transform lookTarget;
    [SerializeField] Transform lookTarget2;
    [SerializeField] TargetingSystem targetingSystem;
    [SerializeField] GameObject _playerWeapon;                //刀(腰上)
    [SerializeField] GameObject playerWeapon;                 //刀(手上)

    AudioManager audioManager;
    GameManager saveDate;
    Transform camera;

    CameraShake cameraShake;
    SceneChange sceneChange;

    [SerializeField] float jumpForce = 3.0f;                  //跳躍力道
    [SerializeField] float rotateSpeed = 6.0f;                //轉向速度

    private const float maxDashTime = 0.8f;

    public float delayTime = 0.02f;
    private float dashStoppingSpeed = 0.1f;
    private float currentDashTime = 2f;
    private float dashSpeed = 200;

    private float gravity = -40.0f;
    private float groundDistance = 0.2f;
    private float v, h;
    private float _animspeed = 0f;
    private float attackTime = 0;

    private bool holdSword = false;
    private bool isGrounded;                          
    public bool attackMode = false;                
    public bool isDeadFadeOut ;

    private Vector3 velocity;
    private Vector3 moveDirection = Vector3.zero;
    private Vector3 vel;

    public LayerMask groundMask;

    CameraFollow cameraFollow;
    CharacterController playerController;
    public Animator animaController;
    public AnimatorStateInfo stateInfo;
    public int isAttackCom = 0;
    public int isMagicAttack = 0;

    public float AnimSpeed
    {
        get => _animspeed;
        set
        {
            if (value >= 2) value = 2;
            if (value <= 0) value = 0;
            _animspeed = value;
        }
    }

    private void Awake()
    {
        camera = Camera.main.transform;
        cameraFollow = FindObjectOfType<CameraFollow>();
        audioManager = FindObjectOfType<AudioManager>();
        cameraShake = FindObjectOfType<CameraShake>();
        saveDate = FindObjectOfType<GameManager>();
        sceneChange = FindObjectOfType<SceneChange>();
    }
    void Start()
    {
        if (GameObject.Find("LookTarget"))
            lookTarget = GameObject.Find("LookTarget").transform;
        if (GameObject.Find("LookTarget2"))
            lookTarget2 = GameObject.Find("LookTarget2").transform;
        if (GameObject.Find("FollowObj"))
            followObj = GameObject.Find("FollowObj").transform;
        followObj.position = lookTarget.position;

        if (GameObject.Find("GroundCheck"))
            groundCheck = GameObject.Find("GroundCheck").transform; //抓著地點座標
        if (GameObject.Find("PlayerWeapon"))
            playerWeapon = GameObject.Find("PlayerWeapon");
        if (GameObject.Find("Weapon0"))
            _playerWeapon = GameObject.Find("Weapon0");

        playerController = GetComponent<CharacterController>();
        animaController = GetComponent<Animator>();

        playerWeapon.SetActive(false);
        _playerWeapon.SetActive(true);
    }

    void Update()
    {
        animaController.SetInteger("isAttackCom", isAttackCom);
        animaController.SetBool("holdSword", holdSword);
        animaController.SetBool("AttackMode", attackMode);

        v = Input.GetAxis("Vertical");
        h = Input.GetAxis("Horizontal");

        stateInfo = animaController.GetCurrentAnimatorStateInfo(0);

        if (attackMode)
        {
            followObj.transform.position = Vector3.SmoothDamp(followObj.position, lookTarget.position, ref vel, 0.08f);
            followObj.transform.rotation = Quaternion.Slerp(followObj.rotation, lookTarget.rotation,
                4f * Time.deltaTime);
        }
        if (!attackMode)
        {
            if (Input.GetKey(KeyCode.E))
            {
                Vector3 newFollowPos;
                newFollowPos = lookTarget2.position;
                newFollowPos = Vector3.SmoothDamp(followObj.position, newFollowPos, ref vel,0.1f);
                followObj.transform.position = newFollowPos;
            }
            else
            {
                followObj.transform.position = Vector3.SmoothDamp(followObj.position, lookTarget.position, ref vel, 0.07f);
                followObj.transform.rotation = lookTarget.rotation;
            }
        }
        if (Input.GetKeyDown(KeyCode.Space) && isGrounded && stateInfo.IsName("NormalModeMove"))
        {   //跳躍
            velocity.y = Mathf.Sqrt(jumpForce * -2.0f * gravity);
            animaController.SetTrigger("isJump");
        }

        if ((Input.GetKeyDown(KeyCode.Z) && !holdSword))
        {   //拔劍
            holdSword = true;
            animaController.SetTrigger("isDrawSword");
        }
        else if (Input.GetKeyDown(KeyCode.Z) && holdSword && !attackMode)
        {   //收劍
            holdSword = false;
            animaController.SetTrigger("isDrawSword1");
        }
        else if (attackMode && holdSword && Input.GetKeyDown(KeyCode.Z))
        {
            holdSword = false;
            animaController.SetTrigger("isDrawSword1");
            attackMode = false;
        }
        if (!attackMode&&holdSword && Input.GetKeyDown(KeyCode.LeftShift))
        {    //開啟戰鬥模式
            attackMode = true;
        }
        else if(attackMode&&holdSword && Input.GetKeyDown(KeyCode.LeftShift))
        {    //關閉戰鬥模式
            attackMode = false;
        }

        //MagicAttackAndColdDown();

        if (Input.GetKeyDown(KeyCode.Space)&&(stateInfo.IsName("AttackModeMove")))
        {   //閃躲
            animaController.SetTrigger("Flee");
        }

        if (Input.GetMouseButtonDown(0)
    && (stateInfo.IsName("NormalModeMove") || stateInfo.IsName("PlayerIdle")) && holdSword)
        {
            playerWeapon.SetActive(true);
            _playerWeapon.SetActive(false);
            isAttackCom = Random.Range(1, 5);
        }
        else if (Input.GetMouseButtonDown(0) && (stateInfo.IsName("AttackModeMove") || stateInfo.IsName("AttackIdle")))
        {   //隨機普通攻擊
            FaceToEnemy();
            isAttackCom = Random.Range(1, 5);
            attackTime = 0;
        }
        if (isAttackCom != 0)
        {
            attackTime += Time.deltaTime;
            if (attackTime >= 0.1f)
            {
                attackTime = 0;
                isAttackCom = 0;
            }
        }

        if (Input.GetKeyDown(KeyCode.L))
        {
            NPCinteraction.Instance.OnInteractionEnd();
        }

        targetingSystem.TargetEnemy();
        targetingSystem.TargetNonMobNPC();
        OnNpcInteraction();

    }
    private void FixedUpdate()  
    {
        //切換 "普通模式" 與 "戰鬥模式"
        if (attackMode)
        {
            AttackModeMove();
            FaceToEnemy();
        }
        else if(!attackMode)
        {
            NormalModeMove();
            Jump();
        }
    }

    //=================正常模式================
    private void NormalModeMove()
    {
        if (targetingSystem.isInteractingWithNPC)
        {
            StopMoving();
            return;
        }
        if (v != 0 || h != 0)
            AnimSpeed += 0.07f;
        else
            AnimSpeed -= 0.13f;
        animaController.SetFloat("Speed", AnimSpeed);

        if (Input.GetKey(KeyCode.LeftShift) && (v != 0 || h != 0) && !holdSword)
        {   //走路
            AnimSpeed -= 0.12f;
            if (AnimSpeed <= 0.8)
                AnimSpeed = 0.8f;
        }

        Vector3 direction = camera.forward * v + camera.right * h;
        float directionLength = direction.magnitude;
        direction.y = 0;
        direction = direction.normalized * directionLength;

        if (direction != Vector3.zero )
        {
            moveDirection = Vector3.Slerp(moveDirection, direction, Time.deltaTime * 10);
            transform.rotation = Quaternion.LookRotation(moveDirection, Vector3.up);
        }
    }

    //=============戰鬥模式==============
    private void AttackModeMove()
    {
        if (targetingSystem.isInteractingWithNPC)
        {
            StopMoving();
            return;
        }

        if (v != 0 || h != 0)
        {
            AnimSpeed += 0.07f;
            if (AnimSpeed >= 1)
                AnimSpeed = 1;
        }
        else
            AnimSpeed -= 0.13f;

        animaController.SetFloat("Speed", AnimSpeed);
        Vector3 direction;
        direction.y = 0f;
        direction.z = v * Vector3.Dot(transform.forward, camera.forward) - h * Vector3.Dot(transform.right, camera.forward);
        direction.x = h * Vector3.Dot(transform.forward, camera.forward) + v * Vector3.Dot(transform.right, camera.forward);
        animaController.SetFloat("AttackModeSpeedV", direction.z, 0.1f, Time.deltaTime);
        animaController.SetFloat("AttackModeSpeedH", direction.x, 0.1f, Time.deltaTime);
    }

    //==============跳躍============
    private void Jump()
    {
        if (playerController.enabled == false) return;

        isGrounded = Physics.CheckSphere(groundCheck.position, groundDistance, groundMask);

        if (isGrounded && velocity.y < 0)
            velocity.y = -2f;
        velocity.y += gravity * Time.deltaTime;

        playerController.Move(velocity * Time.deltaTime);
    }

    //==============面向敵人============
    public void FaceToEnemy()
    {
        GameObject enemyPosition = targetingSystem.selectedMobTarget();

        if (enemyPosition != null && !(stateInfo.IsName("Flee")))
        {
            Vector3 targetDir = enemyPosition.transform.position - transform.position;
            Vector3 newDir = Vector3.RotateTowards(transform.forward, targetDir, rotateSpeed * Time.deltaTime, 0.0f);
            newDir.y = 0;
            transform.rotation = Quaternion.LookRotation(newDir);
        }
    }

    //=============衝刺================
    public IEnumerator DashMove()
    {
        playerWeapon.SetActive(true);
        _playerWeapon.SetActive(false);
        FaceToEnemy();
        currentDashTime = 0f;
        Vector3 gravityVector = new Vector3(0f, gravity, 0f);

        cameraFollow.MotionBlurEffect();
        while (currentDashTime < maxDashTime)
        {
            moveDirection = (transform.forward * dashSpeed) + gravityVector;
            currentDashTime += dashStoppingSpeed * Time.timeScale;

            //Debug.Log(currentDashTime);
            playerController.Move(moveDirection * Time.deltaTime);
            yield return null;
        }
        moveDirection.y = 0;
        cameraFollow.MotionBlurEffect1();
    }

    private void SwordHit()
    {
        AudioManager.instance.Play("14_Player_Sword");
    }
    private void FootStep()
    {
        AudioManager.instance.Play("10_Player_Step_Rock");
        AudioManager.instance.Play("11_Player_Step_Ground");
    }
    public void GameOver()
    {
        sceneChange.GameOver();
    }
    private void DrowSwordSoundEffect()
    {
        if (!holdSword)
        {
            AudioManager.instance.Play("12_Player_DropSword_1");
        }
    }
    private void DrowSwordSoundEffect2()
    {
        if (holdSword)
        {
            AudioManager.instance.Play("13_Player_DropSword_2");
        }
    }
    private void animaDrawSword1()
    {
        if (!holdSword)
        {
            playerWeapon.SetActive(false);
            _playerWeapon.SetActive(true);
        }
        else if (holdSword)
        {
            playerWeapon.SetActive(true);
            _playerWeapon.SetActive(false);
        }
    }
    private void PlayerFlshCast()
    {
        FaceToEnemy();
        holdSword = true;
        StartCoroutine( DashMove() );
    }

    private void OnNpcInteraction()
    {
        if (Input.GetKeyDown(KeyCode.C))
        {
            GameObject target = targetingSystem.selectedNonMobNPCTarget();

            if (target != null)
            {
                if (!targetingSystem.isInteractingWithNPC)
                {
                    if (!targetingSystem.isNonMobNPCinRange()) return;
                }
                NPCinteraction.Instance.OnNPCInteraction(target);
                Vector3 targetDir = target.transform.position - transform.position;
                targetDir.y = 0;
                transform.rotation = Quaternion.LookRotation(targetDir);
            }
        }
    }
    public void StopMoving()
    {
        attackMode = false;
        h = v = 0;
        animaController.SetFloat("Speed", 0);
    }
    private void OnDrawGizmos()
    {
        Gizmos.color = Color.green;
        Gizmos.DrawLine(transform.position, transform.position + transform.forward * 2);
    }
    public void IsDeadFadeOut()
    {
        isDeadFadeOut = true;
    }
}
