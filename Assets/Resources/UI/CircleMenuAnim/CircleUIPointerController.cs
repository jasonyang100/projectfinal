﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CircleUIPointerController : MonoBehaviour
{

    public GameObject meGameObject;
    //public float degree;
    //public Vector3 PointerDir;
    //public float Mousespeed;
    

    [SerializeField]private int _SectionNumber=-99;
    public int SectionNumber
    {
        get
        {
            return _SectionNumber;
        }
        set
        {
            //value %= totalSection;
            if (value <= -1 && value > -99)
                _SectionNumber = 4;
            else _SectionNumber = value>totalSection?0:value ;
        }
    }


    private int totalSection=5;
    private Vector3 screenCenter = new Vector3(Screen.width / 2, Screen.height / 2, 0);
    //private float _Xmouse;
    //private float _Ymouse;   

    //float xMouse
    //{
    //    get { return _Xmouse; }
    //    set
    //    {
    //        _Xmouse = (Mathf.Abs(_Xmouse + value) > 10 ? value > 0 ? 10 : -10 : value);
    //    }
    //}
    //float yMouse
    // {
    //    get { return _Ymouse; }
    //    set
    //    {
    //        _Ymouse = (Mathf.Abs(_Ymouse+value) > 10 ? value>0?10:-10 : value);
    //    }
    //}

    //private float xPosition;
    //private float yPosition;


    private enum m_enum
    {
        button1=-36,
        button2 = -36-72,
        button3 = -36-72-72,
        button4 = -36-72-72-72,
        button5 = 36

    } 

     
    
    
    

    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        //getAxis();
        
        PointerRotate(_SectionNumber);





    }

    //void getAxis()
    //{
    //    xMouse = Input.GetAxis("Mouse X");
    //    yMouse = Input.GetAxis("Mouse Y");

    //    degree = Mathf.Atan(xMouse / yMouse) * 180 / Mathf.PI;
    //}

    //void getPostition()
    //{
    //    xPosition = Input.mousePosition.x;
    //    yPosition = Input.mousePosition.y;

    //    PointerDir = (Input.mousePosition - screenCenter).normalized;
      

    //}

    void PointerRotate(int _MenuNumber)
    {
        switch (_MenuNumber)
        {
            case 0:
                meGameObject.transform.rotation = Quaternion.Euler(0f, 0f, (float)m_enum.button1);
                break;
            case 1:
                meGameObject.transform.rotation = Quaternion.Euler(0f, 0f, (float)m_enum.button2);
                break;
            case 2:
                meGameObject.transform.rotation = Quaternion.Euler(0f, 0f, (float)m_enum.button3);
                break;
            case 3:
                meGameObject.transform.rotation = Quaternion.Euler(0f, 0f, (float)m_enum.button4);
                break;
            case 4:
                meGameObject.transform.rotation = Quaternion.Euler(0f, 0f, (float)m_enum.button5);
                break;
            default:break;
                ;
                


        }

    }



}
