﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TeleportSkillScript : MonoBehaviour
{
    public GameObject NpcGameObject;
    [SerializeField] Renderer TargetRenderer;
    [SerializeField] Material[] TargetMaterials;
    [SerializeField] Material TargetMaterial;
    [SerializeField] Material BlinkMaterial_template;
    [SerializeField] GameObject TeleportParticle_template;
    [SerializeField] GameObject teleTeleportParticle;
    [SerializeField] bool IsActive;

    // Start is called before the first frame update
    void Awake()
    {
        //TargetRenderer = m_gameObject.GetComponent<Renderer>();
        TargetMaterials = TargetRenderer.materials;
        TargetMaterial = TargetMaterials[1];
        
        AttachTeleportParticle();

    }



    // Update is called once per frame
    void Update()
    {

    }

    void ActiveShaderEffect()
    {
        TargetMaterial.SetInt("_IsActive", 1);
    }
    void DeActiveShaderEffect()
    {
        TargetMaterial.SetInt("_IsActive", 0);
    }
    void ShaderEffectSwitch(bool _isactive)
    {
        if (_isactive)
        {
            ActiveShaderEffect();
        }
        else
        {
            DeActiveShaderEffect();
        }
    }

    void AttachTeleportParticle()// attch this gameobject to player
    {
        //transform.parent = NpcGameObject.transform;
        if (teleTeleportParticle == null)
        {
            teleTeleportParticle = Instantiate(TeleportParticle_template, NpcGameObject.transform.position, Quaternion.identity, NpcGameObject.transform);
            teleTeleportParticle.SetActive(false);
        }
        
    }
    private IEnumerator yyy()
    {
        for(int i = 0; i < 10; i++) 
        {
            //
            yield return null;
        }
    }



    void TeleportParticleSwitch(bool _isactive)
    {
        if (_isactive)
        {
            teleTeleportParticle.SetActive(true);
        }
        else
        {
            teleTeleportParticle.SetActive(false);
        }
    }

    private void OnEnable()
    {
        IsActive = true;
        TeleportParticleSwitch(IsActive);
        ShaderEffectSwitch(IsActive);


    }
    private void OnDisable()
    {

        IsActive = false;
        TeleportParticleSwitch(IsActive);
        ShaderEffectSwitch(IsActive);
    }





}
