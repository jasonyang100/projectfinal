﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAI : CharacterAI
{
    public bool isShielding;


    public PlayerAI(Player player) : base(player)
    {
    }

    public bool DetectMob()
    {
        return base.VisionDetect(character.Ctransform, alertRadius, SightRangeAngle, 4f, "Mob");
    }
}
