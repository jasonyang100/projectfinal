﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CircleUIManager : MonoBehaviour
{
    CircleUIPointerController circleUIPointerController;
    CircleUIAnimController[] circleUIAnimControllers;
    private int totalSection;
    private float ScrollWheelGauge;

   
    public GameObject SectionPointer;
    public float ScrollSpeed = 9.0f;     
    [SerializeField] private int _index = 0;
    public int index
    {
        get { return _index; }
        set { _index = value >= 0 ? value % totalSection : totalSection - 1; }
    }

    private bool isFirstSelect;







    // Start is called before the first frame update
    void Start()
    {
        circleUIPointerController = GetComponentInChildren<CircleUIPointerController>();
        circleUIAnimControllers = GetComponentsInChildren<CircleUIAnimController>();
        totalSection = circleUIAnimControllers.Length;
        ScrollWheelGauge = 0;
    }

    // Update is called once per frame
    void Update()
    {
        //Debug.Log(Input.GetAxis("Mouse ScrollWheel"));
        selectaSection();
        keyboardcontrol();
        if (isFirstSelect)
        {
            circleUIAnimControllers[index].IsSelected = true;
            circleUIPointerController.SectionNumber = index;
        }


    }

    void selectaSection()
    {
        for (int i = 0; i < totalSection; i++)
        {
            if (i != index && circleUIAnimControllers[i].IsSelected == true)
            {
                circleUIAnimControllers[index].IsSelected = false;
                index = i;
            }
        }
    }

    void keyboardcontrol()
    {
        ScrollWheelGauge += Input.GetAxis("Mouse ScrollWheel");

        if (ScrollWheelGauge != 0)
        {
            isFirstSelect = true;
        }

        if (ScrollWheelGauge< -ScrollSpeed) // wheeldown means index++
        {
            circleUIAnimControllers[index++].IsSelected = false;
            
            ScrollWheelGauge = 0;

        }
        else if (ScrollWheelGauge > ScrollSpeed)
        {
            circleUIAnimControllers[index--].IsSelected = false;
            ScrollWheelGauge = 0;
        }

    }


}
