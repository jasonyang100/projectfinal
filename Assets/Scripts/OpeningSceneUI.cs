﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class OpeningSceneUI : MonoBehaviour
{
    private string[] context;
    private Text[] ContainTexts;
    private Text ContinueText;
    private string scene_A = "Scene_A";
    private bool CanGoNextScene = false;
    void Start()        
    {
        context = new string[8] 
        {
            "獵魔人洛傑在告示欄前尋找工作時，忽然有一名",
            "女子向他搭話，原來是洛傑的舊識女術士艾莉絲",
            "女術士尋求洛傑的協助，希望他能從一個廢棄的",
            "古代法師實驗室中尋找法力符石。洛傑想起了過",
            "去欠女術士的一些人情，便決定提供協助。女術",
            "士告訴洛傑實驗室的詳細地點後，便給了他一些",
            "法力塵，並告訴他找到裡面的一座法力井並將塵",
            "投入井中，如此便能協助他尋找法力符石。"
        };
        var textFields = GetComponentsInChildren<Text>();
        ContainTexts = new Text[8] 
        { 
            textFields[0], 
            textFields[1], 
            textFields[2], 
            textFields[3], 
            textFields[4],
            textFields[5],
            textFields[6],
            textFields[7]
        };
        ContinueText = textFields[8];
        StartCoroutine(FadeInText());
    }
    private void Update()
    {
        if (!CanGoNextScene) return;
        if (Input.GetKeyDown(KeyCode.C))
        {
            SSceneManager.s_instance.FadeToScene(scene_A);
            CanGoNextScene = false;
        }
    }
    private IEnumerator FadeInText()
    {
        yield return new WaitForSeconds(1.5f);

        int l = context.Length;

        for (int i = 0; i < l; ++i)
        {
            ContainTexts[i].text = context[i];
            ContainTexts[i].GetComponent<Animation>().Play();
            yield return new WaitForSeconds(1f);
        }
        yield return new WaitForSeconds(0.5f);
        CanGoNextScene = true;
        ContinueText.enabled = true;
    }
    //private IEnumerator TypeText()
    //{
    //    yield return new WaitForSeconds(1f);

    //    int l = context.Length;
    //    for (int i = 0; i < l; ++i)
    //    {
    //        foreach (char letter in context[i].ToCharArray())
    //        {
    //            ContainText.text += letter;
    //            yield return new WaitForSeconds(0.05f);
    //        }

    //        ContainText.text += "\n";

    //        yield return new WaitForSeconds(0.5f);
    //    }
    //    isTextEnd = true;
    //    ContinueText.enabled = true;
    //}
}
