﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShieldContact : MonoBehaviour
{
    [SerializeField] MeshRenderer m_meshRenderer;
    float Clock = 2f;

    [SerializeField] bool isActive=true;
    [SerializeField] float Radius = 0.5f;
    [SerializeField] Vector3 contact;

    private Collider col;




    // Start is called before the first frame update
    void Start()
    {
        m_meshRenderer = GetComponent<MeshRenderer>();

        col = GetComponent<Collider>();
    }

    // Update is called once per frame
    void Update()
    {
       
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Weapon" && other.transform.root.tag == "Mob")//other.gameObject.tag == "Weapon" &&
        {
            Debug.Log(other.transform.position);
            StartCoroutine(MovingMaskSphere(other.transform.position));
        }
    }

    //void deploy(bool _isactive)
    //{
    //    if (isActive)
    //    {
            
    //        if (Clock <= 0)
    //        {
    //            m_meshRenderer.material.SetVector("_SphereCenter", new Vector3(0f, 0f, 0f));
    //            Clock = 2f;
    //            isActive = false;
    //        }
    //        else
    //        {
    //            Clock -= Time.deltaTime;
    //            contact=Vector3.Lerp(contact, new Vector3(0f, 0f, 0f), 0.1f);
    //            m_meshRenderer.material.SetVector("_SphereCenter", contact);
    //        }
            
    //    }
        IEnumerator EffectFadeOut()
        {
        float _timer = 1f;
        while (_timer > 0f)
        {
            _timer -= Time.deltaTime;
            contact = Vector3.Lerp(contact, new Vector3(0f, 0f, 0f), 0.1f);
            m_meshRenderer.material.SetVector("_SphereCenter", contact);
            yield return null;

        }
        m_meshRenderer.material.SetVector("_SphereCenter", new Vector3(0f, 0f, 0f));
        contact = new Vector3(0f, 0f, 0f);

    }
        IEnumerator MovingMaskSphere(Vector3 _OtherPositon)
        {
            Vector3 Dir = _OtherPositon-this.transform.position;
        Dir = Dir.normalized;
            contact = Dir * Radius;
        StartCoroutine(EffectFadeOut());

            yield return null;
        }

    private void OnDisable()
    {
        m_meshRenderer.material.SetVector("_SphereCenter", new Vector3(0f, 0f, 0f));
    }

    private void OnEnable()
    {
        contact = new Vector3(0f, 0f, 0f);
    } 
}








