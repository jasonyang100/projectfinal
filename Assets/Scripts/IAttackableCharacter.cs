﻿public interface IAttackableCharacter
{
    string UnderAttackSoundEffect();
}