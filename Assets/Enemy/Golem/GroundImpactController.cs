﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GroundImpactController : MonoBehaviour
{
    private GameObject Golem;
    private Collider HitSphere;
    private void Awake()
    {
        Golem = GameObject.Find("Golem");
        HitSphere = GetComponent<Collider>();
    }
    private void OnEnable()
    {
        StartCoroutine(AuxiliaryTool.Instance.CoroutineWaiter(0.7f, () => HitSphere.enabled = false));
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag != "Player") return;
        else
        {
            Golem.SendMessage("AttackOnHit", new object[2] { other, gameObject.tag });
            HitSphere.enabled = false;
        }
    }
}
