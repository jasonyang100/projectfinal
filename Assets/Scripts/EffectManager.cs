﻿using System.Collections;
using System.Collections.Generic;
using System.Security.Permissions;
using UnityEngine;


[DisallowMultipleComponent]
public class EffectManager : MonoBehaviour
{
    public static EffectManager Instance { private set; get; }

    public Dictionary<string, GameObject> HitEffectdict = new Dictionary<string, GameObject>();

    public Dictionary<string, GameObject> WeaponEffectdict = new Dictionary<string, GameObject>();

    public Dictionary<string,GameObject> WildHuntEffectdict = new Dictionary<string, GameObject>();

    public GameObject Debris;

    public GameObject GetBurning;

    public GameObject GroundImpact;

    public GameObject TitanRageEffect;

    private void Awake()
    {
        Instance = this;
        LoadHitEffectGo("BloodEffect", "SlashHit_VFX2");
        LoadWeaaponEffect("SwordFlashEffect");
        LoadWildHuntEffect("SwordShock");
        Debris = Resources.Load<GameObject>("Debris");
        GetBurning = Resources.Load<GameObject>("Getburning_Go");
        GroundImpact = Resources.Load<GameObject>("GroundImpact_VFX");
        TitanRageEffect = Resources.Load<GameObject>("RageAura_VFX");
    }
    private void LoadHitEffectGo(params string[] names)
    {
        foreach(string name in names)
        {
            object go = Resources.Load($"HitEffect/{name}");

            HitEffectdict.Add(name, go as GameObject);
        }
    }

    private void LoadWeaaponEffect(params string[] names)
    {
        foreach (string name in names)
        {
            object go = Resources.Load($"WeaponEffect/{name}");

            WeaponEffectdict.Add(name, go as GameObject);
        }
    }
    private void LoadWildHuntEffect(params string[] names)
    {
        foreach (string name in names)
        {
            object go = Resources.Load($"WildHuntEffect/{name}");

            WildHuntEffectdict.Add(name, go as GameObject);
        }
    }
}
