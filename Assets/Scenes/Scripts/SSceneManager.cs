﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SSceneManager : MonoBehaviour
{
    public string levelToLoad;
    public static SSceneManager s_instance;
    public Animator animator;
    public static SSceneManager Instance() { return s_instance; }

    private void Awake()
    {
        if (s_instance == null)
        {
            s_instance = this;
            DontDestroyOnLoad(this);
            name = "SceneManager";
        }
        else
            Destroy(gameObject);
    }
    private void Start()
    {
        animator = GetComponent<Animator>();
    }

    public void FadeToScene(string scene)
    {
        levelToLoad = scene;
        animator.SetTrigger("FadeOut");
    }
    public IEnumerator OnFadeComplete()
    {
        AsyncOperation ao = SceneManager.LoadSceneAsync(levelToLoad);
        yield return ao;
        animator.SetTrigger("FadeIn");
    }
}
