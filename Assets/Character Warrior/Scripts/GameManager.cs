﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public static GameManager g_instance;
    public GameObject player;
    public string currentScene;
    public float pX;
    public float pY;
    public float pZ;
    public bool isGameStart = true;
    public float PlayerMaxHp; //轉場儲存玩家MaxHP
    public float PlayerCurrentHp; //轉場儲存玩家CurrentHP

    private void Awake()
    {
        if (g_instance == null)
        {
            g_instance = this;
            DontDestroyOnLoad(this);
            name = "GameManager";
        }
        else
            Destroy(gameObject);

    }
    private void Start()
    {
        if (GameObject.Find("Character Warrior"))
            player = GameObject.Find("Character Warrior");

        if (PlayerPrefs.GetInt("Saved") == 1 && PlayerPrefs.GetInt("TimeToLoad") == 1)
        {
            //pX = player.transform.position.x;
            //pY = player.transform.position.y;
            //pZ = player.transform.position.z;
            //pX = PlayerPrefs.GetFloat("p_x");
            //pY = PlayerPrefs.GetFloat("p_y");
            //pZ = PlayerPrefs.GetFloat("p_z");
            //player.transform.position = new Vector3(pX, pY, pZ);
            PlayerPrefs.SetInt("TimeToLoad", 0);
            PlayerPrefs.Save();
        }
    }
    public void SaveGame()
    {
        PlayerPrefs.SetFloat("p_x", player.transform.position.x);
        PlayerPrefs.SetFloat("p_y", player.transform.position.y);
        PlayerPrefs.SetFloat("p_z", player.transform.position.z);
        PlayerPrefs.SetInt("Saved", 1);
        PlayerPrefs.Save();
        Debug.Log(pX);
    }
    public void LoadGame()
    {
        PlayerPrefs.SetInt("TimeToLoad", 1);
        PlayerPrefs.Save();
    }
}
