﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MobUIManager : MonoBehaviour
{
    GameObject[] MobgameObjects;
    Camera camera;
    List<MobsStateForUI> MobsScreenPositionList;
    MobsStateForUI m_MobsStateForUI;
    Image TemplateHpBar;

    List<Image> HpBars;



    public struct MobsStateForUI
    {
        public GameObject go;
        public Vector3 Screenposition;
        public bool isactive;            
    }
    // Start is called before the first frame update
    void Start()
    {
        MobgameObjects = GameObject.FindGameObjectsWithTag("Mob");
        camera = Camera.main;
       

        

    }

    // Update is called once per frame
    void Update()
    {
        
    }


    void RecordMobstoScreenPosition()
    {
        foreach(GameObject go in MobgameObjects)
        {
            MobsStateForUI st ;
            st.go = go;
            st.isactive = go.activeSelf;
            st.Screenposition = camera.WorldToScreenPoint(go.transform.position);

            MobsScreenPositionList.Add(st);




        }
    }

    void UpdateMobstoScreenPosition()
    {
        for (int i = 0; i < MobsScreenPositionList.Count; i++)
        {
            if (MobsScreenPositionList[i].go.transform != null)// if gameobject is not destoyed
            {
                MobsStateForUI msf;
                msf.go = MobsScreenPositionList[i].go;
                msf.Screenposition = camera.WorldToScreenPoint(MobsScreenPositionList[i].go.transform.position);
                msf.isactive = MobsScreenPositionList[i].go.activeSelf;
                MobsScreenPositionList[i] = msf;
            }
        }
    }

    void DistributeHpBar()
    {
        Image i = Instantiate(TemplateHpBar, this.transform);
        HpBars.Add(i);
    }

    void UpdateHpbarsInfo()
    {
        foreach(var v in MobsScreenPositionList)
        {

        }
    }
}
