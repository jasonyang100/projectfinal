﻿using EnumsManager;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class TargetingSystem : MonoBehaviour
{
    private Dictionary<NPCType, TargetingSystemNPC> dict_NPCMap;
    TargetingSystemNPC Mobs;
    TargetingSystemNPC NonMobNPCs;
    AudioSource audioSource;
    PlayerMove playerMove;
    bool isDeadFadeOut;
    private string scene_A = "Scene_A";
    private string scene;

    [SerializeField] public bool isInteractingWithNPC = false;
    internal class TargetingSystemNPC
    {
        public GameCharacter selectedTarget 
        {
            get
            {
                if (CharactersInSphere.Count > 0) return CharactersInSphere[idx]; 
                else return null;
            }
        }
        public List<GameCharacter> CharactersInSphere = new List<GameCharacter>();
        public int _idx = -1;
        public int idx
        {
            set => _idx = value;
            get
            {
                if (CharactersInSphere.Count == 0) _idx = -1;
                else _idx = Mathf.Clamp(_idx, 0, CharactersInSphere.Count - 1);
                return _idx;
            }
        }
    }

    void Awake()
    {
        scene = SceneManager.GetActiveScene().name;
        Mobs = new TargetingSystemNPC();
        NonMobNPCs = new TargetingSystemNPC();

        // FriendlyNPC & ITEM底層使用同一個容器, 底層可直接操作容器, 外部可由 Dictionary 呼叫
        dict_NPCMap = new Dictionary<NPCType, TargetingSystemNPC>
        {
            { NPCType.Mob, Mobs },
            { NPCType.FriendlyNPC, NonMobNPCs },
            { NPCType.Item, NonMobNPCs}
        };
    }
    void OnTriggerEnter(Collider other)
    {
        AddNPCinList(other.tag, other);
    }
    void OnTriggerExit(Collider other)
    {
        DeleteNPCformList(other.tag, other);
    }
    public TargetingMode GetNowMode()
    {
        if (Mobs.CharactersInSphere.Count > 0) return TargetingMode.Fight;
        else if(NonMobNPCs.CharactersInSphere.Count > 0) return TargetingMode.NonMobNPCInteractive;
        else return TargetingMode.Normal;
    }
    public bool isNonMobNPCinRange()
    {
        if (NonMobNPCs.CharactersInSphere.Count == 0 ) return false;
        Vector3 pos = transform.root.position;
        pos.y = 0f;
        Vector3 tarpos = NonMobNPCs.selectedTarget.Ctransform.position;
        tarpos.y = 0f;
        if (Vector3.Distance(pos, tarpos) <= 6f) return true;
        else return false;
    }   

    private void AddNPCinList(string tag, Collider col)
    {
        if (Enum.TryParse(tag, true, out NPCType chartype))
        {
            GameCharacter character = col.GetComponent<IGameCharacterController>().ReturnGameCharacter();

            var charIntance = dict_NPCMap[chartype];

            if (charIntance.CharactersInSphere.Contains(character)) return;

            charIntance.CharactersInSphere.Add(character);
        }
    }
    private void DeleteNPCformList(string tag, Collider col)
    {
        if (Enum.TryParse(tag, true, out NPCType chartype))
        {
            GameCharacter character = col.GetComponent<IGameCharacterController>().ReturnGameCharacter();

            var charIntance = dict_NPCMap[chartype];
            
            if (charIntance.CharactersInSphere.Contains(character)) charIntance.CharactersInSphere.Remove(character);
        }
    }
    public void TargetEnemy()
    {
        if (GetNowMode() != TargetingMode.Fight) return;
        if (Input.GetKeyDown(KeyCode.Tab))
        {
            if (Mobs.CharactersInSphere.Count == 0) return;
            else if (Mobs.idx == Mobs.CharactersInSphere.Count - 1) Mobs.idx = 0;
            else Mobs.idx++;
        }
    }

    public void TargetNonMobNPC()
    {
        if (GetNowMode() != TargetingMode.NonMobNPCInteractive) return;
        if (isInteractingWithNPC) return;
        if (Input.GetKeyDown(KeyCode.Tab))
        {            
            if (NonMobNPCs.CharactersInSphere.Count == 0) return;
            else if (NonMobNPCs.idx == NonMobNPCs.CharactersInSphere.Count - 1) NonMobNPCs.idx = 0;
            else NonMobNPCs.idx++;
        }
    }

    private void RemoveMob(GameCharacter character)
    {
        if (Mobs.CharactersInSphere.Contains(character)) Mobs.CharactersInSphere.Remove(character);
    }
    public (float, float) SetMobHpToFlotingBar()
    {
        return (Mobs.selectedTarget.MaxHP, Mobs.selectedTarget.currentHP);
    }
    public GameCharacter selectedMobTargetCharacter()
    {
        if (Mobs.selectedTarget == null) return null;
        else return Mobs.selectedTarget;
    }
    public GameObject selectedMobTarget()
    {
        if (Mobs.selectedTarget == null) return null;
        else return Mobs.selectedTarget.CgameObject;
    }
    public GameObject selectedNonMobNPCTarget()
    {
        if (NonMobNPCs.selectedTarget == null) return null;
        else return NonMobNPCs.selectedTarget.CgameObject;
    }
    public void isMobAround()
    {
        if (scene == scene_A)
        {
            if (Mobs.CharactersInSphere.Count == 0 || playerMove.isDeadFadeOut)
            {
                audioSource.volume -= 0.005f;
                if (audioSource.volume <= 0)
                {
                    audioSource.volume = 0;
                    audioSource.enabled = false;
                }
            }
            else
            {
                audioSource.enabled = true;
                audioSource.volume += 0.015f;
                if (audioSource.volume >= 1)
                    audioSource.volume = 1;
            }
        }
    }
    public bool isselectedNonMobInteractable()
    {
        if (NonMobNPCs.selectedTarget == null) return false;

        NonPlayer nonPlayer = NonMobNPCs.selectedTarget as NonPlayer;

        return nonPlayer.CanInteraction;
    }
    private void Start()
    {
        playerMove = FindObjectOfType<PlayerMove>();
        audioSource = GetComponent<AudioSource>();
        audioSource.volume = 0;
        audioSource.enabled = false;

    }
    private void Update()
    {
        isMobAround();
    }
}