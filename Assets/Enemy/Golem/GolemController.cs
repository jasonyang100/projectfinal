﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Reflection;
using System.Linq;
using UnityEngine;
using EnumsManager;

public class GolemController : MonoBehaviour, IGameCharacterController, IAttackableCharacter
{
    private Type controller;
    private Mob golem;
    private MobAI golemai;
    private List<Collider> weaponcollider = new List<Collider>(); //左手&右手
    private GameObject WeaponEffect;

    private GolemAttackForm attackForm = GolemAttackForm.Smash;
    private Dictionary<GolemAttackForm, float> dict_attackRadius;
    private Dictionary<GolemAttackForm, Func<Action<float>, float, IEnumerator>> dict_attackDamage;

    private delegate MobState stateAction();

    private Dictionary<MobState, stateAction> stateActionDict;

    private Dictionary<MobState, Action> AnimActionDict;

    private MobAlertBehaviour alertbehaviour = MobAlertBehaviour.STAND;
    private Dictionary<MobAlertBehaviour, Action> dict_alertbehaviour;
    private Vector3 AlertmovePos;

    private Animator animator;
    private AnimatorStateInfo currentAnimState;

    private GameObject TargetSystemGO;
    private GameObject triggerGo;
    private GetDieing GetDieingComponent;
    private List<GameObject> BurnigGos = new List<GameObject>();

    private int _underattackcounter = 0;
    private int underAttackCounter
    {
        get => _underattackcounter;
        set
        {
            if (value > 6) value = 6;
            if (value < 0) value = 0;
            _underattackcounter = value;
        }
    }
    private float _animSpeedParam = 0f;
    public float animSpeedParam
    {
        get
        {
            switch (golemai.currentState)
            {
                case MobState.IDLE:
                case MobState.DEFENSE:
                    _animSpeedParam = 0f;
                    break;
                case MobState.ALERT:
                    _animSpeedParam = 0.1f;
                    break;
            }
            return _animSpeedParam;
        }
        set
        {
            switch (golemai.currentState)
            {
                case MobState.CHASE:
                case MobState.RETURN:
                    value = Mathf.Clamp(value, 0.2f, 2f);
                    break;
            }
            _animSpeedParam = value;
        }
    }

    private float IdleTimer = 0f;
    private float AlertTimer = 0f;
    private float DefenseTimer = 0f;

    private void Awake()
    {
        controller = this.GetType();
        dict_attackRadius = new Dictionary<GolemAttackForm, float>
        {
            { GolemAttackForm.RocketFist, 6f},
            { GolemAttackForm.Smash, 4f},
            { GolemAttackForm.DoubleStrike, 4f},
            { GolemAttackForm.GroundImpact, 50f }
        };

        dict_attackDamage = new Dictionary<GolemAttackForm, Func<Action<float>, float, IEnumerator>>
        {
            { GolemAttackForm.RocketFist, RocketFist },
            { GolemAttackForm.Smash, Smash },
            { GolemAttackForm.DoubleStrike, DoubleStrike },
            { GolemAttackForm.GroundImpact, GroundImpact }
        };

        dict_alertbehaviour = new Dictionary<MobAlertBehaviour, Action>
        {
            { MobAlertBehaviour.STAND, AlertStand },
            { MobAlertBehaviour.MOVEBACK, AlertMoveback },
            { MobAlertBehaviour.THREATEN, AlertThreaten },
        };

        golem = new Mob(gameObject);
        golem.InitialHpMana(23f, 0f);
        golem.characterName = CharacterName.Golem;
        golem.gravity = 5f;
        golem.moveSpeedDict = new Dictionary<MobState, float>()
        {
            { MobState.CHASE, 6f },
            { MobState.RETURN, 6f },
        };
        golem.m_strDefenseSoundEffect = "20_Player_SwordHitRock1";
        golem.m_strUnderAttackSoundEffect = "20_Player_SwordHitRock1";

        golemai = new MobAI(golem);
        golemai.opposingGoTag = new HashSet<string> { "Player" };
        golemai.mobLocomotionType = MobNormalStateType.IdleOnly;
        golemai.initialPosition = golem.CgameObject.transform.position;
        golemai.idleRestTime = 9f;
        golemai.wanderRadius = 0f;
        golemai.SightRangeAngle = 110f;
        golemai.attackRadius = dict_attackRadius[attackForm];
        golemai.returnRadius = 42f;
        golemai.alertRadius = 24f;

        golemai.maxAttackTimeSpan = 1.2f;
        golemai.minAttackTimeSpan = 0.2f;
        golemai.attackTimeSpan = 0.5f;

        golemai.rotateAbility = 0.02f;

        stateActionDict = new Dictionary<MobState, stateAction>()
        {
            { MobState.IDLE, golemai.Idle },
            { MobState.ALERT, golemai.Alert },
            { MobState.CHASE, golemai.Chase },
            { MobState.FIGHT, golemai.Fight },
            { MobState.DEFENSE, golemai.Defense },
            { MobState.DAMAGED, golemai.Damaged },
            { MobState.RETURN, golemai.Return },
            { MobState.DIE, golemai.Die }
        };
        AnimActionDict = InitializeAnimActionDict();

        //WeaponEffect = GameObject.Find("WeaponEffect");
        TargetSystemGO = GameObject.Find("EnemySearchSphere");
        GetDieingComponent = transform.GetComponent<GetDieing>();
        animator = GetComponent<Animator>();
        golem.Ccollider = GetComponent<Collider>();
        golem.Ccontroller = GetComponent<CharacterController>();
    }
    // Start is called before the first frame update
    void Start()
    {
    }
    // Update is called once per frame
    void Update()
    {
        if (!golem.Ccontroller.isGrounded) golem.Ccontroller.Move(new Vector3(0f, -golem.gravity * 2f, 0f) * Time.deltaTime);
        DoStateMachine();
    }
    private void DoStateMachine()
    {
        currentAnimState = animator.GetCurrentAnimatorStateInfo(0);
        AnimActionDict[golemai.currentState]();
        MobState nextState = stateActionDict[golemai.currentState]();
        golemai.lastState = golemai.currentState;
        if (golemai.cutinState == MobState.NOTSTATE) golemai.currentState = nextState;
        else
        {
            golemai.currentState = golemai.cutinState;
            golemai.cutinState = MobState.NOTSTATE;
        }
        //Debug.Log(golemai.currentState);
    }
    private IEnumerator RocketFist(Action<float> attact, float reduction)
    {
        float originaldamage = 2f;
        float damage = Mathf.Clamp(0f, originaldamage * (1 - reduction), originaldamage);
        attact(damage);
        yield break;
    }
    private IEnumerator Smash(Action<float> attact, float reduction)
    {
        float originaldamage = 1f;
        float damage = Mathf.Clamp(0f, originaldamage * (1 - reduction), originaldamage);
        attact(damage);
        yield break;
    }
    private IEnumerator DoubleStrike(Action<float> attact, float reduction)
    {
        float originaldamage = 1f;
        float damage = Mathf.Clamp(0f, originaldamage * (1 - reduction), originaldamage);
        attact(damage);
        yield break;
    }
    private IEnumerator GroundImpact(Action<float> attact, float reduction)
    {
        float originaldamage = 3f;
        float damage = Mathf.Clamp(0f, originaldamage * (1 - reduction), originaldamage);
        attact(damage);
        yield break;
    }

    private void AlertStand()
    {
        if (golemai.currentState != MobState.ALERT) return;
        if (AlertTimer >= golemai.attackTimeSpan) golemai.isActionDone = true;
    }
    private void AlertMoveback()
    {
        if (golemai.currentState != MobState.ALERT) return;

        if (AlertTimer == 0)
        {
            animator.SetBool("Moveback", true);
            AlertTimer += 0.001f; //加一個微小時間避免animator偵測狀態卡住
            AlertmovePos = transform.position - transform.forward * 4f;
        }

        if (currentAnimState.IsName("Moveback") && !animator.IsInTransition(0))
        {
            if (Mathf.Sign(Vector3.Dot(transform.forward, AlertmovePos - transform.position)) >= 0)
            {
                animator.SetBool("Moveback", false);
            }
            else golem.Ccontroller.Move(-transform.forward * golem.moveSpeedDict[MobState.CHASE] * Time.deltaTime);
        }

        if (AlertTimer >= golemai.attackTimeSpan) golemai.isActionDone = true;
    }
    //private void AlertMoveto()
    //{
    //    if (golemai.currentState != MobState.ALERT) return;

    //    if (AlertTimer == 0)
    //    {
    //        if(Mathf.RoundToInt(Time.deltaTime * 1000) % 2 == 0)
    //            AlertmovePos = transform.position - transform.right * 5f;
    //        else
    //            AlertmovePos = transform.position + transform.right * 5f;
    //    }
    //    Vector3 moveVector =  SteeringBehavior.Seek(transform, AlertmovePos, transform.forward * golem.moveSpeedDict[MobState.CHASE]);
    //    moveVector = SteeringBehavior.Arrival(transform, AlertmovePos, moveVector);
    //    transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(moveVector.normalized), 0.2f);
    //    golem.Ccontroller.Move(moveVector * Time.deltaTime);

    //    if (AlertTimer >= (golemai.attackTimeSpan + 1.5f)) golemai.isActionDone = true;
    //}
    private void AlertThreaten()
    {
        if (golemai.currentState != MobState.ALERT) return;

        if (AlertTimer == 0)
        {
            animator.SetTrigger("Threaten");
            AlertTimer += 0.001f; //加一個微小時間避免animator偵測狀態卡住
        }
        if (AlertTimer >= golemai.attackTimeSpan) golemai.isActionDone = true;
    }
    private Dictionary<MobState, Action> InitializeAnimActionDict()
    {
        return new Dictionary<MobState, Action>()
        {
            { MobState.IDLE, () =>
                {
                    if(golemai.lastState != golemai.currentState)
                    {
                        animator.SetBool("Defense", false);
                        animator.SetFloat("Speed", animSpeedParam);
                        IdleTimer = 0f;
                    }
                    if (currentAnimState.IsTag("Idle") && !animator.IsInTransition(0))
                    {
                        IdleTimer += Time.deltaTime;
                        if(IdleTimer > 2f && IdleTimer < 4f) animator.SetBool("Lookaround", true);
                        if(IdleTimer > 4.5f && IdleTimer < 6f) animator.SetBool("Lookaround", false);
                        if(IdleTimer >= golemai.idleRestTime) Dispersion();
                    }
                }
            },
            { MobState.ALERT, () =>
                {
                    if(golemai.lastState != golemai.currentState)
                    {
                        AlertTimer = 0f;
                        animator.SetFloat("Speed", animSpeedParam);
                        animator.SetBool("isDispersion", false);
                        animator.SetBool("Lookaround", false);
                        animator.SetBool("Moveback", false);
                        animator.ResetTrigger("getHit");
                        if(golemai.aimingGo != null)
                        {
                            SelectAlertForms();
                            SelectAttackMoves();
                        }
                        else alertbehaviour = MobAlertBehaviour.STAND;
                    }
                    dict_alertbehaviour[alertbehaviour]();

                    if (currentAnimState.IsName("Locomotion") && !animator.IsInTransition(0))
                        AlertTimer += Time.deltaTime;
                }
            },
            { MobState.CHASE, () =>
                {
                    animator.SetBool("Defense", false);
                    AuxiliaryTool.Instance.ResetAnimatorTriggers(animator,"Threaten");
                    animSpeedParam += 0.2f;
                    animator.SetFloat("Speed", animSpeedParam);
                }
            },
            { MobState.FIGHT, () => //Fight
                {
                    if(golemai.lastState != golemai.currentState)
                    {
                         golemai.attackTimeSpan = golemai.maxAttackTimeSpan;
                         AuxiliaryTool.Instance.ResetAnimatorTriggers(animator,"Threaten","getHit");
                    }
                    if(!currentAnimState.IsTag("Attack") && !animator.IsInTransition(0))
                        animator.SetTrigger(attackForm.ToString());
                }
            },
            { MobState.DEFENSE, () => //Defense
                {
                    animator.SetFloat("Speed", animSpeedParam);
                    if(golemai.lastState != golemai.currentState)
                    {
                        DefenseTimer = 0f;
                        golemai.attackTimeSpan -= 0.8f;
                        EnableWeapon(false);
                        animator.SetBool("Defense", true);
                    }
                    if (currentAnimState.IsName("Defense") && !animator.IsInTransition(0))
                    {
                        DefenseTimer += Time.deltaTime;
                        if(DefenseTimer >= 3f)
                        {
                            golemai.isActionDone = true;
                            animator.SetBool("Defense", false);
                        }
                    }
                }
            },
            { MobState.DAMAGED,  () => //Damaged
                {
                    if(golemai.lastState != golemai.currentState)
                    {
                        golemai.attackTimeSpan -= 0.8f;
                        AuxiliaryTool.Instance.ResetAnimatorTriggers(animator,"Threaten");
                        animator.SetBool("Defense",false);
                        animator.SetTrigger("getHit");
                        EnableWeapon(false);
                    }
                }
            },
            { MobState.RETURN, () => //Return
                {
                    animator.SetBool("Defense",false);
                    AuxiliaryTool.Instance.ResetAnimatorTriggers(animator,"Threaten","getHit");
                    animSpeedParam += 0.1f;
                    animator.SetFloat("Speed", animSpeedParam);
                }
            },
            { MobState.DIE, () => //Die
                {
                    EnableWeapon(false);
                    AuxiliaryTool.Instance.ResetAnimatorTriggers(animator,"Threaten","getHit");
                    AudioManager.instance.Play("40_Golem_Dead");
                    animator.SetBool("Defense",false);
                    animator.SetBool("isDead",true);
                    TargetSystemGO.SendMessage("RemoveMob", golem);
                    Destroy(triggerGo);
                    removeBurning(1);
                    StopAllCoroutines();
                    StartCoroutine( AuxiliaryTool.Instance.CoroutineWaiter(1f,() => GetDieingComponent.enabled = true));
                    golem.Ccollider.enabled = false;
                    golem.Ccontroller.enabled = false;
                    this.enabled = false;
                }
            },
        };
    }
    //private void OnControllerColliderHit(ControllerColliderHit hit)
    //{
    //    if (hit.gameObject.layer == 8)
    //    {
    //        float offsety = hit.point.y - transform.position.y;

    //        float dy = Mathf.Abs(golemcollider.bounds.center.y - offsety);
    //        if (dy < (golemcollider.bounds.center.y / 4)) golemai.isHitWall = true;
    //    }
    //}
    private void OnTriggerEnter(Collider other)
    {
    }
    private void UnderAttack(object[] obj)
    {
        Vector3 HitDir = (obj[0] as Transform).position;
        Func<Action<float>, float, IEnumerator> HitDel = obj[1] as Func<Action<float>, float, IEnumerator>;
        float reduction = 0f;
        if (golemai.currentState != MobState.DEFENSE)
        {
            golemai.currentState = MobState.DAMAGED;

            if (underAttackCounter == 0)
            {
                golemai.currentState = MobState.DAMAGED;
                underAttackCounter++;
            }
            else
            {
                int diceNo = UnityEngine.Random.Range(underAttackCounter, 7);

                if (diceNo == underAttackCounter)
                {
                    golemai.currentState = MobState.DEFENSE;
                    underAttackCounter = 0;
                }
                else underAttackCounter += 2;
            }
        }
        else reduction = 0.8f;

        StartCoroutine(HitDel(damage => golem.GetHit(damage), reduction));
        golemai.GetHitDir = HitDir - gameObject.transform.position;
        //Debug.Log(troll.baseHP);
    }
    void OnAnimationAttackStart()
    {
        EnableWeapon(true);
    }
    void OnAnimationAttackEnd()
    {
        EnableWeapon(false);
    }
    private void OnAnimationDone(string state)
    {
        if (state != golemai.currentState.ToString()) return;
        golemai.isActionDone = true;
    }
    private void OnHitEffect(Collider col)
    {
        string HitSoundEffect = col.GetComponent<IAttackableCharacter>().UnderAttackSoundEffect();
        AudioManager.instance.Play(HitSoundEffect);
    }
    private void AttackOnHit(object[] obj)
    {
        Collider col = obj[0] as Collider;
        string Hittag = obj[1] as string;

        if (!golemai.opposingGoTag.Contains(col.tag)) return;
        if (Hittag == "Weapon")
        {
            col.SendMessage("UnderAttack", new object[2] { transform, dict_attackDamage[attackForm] });
            OnHitEffect(col);
            EnableWeapon(false);
        }
    }
    private void SelectAlertForms()
    {
        if (golemai.aimingGo == null) return;

        if (golemai.lastState == MobState.IDLE)
        {
            alertbehaviour = MobAlertBehaviour.THREATEN;
            return;
        }

        int rnd = UnityEngine.Random.Range(0, 6);

        switch (rnd)
        {
            case 0:
                alertbehaviour = MobAlertBehaviour.THREATEN;
                break;
            case 1:
            case 2:
            case 3:
                alertbehaviour = MobAlertBehaviour.STAND;
                break;
            default:
                alertbehaviour = MobAlertBehaviour.MOVEBACK;
                break;
        }
    }

    private void SelectAttackMoves()
    {
        if (golemai.aimingGo == null) return;

        float distance = Vector3.Distance(transform.position, golemai.aimingGo.transform.position);

        if (distance <= 7.5f)
        {
            int rnd = UnityEngine.Random.Range(0, 6);
            switch (rnd)
            {
                case 0:
                    attackForm = GolemAttackForm.Smash;
                    break;
                case 1:
                    attackForm = GolemAttackForm.DoubleStrike;
                    break;
                default:
                    attackForm = GolemAttackForm.GroundImpact;
                    break;
            }
        }
        else
        {
            int rnd = UnityEngine.Random.Range(0, 5);
            switch (rnd)
            {
                case 0:
                    attackForm = GolemAttackForm.Smash;
                    break;
                case 1:
                    attackForm = GolemAttackForm.DoubleStrike;
                    break;
                default:
                    attackForm = GolemAttackForm.RocketFist;
                    break;
            }
        }
        golemai.attackRadius = dict_attackRadius[attackForm];
    }
    private void OnAnimationGroundImpact()
    {
        GameObject GroundImpactGO = Instantiate(EffectManager.Instance.GroundImpact);
        GroundImpactGO.transform.position = transform.position;
        GroundImpactGO.transform.rotation = Quaternion.identity;
        AudioManager.instance.Play("40_Golem_Dead");
        Destroy(GroundImpactGO, 1.8f);
        Camera.main.SendMessage("StartShake", new object[2] { 0.5f, 0.3f });
    }
    private void OnAnimationThreanShock()
    {
        Camera.main.SendMessage("StartShake", new object[2] { 0.15f, 0.2f });
        AudioManager.instance.Play("43_Golem_Punch");
    }
    public void CallCharacterRegistrar(object[] obj)
    {
        GameCharacter gc = obj[0] as GameCharacter;
        bool bo = (bool)obj[1];
        if (bo) golem.Register(gc);
        else golem.Unregister(gc);
    }
    private void AddWeaponGameObject(Collider col) => weaponcollider.Add(col);

    private void EnableWeapon(bool bo)
    {
        foreach (Collider col in weaponcollider)
        {
            col.enabled = bo;
        }
    }
    private void OnOpposingGoDie(GameObject go)
    {
        StartCoroutine(AuxiliaryTool.Instance.CoroutineWaiter(1f,
            () =>
            {
                if (golemai.aimingGo == go) golemai.aimingGo = null;
                golemai.currentState = MobState.RETURN;
                animator.SetBool("Defense", false);
                AuxiliaryTool.Instance.ResetAnimatorTriggers(animator, "Threaten", "Lookaround", "getHit");
            }));
    }
    private void Assembly(GameObject triggerGo)
    {
        this.triggerGo = triggerGo;
        animator.SetBool("isDispersion", false);
        AudioManager.instance.Play("40_Golem_Dead");
    }
    private void Dispersion()
    {
        if (golemai.aimingGo != null) return;
        animator.SetBool("Lookaround", false);
        animator.SetBool("isDispersion", true);
        //this.enabled = false;
    }
    private void GetBurning()
    {
        GameObject go = Instantiate(EffectManager.Instance.GetBurning);
        BurnigGos.Add(go);
        go.transform.parent = transform;
        go.transform.position = golem.Ccollider.bounds.center;

        StartCoroutine(AuxiliaryTool.Instance.CoroutineWaiter(5f, () =>
        {
            if (BurnigGos.Contains(go))
            {
                BurnigGos.Remove(go);
                Destroy(go);
            }
        }));
    }
    private void removeBurning(float destorytime = 0f)
    {
        if (BurnigGos.Count > 0)
        {
            foreach (var go in BurnigGos)
            {
                if (go != null) Destroy(go);
            }
            BurnigGos.Clear();
        }
    }
    public GameCharacter ReturnGameCharacter()
    {
        return golem;
    }
    public string UnderAttackSoundEffect()
    {
        if (golemai.currentState == MobState.DEFENSE) return golem.m_strDefenseSoundEffect;
        else return golem.m_strUnderAttackSoundEffect;
    }
}