﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WildHuntTrigger : MonoBehaviour
{
    private GameObject WildHunt;
    private SphereCollider sphereCollider;
    [SerializeField]
    private Renderer rend;
    private Vector2 offsetv2 = new Vector2(1, 1);
    private float timer = 0;

    private void Awake()
    {
        WildHunt = GameObject.Find("WildHunt");
        sphereCollider = transform.GetComponent<SphereCollider>();
        rend = GetComponent<Renderer>();
    }
    private void Update()
    {
        timer += Time.deltaTime*0.01f;
        rend.material.mainTextureOffset = offsetv2 * timer;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (!other.CompareTag("Player")) 
        {
            return; 
        }
        WildHunt.SendMessage("Engage", other);
    }
}