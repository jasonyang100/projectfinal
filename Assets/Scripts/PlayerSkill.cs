﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using UnityEngine;

public class PlayerSkill
{
    private float _coolingtime;
    private float coolingTime
    {
        set => _coolingtime = Mathf.Clamp(value, 0f, _cooldownTime);
        get => _coolingtime;
    } // 技能冷卻中的計時間
    private float _cooldownTime;
    public float cooldownTime
    {
        set => _cooldownTime = Mathf.Max(0f, value);
        private get => _cooldownTime;
    } // 技能冷卻時間

    private float _castingTime;
    public float castingTime
    {
        set => _castingTime = Mathf.Max(0f, value);
        private get => _castingTime;
    }  // 技能持續時間

    private float _deployTime = 0.02f;
    public float deployTime
    {
        set => _deployTime = Mathf.Max(0f, value);
        private get => _deployTime;
    } // 施放技能延遲時間

    public GameObject skillInstance;

    public bool isTrunOnOfforInstantiate = true; // true 開關類型 , false , 生成類型
    public bool isCasting { private set; get; } = false;

    public Action StartCastAct;

    public Action<bool> CastingAct;

    public Action CoolDownAct;


    // for Flame data 

    public PlayerSkill(GameObject go)
    {
        skillInstance = go;
    }
    public void SetSkillData( float castingTime, float cooldownTime, float depolyTime)
    {
        (this.castingTime, this.cooldownTime, this.deployTime) = (castingTime, cooldownTime, depolyTime);
    }
    public IEnumerator Cast(bool iscooling)
    {
        if (iscooling || isCasting) yield break;

        isCasting = true;

        StartCastAct?.Invoke(); //動畫開始施放函式

        yield return new WaitForSeconds(deployTime); // 施放延遲

        //skillInstance.SetActive(true); //施放
        StartEffect(true);

        yield return new WaitForSeconds(0.001f); // 特效播完先delay一小段時間

        CastingAct?.Invoke(true); //施放中函式

        CoolDownAct();

        //_ = SkillCoolDown();

        yield return new WaitForSeconds(castingTime); // 技能持續時間

        CastingAct?.Invoke(false); //技能結束

        //skillInstance.SetActive(false);
        StartEffect(false);

        isCasting = false;

        yield break;
    }

    public void StartEffect(bool bo)
    {
        if (bo)
        {
            if (isTrunOnOfforInstantiate) skillInstance.SetActive(true);
            else
            {
                GameObject Token = GameObject.Instantiate(skillInstance);
                Token.transform.position = skillInstance.transform.position;
                Token.transform.rotation = skillInstance.transform.rotation;
                Token.SetActive(true);
                _ = AuxiliaryTool.Instance.AsyncWaiter(1f, () => Token.GetComponentInChildren<Collider>().enabled = false);
                GameObject.Destroy(Token, 2.2f);
            }
        }
        else
        {
            if (isTrunOnOfforInstantiate) skillInstance.SetActive(false);
            else return;
        }
    }

    //public async Task SkillCoolDown()
    //{
    //    isCooling = true;

    //    for (float i = cooldownTime; i > 0; i -= 0.2f * Time.timeScale)
    //    {
    //        if (!Application.isPlaying) return;

    //        _coolingtime = i;

    //        await Task.Delay(TimeSpan.FromSeconds(0.2 * Time.timeScale));
    //    }
    //    isCooling = false;
    //}
}
