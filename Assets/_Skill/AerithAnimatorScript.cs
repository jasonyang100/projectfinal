﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AerithAnimatorScript : MonoBehaviour
{
    [SerializeField]Animator AerithAnimator;

    public int StatePhase;
    public bool IsActive;


    // Start is called before the first frame update
    void Start()
    {
        AerithAnimator = this.GetComponent<Animator>();
        StatePhase = -1;
        
        IsActive = true;
    }
    private void OnEnable()
    {
        IsActive = true;

    }



    // Update is called once per frame
    void Update()
    {
        AerithAnimator.SetBool("IsActive",IsActive);
        AerithAnimator.SetInteger("StatePhase", StatePhase);
    }
}
