﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Skill_ignis : MonoBehaviour
{
    public KeyCode m_keycode;//49~57 = 1~9
    public bool IsActive;
    public bool IsRecastable;
    public GameObject TragetGameObject;
    public GameObject MeGameObject;
    public GameObject SkillCarrierGameObject;
    public float InputDelayTime = 5;
    [SerializeField] float _InputDelayTime = 0;
    public float DeployTime = 10;
    [SerializeField] float _DeployTime = 10;


    //public Collider m_collider;
    //public Renderer m_renderer;
    //public Material[] m_materials;
    //public Material statsMaterial;
   

    // Start is called before the first frame update
    void Start()
    {            
                
        SkillCarrierGameObject.SetActive(false);

        
        IsActive = false;
        DeployTime = 3f;
    }

    // Update is called once per frame
    void Update()
    {
        deploySkill(triggered());
    }

   

    
    public bool triggered()
    {
        if (_InputDelayTime <= 0)
        {
            if (Input.GetKeyDown(m_keycode))
            {
                if (!IsRecastable)
                {
                    IsActive = !IsActive;
                }
                else if (IsRecastable)
                {
                    _DeployTime = DeployTime;
                    IsActive = true;

                }

                _InputDelayTime = InputDelayTime;
            }

        }
        _InputDelayTime -= Time.deltaTime;
        return IsActive;
    }


    public void deploySkill(bool _isActive)
    {
        if (_isActive)
        {

            _DeployTime -= Time.deltaTime;
            if (_DeployTime < 0)
            {
                IsActive = !_isActive;

            }
        }
        else _DeployTime = DeployTime;
        SkillCarrierGameObject.SetActive(IsActive);

    }

}
