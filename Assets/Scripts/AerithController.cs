﻿using EnumsManager;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AerithController : MonoBehaviour, IGameCharacterController
{
    private NonPlayer Aerith;
    
    private void Awake()
    {
        Aerith = new NonPlayer(gameObject);

        Aerith.characterName = CharacterName.Aerith;

        Aerith.CanInteraction = false;

        Aerith.CAnimator = transform.Find("AerithHolograph_standpose").GetComponent<Animator>();
    }

    // Start is called before the first frame update
    void Start()
    {
        Aerith.talks = new string[10]
        {
            "做得好，洛傑",
            "我隱約感應到些微的法力擾動，符石應該就在附近",
            "快去找吧，應該就在你右手邊的方向",
            "我已嘗試設置傳送門",
            "但這裡受到嚴重的法力干擾，導致我無法啟動",
            "應該是哪裏被設置了的魔法機關",
            "設法排除它，我才能協助你離開",
            "小心點，狂獵戰士瑞丁已覬覦符石的力量許久",
            "他不會放過這個絕佳的機會!",
            "通過傳送門後，前往維拉湖，我們在湖邊會合!",
        };
    }
    private void ActivateInteraction()
    {
        StartCoroutine(AuxiliaryTool.Instance.CoroutineWaiter(5.5f, () => Aerith.CanInteraction = true));
    }
    public GameCharacter ReturnGameCharacter()
    {
        return Aerith;
    }
}