﻿using System.Linq;
using UnityEngine;

public class SteeringBehavior
{   
    public static Vector3 Seek(Transform obj, Vector3 targetvec, Vector3 moveVector, float AccelerationFatcor = 1.0f) 
    {
        Vector3 fromPos = xzVector3(obj.position); 

        Vector3 targetPos = xzVector3(targetvec);

        Vector3 targetVector = targetPos - fromPos;

        Vector3 seekAcc = (targetVector - moveVector).normalized * AccelerationFatcor;

        return (moveVector + seekAcc).normalized * moveVector.magnitude;
    }
    public static Vector3 Flee(Transform obj, Vector3 targetvec, Vector3 currMovevec, float AccelerationFatcor = 1.0f)
    {
        return -Seek(obj, targetvec, currMovevec, AccelerationFatcor);
    }
    public static Vector3 Arrival(Transform obj, Vector3 targetvec, Vector3 moveVector, float decelerationRadius = 2f, float offset = 1f)
    {
        Vector3 fromPos = xzVector3(obj.position);

        Vector3 targetPos = xzVector3(targetvec);

        float distance = Vector3.Distance(fromPos, targetPos);

        if (distance <= offset) return Vector3.zero;

        return moveVector * Mathf.Min(1f, distance / decelerationRadius);
    }
    public static Vector3 ObstacleAviodance(Transform obj, Vector3 moveVector, Vector3 targetvec, Vector3 probecenter, Vector3 probesize, string tagname, float aviodpower = 1f)
    {
        Quaternion q = Quaternion.Euler(moveVector);

        IOrderedEnumerable<Collider> orderedEnumerableCol = Physics.OverlapBox(probecenter, probesize / 2, q)
                                                            .Where(c => c.CompareTag(tagname))
                                                            .OrderBy(c => Vector3.Distance(probecenter, c.transform.position));
     
        if (orderedEnumerableCol.Count() == 0) return moveVector;

        Collider nearestcol;

        if (orderedEnumerableCol.FirstOrDefault().transform == obj) nearestcol = orderedEnumerableCol.Skip(1).FirstOrDefault();

        else nearestcol = orderedEnumerableCol.FirstOrDefault();

        if (nearestcol == null) 
        {
            if(Physics.Raycast(obj.position, targetvec, out RaycastHit hit))
            {
                if (hit.transform.tag == tagname) return obj.forward;
                else return moveVector;
            }
            else return moveVector;
        }
        else
        {
            Vector3 fromPos = xzVector3(obj.position);

            Vector3 nearestcolPos = xzVector3(nearestcol.transform.position);

            Vector3 obstacleVec = nearestcolPos - fromPos;

            Vector3 normalVec = Vector3.Cross(moveVector, obstacleVec).normalized;

            Vector3 avioddir = Vector3.Cross(moveVector, normalVec).normalized;

            Vector3 closestpos = nearestcol.ClosestPointOnBounds(obj.position);

            float closestdistance = (xzVector3(closestpos) - fromPos).magnitude;

            if (closestdistance <= 0) closestdistance = 0.1f;

            //Debug.Log(probesize.z / closestdistance);

            Vector3 aviodAcc = avioddir * (probesize.z / closestdistance) * aviodpower;

            //Debug.DrawLine(nearestcol.transform.position, nearestcol.transform.position + aviodAcc, Color.blue);

            return (moveVector + aviodAcc).normalized * moveVector.magnitude;
        }
    }
    public static Vector3 Wander(Vector3 objpos, float wanderRadius)
    {
        float nexttheta = Random.Range(0, 2 * Mathf.PI);

        float nextx = objpos.x + wanderRadius * Mathf.Cos(nexttheta);

        float nextz = objpos.z + wanderRadius * Mathf.Sin(nexttheta);

        return new Vector3(nextx, 0f, nextz);
    }

    private static Vector3 xzVector3( Vector3 vec3 )
    {
        return new Vector3(vec3.x, 0.0f, vec3.z);
    }
}
