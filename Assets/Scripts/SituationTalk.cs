﻿
using UnityEngine;

public class SituationTalk : MonoBehaviour
{
    public string talk;
    public float delaytime = 0f;

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            StartCoroutine(AuxiliaryTool.Instance.CoroutineWaiter(
                delaytime, () => NPCinteraction.Instance.OnSituationTalk(talk, 2.5f)));
            GetComponent<Collider>().enabled = false;
            Destroy(gameObject, delaytime + 0.1f);
        }
    }
}
