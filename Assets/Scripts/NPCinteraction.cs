﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using EnumsManager;

public class NPCinteraction : MonoBehaviour
{
    public static NPCinteraction Instance;
    public GameObject NPCArith;
    private GameObject InteractionPanel;

    private GameObject SituationContextPanel;
    private Text SituationContextText;
    private Animation SituationContextAnimation;
    private bool isSituationtextshow = false;

    private GameObject DialoguePanel;
    private Text NPCDialogues;

    private GameObject Item;
    private Image ItemIcon;
    private Text ItemHeader;
    private Text ItemInstruction;

    public Image ItemOnSkillPanel;

    Queue<string> talksContain = new Queue<string>();

    TargetingSystem targetingSystem;
    //GameObject target = null;
    //Camera cam;
    private InteractionState interactionState = InteractionState.Start;

    private void Awake()
    {
        Instance = this;
        DontDestroyOnLoad(this);
    }
    private void Start()
    {
        InteractionPanel = transform.Find("InteractionPanel").gameObject;
        InteractionPanel.SetActive(false);
        
        SituationContextPanel = transform.Find("SituationContextPanel").gameObject;
        SituationContextPanel.SetActive(false);
        SituationContextText = transform.Find("SituationContextPanel").Find("SituationContext").GetComponent<Text>();
        SituationContextAnimation = transform.Find("SituationContextPanel").Find("SituationContext").GetComponent<Animation>();
        SituationContextAnimation["TextFadeIn"].speed = 12f;


        DialoguePanel = transform.Find("DialoguePanel").gameObject;
        DialoguePanel.SetActive(false);
        NPCDialogues = transform.Find("DialoguePanel").Find("NPCDialogues").GetComponent<Text>();

        Item = transform.Find("Item").gameObject;
        ItemIcon = transform.Find("Item").Find("Icon").GetComponent<Image>();
        ItemHeader = transform.Find("Item").Find("Header").GetComponent<Text>();
        ItemInstruction = transform.Find("Item").Find("Instruction").GetComponent<Text>();
        Item.SetActive(false);
        targetingSystem = FindObjectOfType<TargetingSystem>();
        //cam = Camera.main;
    }
    private void LateUpdate()
    {
        OnNonMobNPCInteractive();
    }
    public bool OnNPCInteraction(GameObject target)
    {
        if (!targetingSystem.isselectedNonMobInteractable()) return false;
        if (target == null) return false;

        if (target.TryGetComponent<IGameCharacterController>(out IGameCharacterController controller))
        {
            NonPlayer nonPlayer = controller.ReturnGameCharacter() as NonPlayer;

            if (!nonPlayer.CanInteractionRepeat) return false;
            if (nonPlayer.npcType == NPCType.FriendlyNPC) return OnNPCTalk(nonPlayer);
            else if (nonPlayer.npcType == NPCType.Item) return OnItemInteraction(nonPlayer as Item);
            else return false;
        }
        else return false;
    }

    private IEnumerator ItemInteraction(Item item)
    {
        item.m_animation.Play();

        AudioManager.instance.Play("46_Treasure");

        targetingSystem.isInteractingWithNPC = true;

        while (item.m_animation.isPlaying)
        {
            yield return null;
        }

        ItemHeader.text = item.ItemName;

        ItemInstruction.text = item.Instruction;
        
        ItemIcon.sprite = item.ItemSprite;

        Item.SetActive(true);

        ItemOnSkillPanel.enabled = true;

        yield break;
    }
    private bool OnItemInteraction(Item item)
    {
        if (interactionState == InteractionState.Start)
        {
            interactionState = InteractionState.Interacting;

            StartCoroutine(ItemInteraction(item));

            return true;
        }
        else
        { 
            ItemHeader.text = "";
            ItemInstruction.text = "";
            ItemIcon.sprite = null;
            Item.SetActive(false);
            item.CanInteractionRepeat = false;
            targetingSystem.isInteractingWithNPC = false;
            item.CanInteraction = false;
            interactionState = InteractionState.Start;
            AudioManager.instance.Play("8_UI_Pick");
            NPCArith.SetActive(false);
            return false;
        }
    }
    public void OnInteractionEnd()
    {
        ItemHeader.text = "";
        ItemInstruction.text = "";
        ItemIcon.sprite = null;
        Item.SetActive(false);
        targetingSystem.isInteractingWithNPC = false;
        interactionState = InteractionState.Start;
        NPCDialogues.text = "";
        DialoguePanel.SetActive(false);
        talksContain.Clear();
    }
    private void OnDialogueStart(string[] talks)
    {
        NPCDialogues.text = "";

        DialoguePanel.SetActive(true);

        foreach (var paragraph in talks)
        {
            talksContain.Enqueue(paragraph);
        }
        interactionState = InteractionState.Interacting;

        targetingSystem.isInteractingWithNPC = true;
    }
    private bool OnNPCTalk(NonPlayer nonPlayer)
    {
        if (!nonPlayer.CanInteractionRepeat) return false;
        if (interactionState == InteractionState.Start)
        {
            OnDialogueStart(nonPlayer.talks);
            nonPlayer.CAnimator.SetTrigger("Think");

            NPCDialogues.text = talksContain.Dequeue();

            return true;
        }
        else
        {
            if (talksContain.Count == 0)
            {
                OnDialogueEnd(nonPlayer, true);
                return false;
            }
            else
            {
                NPCDialogues.text = talksContain.Dequeue();
                return true;
            }
        }
    }
    private void OnDialogueEnd( NonPlayer nonPlayer, bool CanTalkRepeat )
    {
        NPCDialogues.text = "";

        DialoguePanel.SetActive(false);

        talksContain.Clear();

        interactionState = InteractionState.Start;

        nonPlayer.CanInteractionRepeat = CanTalkRepeat;

        targetingSystem.isInteractingWithNPC = false;
    }
    private void OnNonMobNPCInteractive()
    {
        if (!targetingSystem.isselectedNonMobInteractable()) return;

        if (interactionState == InteractionState.Interacting)
        {
            InteractionPanel.SetActive(false);
            return;
        }

        if (targetingSystem.GetNowMode() != TargetingMode.NonMobNPCInteractive)
        {
            InteractionPanel.SetActive(false);
        }
        else
        {
            if (targetingSystem.isNonMobNPCinRange())
            {
                InteractionPanel.SetActive(true);
                //target = targetingSystem.selectedNonMobNPCTarget();
                //if (target == null) return;
                //Vector3 pos = cam.WorldToScreenPoint(target.transform.position + new Vector3(0, 3.5f, 0));
                //InteractionIcon.transform.position = pos;
            }
            else InteractionPanel.SetActive(false);
        }
    }
    public void OnSituationTalk(string talk, float vanishtime)
    {
        if (isSituationtextshow) return;
        if (InteractionPanel.activeSelf == true || DialoguePanel.activeSelf == true) return;

        isSituationtextshow = true;

        SituationContextPanel.SetActive(true);

        SituationContextText.text = talk;

        SituationContextAnimation.Play();

        StartCoroutine(AuxiliaryTool.Instance.CoroutineWaiter(vanishtime, () =>
        {
            SituationContextText.text = "";

            SituationContextPanel.SetActive(false);

            isSituationtextshow = false;
        }));
    }
    private void DisableSituationContext()
    {
        SituationContextText.text = "";

        SituationContextPanel.SetActive(false);

        isSituationtextshow = false;
    }
}