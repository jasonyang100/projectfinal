﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class FlashSkillScript : MonoBehaviour
{
    [SerializeField]GameObject PlayersGameobject1;//character's render goes disable while flash skill is on
    [SerializeField]GameObject PlayersGameobject2;
    

    // Start is called before the first frame update
    void Awake()
    {

        PlayersGameobject1 = GameObject.Find("Character Warrior/Character");
        PlayersGameobject2 = GameObject.Find("Character Warrior/CATRigHub001");  
    }
    private void Start()
    {
        //PlayersGameobject1 = GameObject.Find("Character Warrior/Character");
        //PlayersGameobject2 = GameObject.Find("Character Warrior/CATRigHub001");
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void OnEnable()
    {
        PlayersGameobject1.SetActive(false);
        PlayersGameobject2.SetActive(false);

    }
    private void OnDisable()
    {
        PlayersGameobject1.SetActive(true);
        PlayersGameobject2.SetActive(true);
    }
}
