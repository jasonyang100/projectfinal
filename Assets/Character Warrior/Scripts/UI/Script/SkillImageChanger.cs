﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SkillImageChanger : MonoBehaviour
{
    Image image;
    ArrowImage arrowImage;
    int nowSkill;
    public Sprite[] sprites;

    private void Awake()
    {
        arrowImage = FindObjectOfType<ArrowImage>();
        image = GetComponent<Image>();
    }
    private void Update()
    {
        nowSkill = arrowImage.skillSwitch;
        switch (nowSkill) 
        {
            case 0:
                image.sprite = sprites[0];
                break;
            case 1:
                image.sprite = sprites[1];
                break;
            case 2:
                image.sprite = sprites[2];
                break;
        }
    }
}
