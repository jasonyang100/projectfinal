﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Reflection;
using System;
using EnumsManager;

public class PlayerController : MonoBehaviour, IGameCharacterController, IAttackableCharacter
{
    Player player;
    PlayerAI playerai;
    Type controller;
    Collider playercollider;
    Collider weaponcollider;
    Animator animator;
    ArrowImage arrowImage;
    private GameObject WeaponEffect;
    private List<Action> AnimActionList;
    private Dictionary<PlayerAttackForm, Func<Action<float>, float, IEnumerator>> dict_attackDamage;
    private Vector3 SwordSwingReferencePoint;

    private Dictionary<PlayerSkillType, PlayerSkill> skills = new Dictionary<PlayerSkillType, PlayerSkill>();
    public GameObject ShieldGO;
    public GameObject FlameGO;
    public GameObject FlashGO;

    public GameObject skillUI;
    private int skillSwitch;

    [SerializeReference]
    public GameObject NowSkill;

    private int _currentskillid = 0;
    public int CurrentSkillId
    {
        set => _currentskillid = Mathf.Clamp(value, 0, skills.Count - 1);
        get => _currentskillid;
    }
    public Camera cam;

    private void InitializeSkill()
    {
        PlayerSkill Shield = new PlayerSkill(ShieldGO);
        Shield.SetSkillData(5f, 7f, 0.02f);
        Shield.StartCastAct = () => StartCast((int)PlayerSkillType.Shield);
        Shield.CastingAct = CastShield;

        Shield.CoolDownAct = () => StartCoroutine(SpellCastManaRecovery(7f));

        ShieldGO.SetActive(false);
        skills.Add(PlayerSkillType.Shield, Shield);

        PlayerSkill Flame = new PlayerSkill(FlameGO);
        Flame.SetSkillData(0.7f, 7f, 0.22f);
        Flame.StartCastAct = () => StartCast((int)PlayerSkillType.Flame);
        Flame.CastingAct = CastFlame;
        Flame.isTrunOnOfforInstantiate = false;

        Flame.CoolDownAct = () => StartCoroutine(SpellCastManaRecovery(7f));

        FlameGO.SetActive(false);
        skills.Add(PlayerSkillType.Flame, Flame);

        PlayerSkill Flash = new PlayerSkill(FlashGO);
        Flash.SetSkillData(0.25f, 5f, 0.3f);
        Flash.StartCastAct = () => StartCast((int)PlayerSkillType.Flash);
        Flash.CastingAct = CastFlash;

        Flash.CoolDownAct = () => StartCoroutine(SpellCastManaRecovery(5f));

        FlashGO.SetActive(false);
        skills.Add(PlayerSkillType.Flash, Flash);
    }
    private void StartCast(int skillid)
    {
        AnimatorStateInfo currentAnimState = animator.GetCurrentAnimatorStateInfo(0);

        if (currentAnimState.IsName("AttackModeMove")
            || currentAnimState.IsName("NormalModeMove")
            || currentAnimState.IsName("PlayerIdle")
            || currentAnimState.IsName("AttackIdle"))
        {
            animator.SetInteger("isMagicAttack", skillid);
            animator.SetTrigger("isMagic");
        }
    }
    public IEnumerator SpellCastManaRecovery(float CoolDownTime)
    {
        if (player.Mana < 100) yield break;

        player.Mana = 0;

        CharacterStatusUI.Instance.SetNowMana(player.Mana);

        float RecoveryValuePerTimeSpan = (100 * player.ManaRecoveryTimeSpan) / CoolDownTime;

        for (float fl = player.ManaRecoveryTimeSpan; fl <= CoolDownTime; fl += player.ManaRecoveryTimeSpan)
        {
            yield return new WaitForSeconds(player.ManaRecoveryTimeSpan);

            player.Mana += RecoveryValuePerTimeSpan;

            CharacterStatusUI.Instance.SetNowMana(player.Mana);
        }
        yield break;
    }

    private void CastShield(bool bo)
    {
        if (bo)
        {
            playerai.isShielding = true;
            AudioManager.instance.Play("22_Player_MagicShield");
        }
        else playerai.isShielding = false;
    }
    private void CastFlame(bool bo)
    {
        if (bo) AudioManager.instance.Play("23_Player_MagicIgnis");
    }
    private void CastFlash(bool bo)
    {

        if (bo)
        {
            AudioManager.instance.Play("24_Player_MagicFlash");
            gameObject.SendMessage("PlayerFlshCast");
        }
    }
    private void OnCastSkill()
    {
        if (Input.GetKeyDown(KeyCode.Q))
        {
            StartCoroutine(skills[(PlayerSkillType)CurrentSkillId].Cast((player.Mana < 100f)));//true=在冷卻
        }
    }
    private void OnSwitchSkill()
    {
        if (Input.GetKey(KeyCode.R))
        {
            skillSwitch = arrowImage.skillSwitch;
            Time.timeScale = 0.05f;
            // 打開UI介面
            skillUI.SetActive(true);
            switch (skillSwitch)
            {
                case 0:
                    CurrentSkillId = 0;
                    break;
                case 1:
                    CurrentSkillId = 1;
                    break;
                case 2:
                    CurrentSkillId = 2;
                    break;
            }
            NowSkill = skills[(PlayerSkillType)CurrentSkillId].skillInstance;
        }
        if (Input.GetKeyUp(KeyCode.R))
        {
            Time.timeScale = 1f;
            skillUI.SetActive(false);
        }
    }
    private void Awake()
    {
        InitializeSkill();
        controller = this.GetType();
        animator = GetComponent<Animator>();
        playercollider = GetComponent<Collider>();
        //-------UI---------
        skillUI = GameObject.Find("Skill_UI");
        arrowImage = FindObjectOfType<ArrowImage>();
        //-------UI---------
        player = new Player(gameObject);
        player.ShieldReduction = 0.5f;
        player.Ccontroller = GetComponent<CharacterController>();

        dict_attackDamage = new Dictionary<PlayerAttackForm, Func<Action<float>, float, IEnumerator>>
        {
            { PlayerAttackForm.NormalAttack, NormalAttack },
            { PlayerAttackForm.Flame, Flame },
            { PlayerAttackForm.DashAttack, DashAttack }
        };

        player.m_strShieldSoundEffect = "17_Player_SwordHitBody3";
        player.m_strUnderAttackSoundEffect = "17_Player_SwordHitBody3";

        playerai = new PlayerAI(player);
        playerai.opposingGoTag = new HashSet<string> { "Mob" };
        AnimActionList = InitializeAnimActionList();

        cam = Camera.main;

    }
    private void Start()
    {
        //------------UI----------
        skillUI.SetActive(false); // Skill UI
        skillSwitch = arrowImage.skillSwitch;
        //------------UI----------
        WeaponEffect = Instantiate(EffectManager.Instance.WeaponEffectdict["SwordFlashEffect"]);
        WeaponEffect.transform.SetParent(weaponcollider.transform);
        WeaponEffect.transform.localPosition = new Vector3(0f, 0f, -60f);
        WeaponEffect.transform.localRotation = Quaternion.Euler(0f, -90f, 0f);
        WeaponEffect.transform.localScale = Vector3.one;

        if (GameManager.g_instance.isGameStart)
        {
            player.InitialHpMana(35f, 100f);
        }
        else
        {
            player.InitialHpMana(GameManager.g_instance.PlayerMaxHp, 100f);
            player.UpdateCurrentHp(GameManager.g_instance.PlayerCurrentHp);
        }
    }

    private void Update()
    {
        OnSwitchSkill();
        OnCastSkill();
    }
    private void OnCollisionEnter(Collision collision)
    {
    }
    private List<Action> InitializeAnimActionList()
    {
        return new List<Action>()
        {
            () => //Damaged
            {
                AudioManager.instance.Play("27_Player_GetHit");
                weaponcollider.enabled = false;
                animator.SetTrigger("getHit");
                WeaponEffect.SetActive(false);
            },
            () => //Die
            {
                AudioManager.instance.Play("29_Player_Dead");
                WeaponEffect.SetActive(false);
                animator.SetBool("isDead",true);
                GetComponent<PlayerMove>().enabled = false;
                weaponcollider.enabled = false;
                playercollider.enabled = false;
                foreach(var mob in player.fightObservers)
                {
                    mob.CgameObject.SendMessage("OnOpposingGoDie", gameObject);
                }
                //StopAllCoroutines();
                player.Ccontroller.enabled = false;
            },
        };
    }
    public void CallCharacterRegistrar(object[] obj)
    {
        GameCharacter gc = obj[0] as GameCharacter;
        bool bo = (bool)obj[1];
        if (bo) player.Register(gc);
        else player.Unregister(gc);
    }
    private IEnumerator NormalAttack(Action<float> attact, float reduction)
    {
        float originaldamage = 1f;
        float damage = Mathf.Clamp(0f, originaldamage * (1 - reduction), originaldamage);
        attact(damage);
        yield break;
    }
    private IEnumerator Flame(Action<float> attact, float reduction)
    {
        float originaldamage = 0.8f;
        float damage = Mathf.Clamp(0f, originaldamage * (1 - reduction), originaldamage);
        attact(damage);
        yield return new WaitForSeconds(0.2f);
        for (int i = 0; i < 5; ++i)
        {
            originaldamage = 0.2f;
            damage = Mathf.Clamp(0f, originaldamage * (1 - reduction), originaldamage);
            attact(damage);
            yield return new WaitForSeconds(0.2f);
        }
        yield break;
    }
    private IEnumerator DashAttack(Action<float> attact, float reduction)
    {
        float originaldamage = 2f;
        float damage = Mathf.Clamp(0f, originaldamage * (1 - reduction), originaldamage);
        attact(damage);
        yield break;
    }
    private IEnumerator OnHitEffect(Collider col)
    {
        Vector3 hitpos = col.ClosestPointOnBounds(SwordSwingReferencePoint);
        GameObject SlashHit_VFX2 = Instantiate(EffectManager.Instance.HitEffectdict["SlashHit_VFX2"]);
        Vector3 approximatenormal = -(SwordSwingReferencePoint - col.bounds.center);
        SlashHit_VFX2.transform.position = hitpos;
        SlashHit_VFX2.transform.forward = approximatenormal;
        SlashHit_VFX2.SetActive(true);
        Destroy(SlashHit_VFX2, 1f);
        cam.SendMessage("StartShake", new object[2] { 0.15f, 0.2f });

        string HitSoundEffect = col.GetComponent<IAttackableCharacter>().UnderAttackSoundEffect();
        AudioManager.instance.Play(HitSoundEffect);

        int rnd = UnityEngine.Random.Range(0, 57);
        if (rnd == 0) NPCinteraction.Instance.OnSituationTalk("洛傑: 死吧!", 2f);
        else if(rnd == 1) NPCinteraction.Instance.OnSituationTalk("洛傑: 再來啊!", 2f);

        for (int i = 0; i <= 10; i += 4)
        {
            Time.timeScale = i * 0.1f;
            yield return new WaitForSeconds(i * 0.005f);
        }
        Time.timeScale = 1f;
        yield break;
        //Ray ray = new Ray(weaponcollider.transform.position, -weaponcollider.transform.forward);
        //RaycastHit hit;

        //if (Physics.Raycast(ray, out hit, 1 << 8))
        //{
        //    //GameObject BloodEffect = Instantiate( EffectManager.Instance.HitEffectdict["BloodEffect"] );
        //    //Vector3 pos = hit.point;
        //    //Vector3 approximatenormal = pos - colcenter;
        //    //BloodEffect.transform.position = pos;
        //    //BloodEffect.transform.forward = approximatenormal;
        //    //BloodEffect.SetActive(true);
        //    //StartCoroutine(AuxiliaryTool.Instance.CoroutineWaiter(0.5f, () =>  Destroy(BloodEffect) ));
        //    GameObject SlashHit_VFX2 = Instantiate(EffectManager.Instance.HitEffectdict["SlashHit_VFX2"]);
        //    Vector3 pos = hit.point;
        //    Vector3 approximatenormal = pos - colcenter;
        //    SlashHit_VFX2.transform.position = pos;
        //    SlashHit_VFX2.transform.forward = approximatenormal;
        //    SlashHit_VFX2.SetActive(true);
        //    StartCoroutine(AuxiliaryTool.Instance.CoroutineWaiter(0.5f, () => Destroy(SlashHit_VFX2)));

        //}
    }
    private void UnderAttack(object[] obj)
    {
        Vector3 HitPos = (obj[0] as Transform).position;
        Func<Action<float>, float, IEnumerator> HitDel = obj[1] as Func<Action<float>, float, IEnumerator>;

        float reduction = 0;

        if (playerai.isShielding) reduction = player.ShieldReduction;
        else AnimActionList[0]();

        StartCoroutine(HitDel(damage => player.GetHit(damage), reduction));

        if (!playerai.isShielding) StartCoroutine(BloodScreenEffect());
        cam.SendMessage("StartShake", new object[2] { 0.1f, 0.1f });

        int rnd = UnityEngine.Random.Range(0, 30);

        if (rnd == 0) NPCinteraction.Instance.OnSituationTalk("洛傑: 不妙", 2f);
        else if(rnd == 1) NPCinteraction.Instance.OnSituationTalk("洛傑: 可惡!", 2f);

        if (player.isDead)
        {
            AnimActionList[1]();
            GetComponent<PlayerMove>().enabled = false;
            playercollider.enabled = false;
        }
    }
    private IEnumerator BloodScreenEffect()
    {
        Color tempColor = CharacterStatusUI.Instance.BloodScreen.color;
        if (tempColor.a != 0) yield break;
        tempColor.a = 0.6f;
        CharacterStatusUI.Instance.BloodScreen.color = tempColor;
        for (int i = 0; i < 30; i++)
        {
            if (player.isDead) yield break;
            tempColor.a -= 0.02f;
            CharacterStatusUI.Instance.BloodScreen.color = tempColor;
            yield return null;
        }
        tempColor.a = 0f;
        CharacterStatusUI.Instance.BloodScreen.color = tempColor;
        yield break;
    }
    private void AttackOnHit(object[] obj)
    {
        Collider col = obj[0] as Collider;
        string Hittag = obj[1] as string;
        if (!playerai.opposingGoTag.Contains(col.tag)) return;
        AnimatorStateInfo currentAnimState = animator.GetCurrentAnimatorStateInfo(0);
        switch (Hittag)
        {
            case "Weapon":
                StartCoroutine(OnHitEffect(col));
                if (currentAnimState.IsName("DashAttack"))
                    col.SendMessage("UnderAttack", new object[2] { transform, dict_attackDamage[PlayerAttackForm.DashAttack] });
                else
                    col.SendMessage("UnderAttack", new object[2] { transform, dict_attackDamage[PlayerAttackForm.NormalAttack] });
                //weaponcollider.enabled = false;
                break;
            case "Flame":
                col.SendMessage("UnderAttack", new object[2] { transform, dict_attackDamage[PlayerAttackForm.Flame] });
                col.SendMessage("GetBurning");
                break;
            default:
                break;
        }
    }
    private void AddWeaponGameObject(Collider col) => weaponcollider = col;
    void OnAnimationAttackStart()
    {
        AnimatorStateInfo currentAnimState = animator.GetCurrentAnimatorStateInfo(0);
        if (currentAnimState.IsTag("SwordAttack") && !currentAnimState.IsName("Get_Hit") && !animator.IsInTransition(0))
        {
            SwordSwingReferencePoint = weaponcollider.bounds.center;
            weaponcollider.enabled = true;
            WeaponEffect.SetActive(true);
        }
    }
    void OnAnimationAttackEnd()
    {
        weaponcollider.enabled = false;
        WeaponEffect.SetActive(false);
    }
    public GameCharacter ReturnGameCharacter()
    {
        return player;
    }
    public string UnderAttackSoundEffect()
    {
        if (playerai.isShielding) return player.m_strShieldSoundEffect;
        else return player.m_strUnderAttackSoundEffect;
    }
}