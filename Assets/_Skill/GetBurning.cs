﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

/// <summary>
/// aborted,please use getburning2
/// </summary>
public class GetBurning : MonoBehaviour
{
    List<Material> MaterialList;
    Material StatusMaterial;
    [SerializeField] float Timer = 5f;
    float _Timer;
    // Start is called before the first frame update
    void Start()
    {
        _Timer =Timer;
        StatusMaterial = Resources.Load<Material>("Burning_Material");
        MaterialList = new List<Material>();
        MaterialList = transform.GetComponent<Renderer>().materials.ToList<Material>();
        MaterialList.Add(StatusMaterial);
        transform.GetComponent<Renderer>().materials = MaterialList.ToArray();


    }

    // Update is called once per frame
    void Update()
    {
        _Timer -= Time.deltaTime;
        if (_Timer < 0)
        {
            EndBurning();
        }
        
    }

    void EndBurning()
    {
        MaterialList.Remove(StatusMaterial);
        transform.GetComponent<Renderer>().materials = MaterialList.ToArray();
        _Timer = Timer;
        Destroy(this);

    }
}
