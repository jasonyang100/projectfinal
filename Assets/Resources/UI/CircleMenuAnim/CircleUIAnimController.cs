﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CircleUIAnimController : MonoBehaviour
{
    Animator animator;
    public bool IsSelected;
    // Start is called before the first frame update
    void Start()
    {
        animator = GetComponent<Animator>();
        animator.SetBool("IsSelected", false);
    }

    // Update is called once per frame
    void Update()
    {
        setbool(IsSelected);
    }

    void setbool(bool b)
    {
        if (b)
        {
            animator.SetBool("IsSelected", true);
        }
        else
        {
            animator.SetBool("IsSelected", false);
        }

    }
}
