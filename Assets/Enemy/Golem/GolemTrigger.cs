﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GolemTrigger : MonoBehaviour
{
    [SerializeField]
    private GameObject Golem;

    private void Awake() => Golem = GameObject.Find("Golem");

    private void OnTriggerEnter(Collider other)
    {
        if (!other.CompareTag("Player")) return;
        Golem.GetComponent<GolemController>().enabled = true;
        Golem.SendMessage("Assembly", gameObject);
    }
}
