﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using EnumsManager;

public class BossFloatingBar : MonoBehaviour
{
    GameCharacter Boss;
    [SerializeField] Image Container;
    [SerializeField] Image HpBar;
    [SerializeField] Image image;

    void Start()
    {
        Boss = GameObject.Find("WildHunt").GetComponent<IGameCharacterController>().ReturnGameCharacter();
        
        Container = transform.Find("Container").GetComponent<Image>();
        HpBar = transform.Find("Container").Find("HpBar").GetComponent<Image>();
        image = transform.Find("Container").Find("HpBar").Find("Image").GetComponent<Image>();

    }

    private void LateUpdate()
    {
        UpdateHpBar();
    }
    float UpdateHpBar()
    {
        float p = (Boss.currentHP / Boss.MaxHP);
        HpBar.fillAmount = p;
        return p;
    }
}