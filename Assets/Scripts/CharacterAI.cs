﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using EnumsManager;

public abstract class CharacterAI
{
    protected GameCharacter character;

    protected TimerManager timer;

    public HashSet<string> opposingGoTag;

    protected HashSet<GameObject> HashSet_fightingGo = new HashSet<GameObject>();

    private GameObject _aiminggo = null;
    public GameObject aimingGo
    {
        get => _aiminggo;
        set
        {
            if (_aiminggo == value) return;
            if (value == null || !(opposingGoTag.Contains(value.tag)) ) value = null;
            _aiminggo = value;
        }
    }
    private Vector3 _targetvec;
    protected Vector3 TargetVec
    {
        get
        {
            if (aimingGo == null) return _targetvec;
            else return aimingGo.transform.position;
        }
        set => _targetvec = value;
    }

    private bool _isactiondone = false;
    public virtual bool isActionDone
    {
        set => _isactiondone = value;
        get
        {
            bool tempb = _isactiondone;
            _isactiondone = false;
            return tempb;
        }
    }

    public float alertRadius { set; get; } // 偵測敵人半徑, 搭配視野視角
    public float SightRangeAngle { set; get; }

    public CharacterAI(GameCharacter character)
    {
        timer = new TimerManager();
        this.character = character;
        this.character.characterAI = this;
    }

    protected bool VisionDetect(Transform charatransform, float radius, float angle, float eyeoffset, string tagname)
    {
        Collider[] cols = Physics.OverlapSphere(charatransform.position, radius)
            .Where(c => c.CompareTag(tagname))
            .Where(c => Vector3.Angle(charatransform.forward, c.transform.position - charatransform.position) < angle)
            .ToArray();
        
        List<Collider> seecolslist = new List<Collider>();
        foreach (var item in cols)
        {           
            Vector3 origin = new Vector3(charatransform.position.x, charatransform.position.y + eyeoffset, charatransform.position.z);
            Vector3 Dir = item.bounds.center - origin;
            Physics.Raycast(origin, Dir, out RaycastHit hitpoint, radius);
            if (hitpoint.transform == item.transform) seecolslist.Add(item);
        }

        if (seecolslist.Count > 0)
        {
            foreach (var col in cols)
            {
                HashSet_fightingGo.Add(col.gameObject);
            }
            GameObject CGo = cols.First().gameObject;
            CallSubjectControllerRegistrar(character, true);

            if (CGo != aimingGo)
            {
                aimingGo = CGo;
            }
            return true;
        }
        else
        {
            //aimingGo = null;
            return false;
        }
    }
    public void CallSubjectControllerRegistrar(GameCharacter character, bool registerorunregister)
    {
        if (HashSet_fightingGo.Count == 0) return;
        object[] objarr = new object[2] { character, registerorunregister };

        foreach (var go in HashSet_fightingGo)
        {
            go.SendMessage("CallCharacterRegistrar", objarr);
        }
    }
}
