﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EnumsManager;
using System.Linq;

public class MobAI : CharacterAI
{
    public MobState currentState = MobState.IDLE;

    public MobState lastState = MobState.NOTSTATE;

    public MobState cutinState = MobState.NOTSTATE;

    public MobNormalStateType mobLocomotionType;

    private HashSet<GameObject> HashSet_Companies = new HashSet<GameObject>();

    private Vector3 _gethitdir;
    public Vector3 GetHitDir { get => _gethitdir; set { _gethitdir = new Vector3(value.x, 0f, value.z); } }

    private bool _ishitwall = false;
    public bool isHitWall
    {
        set => _ishitwall = value;
        get
        {
            bool tempb = _ishitwall;
            _ishitwall = false;
            return tempb;
        }
    }
    public Vector3 initialPosition { set; get; }

    private float _idleresttime = 20f;
    public float idleRestTime 
    {
        set => _idleresttime = value;
        get
        {
            int seed = Mathf.RoundToInt(character.Ctransform.position.x); //以當前位置的x值得到一個隨機種子
            UnityEngine.Random.InitState(seed);
            return UnityEngine.Random.Range(_idleresttime - 2f, _idleresttime + 2f);
        }  
    }
    public float wanderRadius { set; get; } // 閒逛半徑
    public float returnRadius { set; get; } // 超過出生點回到原位半徑
    public float attackRadius { set; get; }

    public float minAttackTimeSpan = 0.2f;
    public float maxAttackTimeSpan = 3f;

    private float _attacktimespan = 1.6f;

    public float rotateAbility = 0.05f;

    public float attackTimeSpan
    {
        set 
        {
            value = Mathf.Clamp(value, minAttackTimeSpan, maxAttackTimeSpan);

            _attacktimespan = value;
        }
        get => _attacktimespan;
    }
    // if true, the Mob won't return to initial position and fight until die.
    public bool neverFallback = false;

    public bool TurnOnCompaniesAviodance = false;

    public MobAI(Mob mob) : base(mob)
    {
        var cols = Physics.OverlapSphere(character.Ctransform.position, 20f).Where(c => c.CompareTag("Mob"));

        foreach (var com in cols)
        {
            if (com.gameObject == mob.CgameObject) continue;
            HashSet_Companies.Add(com.gameObject);
        }
    }

    protected bool DetectPlayer()
    {
        return base.VisionDetect(character.Ctransform, alertRadius, SightRangeAngle, 4f, "Player");        
    }
    private bool CheckReturn()
    {
        if (neverFallback) return false;
        float distance = Vector3.Distance(character.Ctransform.position, initialPosition);

        if (distance > returnRadius)
        {
            aimingGo = null;
            CallSubjectControllerRegistrar(character, false);
            HashSet_fightingGo.Clear();
            aimingGo = null;
            return true;
        }
        else return false;
    }
    private bool CopeWithDeath()
    {
        if (character.isDead)
        {
            CallSubjectControllerRegistrar(character, false);
            HashSet_fightingGo.Clear();
            HashSet_Companies.Clear();
            aimingGo = null;
            return true;
        }
        else return false;
    }

    public void CallCompanies()
    {
        if (aimingGo == null) return;

        foreach(var com in HashSet_Companies)
        {
            HashSet_fightingGo.Add(aimingGo);          

            com.SendMessage("RetriveCompanyAlert", aimingGo);
        }
    }
    private void RotateToTarget()
    {
        if (aimingGo != null) 
        {
            Vector3 aimingGodir = aimingGo.transform.position - character.Ctransform.position;
            Vector3 lookatdir = new Vector3(aimingGodir.x, 0f, aimingGodir.z).normalized;
            character.Ctransform.rotation =
                Quaternion.Slerp(character.Ctransform.rotation, Quaternion.LookRotation(lookatdir), rotateAbility);
        }
        else
        {
            character.Ctransform.rotation = 
                Quaternion.Slerp(character.Ctransform.rotation, Quaternion.LookRotation(GetHitDir), rotateAbility);
        }
    }

    private bool MoveTo(Vector3 moveVec, float arrivaloffset = 1f)
    {
        Vector3 moveVector = SteeringBehavior.Seek(character.Ctransform, TargetVec, moveVec);

        if (TurnOnCompaniesAviodance)
        {
            Vector3 center = character.Ccollider.bounds.center + moveVector.normalized * 5f;

            Vector3 probe = new Vector3(character.Ccollider.bounds.size.x, character.Ccollider.bounds.size.y, 10f);

            moveVector = SteeringBehavior.ObstacleAviodance(character.Ctransform, moveVector, TargetVec, center, probe, "Mob", 0.35f);
        }

        moveVector = SteeringBehavior.Arrival(character.Ctransform, TargetVec, moveVector, offset: arrivaloffset);

        if (moveVector == Vector3.zero) return true;

        else character.Ctransform.rotation = Quaternion.Slerp(character.Ctransform.rotation, Quaternion.LookRotation(moveVector.normalized), 1f);

        character.Ccontroller.Move(moveVector * Time.deltaTime);

        return false;
    }
    private void PreventStateStuck(MobState state)//狀態機卡住,強制跳離
    {
    }

    public MobState Idle()
    {
        if (lastState != currentState) isActionDone = false;

        if (DetectPlayer()) return MobState.ALERT;

        //bool istirrger = timer.ClockTimer(0.1f, idleRestTime,
        //    () => TargetVec = SteeringBehavior.Wander(initialPosition, wanderRadius),"IDLE");
        
        if (isActionDone)
        {
            if (mobLocomotionType == MobNormalStateType.Wander)
            {
               
                TargetVec = SteeringBehavior.Wander(initialPosition, wanderRadius);
                return MobState.WANDER;
            }
            else if(mobLocomotionType == MobNormalStateType.IdleOnly)
            {
                return MobState.IDLE;
            }
        }
        if (CheckReturn()) return MobState.RETURN;
        return MobState.IDLE;
    }
    public MobState Wander()
    {
        if (lastState != currentState) isActionDone = false;

        if (DetectPlayer()) return MobState.ALERT;
        bool isarrive = MoveTo(character.moveVector(MobState.WANDER));
        if (isHitWall)
        {
            character.Ctransform.rotation = Quaternion.Slerp(character.Ctransform.rotation, Quaternion.LookRotation(-character.Ctransform.forward), 1f);
            return MobState.IDLE;
        }
        if (isarrive) return MobState.IDLE;
        return MobState.WANDER;
    }
    public MobState Alert()
    {
        if (lastState != currentState) isActionDone = false;

        if (CheckReturn()) return MobState.RETURN;
        if (CopeWithDeath()) return MobState.DIE;
        RotateToTarget();
        if (aimingGo == null)
        {
            DetectPlayer();
            if (isActionDone) return MobState.IDLE;
        }
        else
        {
            if (isActionDone)
            {
                float distance = Vector3.Distance(aimingGo.transform.position, character.Ctransform.position);
                //if (distance > alertRadius) return MobState.IDLE;
                if (distance > attackRadius) return MobState.CHASE;
                else return MobState.FIGHT;
            }
        }
        return MobState.ALERT;
    }
    public MobState Chase()
    {
        if (lastState != currentState) isActionDone = false;
        bool isarrive = MoveTo(character.moveVector(MobState.CHASE), attackRadius);
        //if (isHitWall) return MobState.ALERT;
        if (CopeWithDeath()) return MobState.DIE;
        if (isarrive) return MobState.FIGHT;
        if (CheckReturn()) return MobState.RETURN;
        return MobState.CHASE;
    }
    public MobState Fight()
    {
        if (lastState != currentState) isActionDone = false;
        if (CopeWithDeath()) return MobState.DIE;
        if (isActionDone) return MobState.ALERT;
        return MobState.FIGHT;
    }
    public MobState Defense()
    {
        if (lastState != currentState) isActionDone = false;

        if (CopeWithDeath()) return MobState.DIE;
        RotateToTarget();
        if (isActionDone) return MobState.ALERT;
        return MobState.DEFENSE;
    }
    public MobState Damaged()
    {
        if (lastState != currentState) isActionDone = false;

        if (CopeWithDeath()) return MobState.DIE;
        if (isActionDone) return MobState.ALERT;
        return MobState.DAMAGED; 
    }
    public MobState Return()
    {
        if (lastState != currentState) isActionDone = false;

        TargetVec = initialPosition;
        bool isarrive = MoveTo(character.moveVector(MobState.RETURN));
        if (isHitWall)
        {
            character.Ctransform.position = initialPosition;
            return MobState.IDLE;
        }
        if (isarrive) return MobState.IDLE;
        return MobState.RETURN;
    }
    public MobState Die() => MobState.DIE;
}