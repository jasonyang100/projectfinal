﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class AudioManager : MonoBehaviour
{
    public static AudioManager instance;
    TargetingSystem targetingSystem;
    bool isPlayBattleBGM;
    public Sound[] gameSounds;
    
    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(this);
            name = "AudioManager";
        }
        else
            Destroy(gameObject);

        foreach(Sound s in gameSounds)
        {
            s.source = gameObject.AddComponent<AudioSource>();
            s.source.clip = s.clip;
            s.source.volume = s.volume;
            s.source.pitch = s.pitch;
            s.source.loop = s.loop;
            s.source.outputAudioMixerGroup = s.output;
        }
    }

    public void Play(string name, bool isaffectbytimescale = true)
    {
        Sound s = Array.Find(gameSounds, sound => sound.name == name);
        StartCoroutine( AudioPlay(s.source, isaffectbytimescale) );
    }
    private IEnumerator AudioPlay(AudioSource source, bool isaffectbytimescale)
    {
        source.Play();
        if (!isaffectbytimescale) yield break;
        while (source.isPlaying)
        {
            source.pitch = Time.timeScale;
            yield return null;
        }
    }
    public void Stop(string name)
    {
        Sound s = Array.Find(gameSounds, sound => sound.name == name);
        s.source.Stop();
    }
    public void FadeOutStop(string name) 
    {
        Sound s = Array.Find(gameSounds, sound => sound.name == name);
        StartCoroutine(fadeouvolume(s.source));
    }
    private IEnumerator fadeouvolume(AudioSource source)
    {
        while(source.volume > 0)
        {
            source.volume -= 0.002f;

            yield return null;
        }
        source.Stop();
        source.volume = 1;
        yield break;
    }
}
