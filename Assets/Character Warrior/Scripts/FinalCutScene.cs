﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FinalCutScene : MonoBehaviour
{
    [SerializeField] Transform FinalTarget;
    TheEndTriggler theEndTriggler;
    SSceneManager sSceneManager;
    CameraFollow cameraFollow;
    FinalCutScene finalCutScene;
    PlayerMove playerMove;
    Vector3 vec;

    private void Start()
    {
        theEndTriggler = FindObjectOfType<TheEndTriggler>();
        cameraFollow = GetComponent<CameraFollow>();
        finalCutScene = GetComponent<FinalCutScene>();
        playerMove = FindObjectOfType<PlayerMove>();
        sSceneManager = FindObjectOfType<SSceneManager>();
    }
    void Update()
    {
        if (theEndTriggler.isFinal == true)
        {
            playerMove.StopMoving();
        }
    }
    private void StartFinalCutScene()
    {
        StartCoroutine(FinalCut());
    }

    private IEnumerator FinalCut()
    {
        yield return new WaitForSecondsRealtime(1f);
        cameraFollow.enabled = false;

        Vector3 nowVec;
        Quaternion nowQua;

        while (true)
        {
            float dis = Vector3.Distance(transform.position, FinalTarget.position);
            if (dis <= 0.1) break;
     
            nowVec = Vector3.SmoothDamp(transform.position, FinalTarget.position, ref vec, 120f * Time.deltaTime);
            nowQua = Quaternion.Slerp(transform.rotation, FinalTarget.rotation, 0.5f * Time.deltaTime);
            transform.position = nowVec;
            transform.rotation = nowQua;
            yield return null;
        }
        yield return new WaitForSecondsRealtime(0.3f);
        NPCinteraction.Instance.OnSituationTalk("艾莉絲!", 1.7f);

        yield return new WaitForSecondsRealtime(1.8f);
        NPCinteraction.Instance.OnSituationTalk("妳怎麼了?", 1.7f);

        yield return new WaitForSecondsRealtime(1.8f);
        NPCinteraction.Instance.OnSituationTalk("艾莉絲!!", 1.7f);
        yield return new WaitForSecondsRealtime(1.8f);

        sSceneManager.FadeToScene("TheEnd");
        finalCutScene.enabled = false;
    }
}