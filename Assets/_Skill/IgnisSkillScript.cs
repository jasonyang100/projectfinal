﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IgnisSkillScript : MonoBehaviour
{
    private Collider col;
    private GameObject Player;

    private void Awake()
    {
        col = GetComponent<Collider>();
        Player = GameObject.Find("Character Warrior");
    }

    private void GenerateGroundFire()
    {
        if(Physics.Raycast(col.bounds.center, -Vector3.up, out RaycastHit hit, 1 << 8))
        {
            GameObject GroundFireGo = Instantiate(EffectManager.Instance.Debris);
            GroundFireGo.transform.position = hit.point;
            Destroy(GroundFireGo, 5);
        }
    }
    private void OnEnable()
    {
        GenerateGroundFire();
    }

    // Start is called before the first frame update
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Mob")
        {
            Player.SendMessage("AttackOnHit", new object[2] { other, gameObject.tag });
            //GameObject go = Instantiate(EffectManager.Instance.GetBurning);
            //go.transform.parent = other.transform;
            //go.transform.position = other.bounds.center;
            //go.tag = "Burning";
            //Destroy(go, 5f);
        }
    }
}