﻿using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine;
using System.Threading.Tasks;

public class AuxiliaryTool : Singleton<AuxiliaryTool>
{
    public IEnumerator CoroutineWaiter(float second, Action action = null)
    {
        yield return new WaitForSeconds(second);

        action?.Invoke();
    }
    public async Task AsyncWaiter(float second, Action action = null)
    {
        await Task.Delay(TimeSpan.FromSeconds(second * Time.timeScale));
        if (!Application.isPlaying) return;
        action?.Invoke();
    }
    public void ResetAnimatorTriggers(Animator animator, params string[] parameters)
    {
        foreach (var p in parameters)
        {
            animator.ResetTrigger(p);
        }
    }

}
