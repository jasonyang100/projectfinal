﻿using System;
using System.Collections;
using System.Collections.Generic;

public sealed class TimerManager
{
    private float settime = 0f;
    private float triggertime = 0f;
    private float dt = 0f;
    private Action action;
    private bool isSet = false;

    private void SetTimer(float dt, float triggertime, Action action, bool isset)
    {
        this.dt = dt;
        this.settime = 0f;
        this.triggertime = triggertime;
        this.action = action;
        this.isSet = isset;
    }
    public bool ClockTimer(float dt, float triggertime, Action action)
    {
        if (!isSet) SetTimer(dt, triggertime, action, true);
        if(settime < triggertime) settime += dt;
        else
        {
            action();
            SetTimer(0f, 0f, null, false);
            return true;
        }
        return false;
    }
}
