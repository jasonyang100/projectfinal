﻿using EnumsManager;
using System;
using System.Collections.Generic;
using System.Collections;
using System.Reflection;
using System.Linq;
using UnityEngine;

public class TrollController : MonoBehaviour, IGameCharacterController, IAttackableCharacter
{
    private Type controller;
    private Mob troll;
    private MobAI trollai;
    private List<Collider> weaponcollider = new List<Collider>();

    private TrollAttackForm attackForm = TrollAttackForm.JumpAttack;
    private Dictionary<TrollAttackForm, float> dict_attackRadius;
    private Dictionary<TrollAttackForm, Func<Action<float>, float, IEnumerator>> dict_attackDamage;

    private delegate MobState stateAction();

    private Dictionary<MobState, stateAction> stateActionDict;

    private Dictionary<MobState, Action> AnimActionDict;

    private MobAlertBehaviour alertbehaviour = MobAlertBehaviour.STAND;
    private Dictionary<MobAlertBehaviour, Action> dict_alertbehaviour;

    private Animator animator;
    private AnimatorStateInfo currentAnimState;

    private GameObject TargetSystemGO;
    private GetDieing GetDieingComponent;
    private List<GameObject> BurnigGos = new List<GameObject>();

    private int _underattackcounter = 0;
    private int underAttackCounter
    { 
        get => _underattackcounter; 
        set
        { 
            if (value > 6) value = 6;
            if (value < 0) value = 0;
            _underattackcounter = value;
        } 
    }
    private float _animSpeedParam = 0f;
    public float animSpeedParam
    {
        get
        {
            switch (trollai.currentState)
            {
                case MobState.IDLE:
                case MobState.ALERT:
                case MobState.DEFENSE:
                    _animSpeedParam = 0f;
                    break;           
            }
            return _animSpeedParam;
        }
        set
        {
            switch (trollai.currentState)
            {               
                case MobState.WANDER:
                    value = Mathf.Clamp(value, 0.1f, 1f);
                    break;
                case MobState.CHASE:
                case MobState.RETURN:
                    value = Mathf.Clamp(value, 0.2f, 2f);
                    break;
            }
            _animSpeedParam = value;
        }
    }
    private float IdleTimer = 0f;
    private float AlertTimer = 0f;
    private float DefenseTimer = 0f;

    private void Awake()
    {
        controller = this.GetType();
        dict_attackRadius = new Dictionary<TrollAttackForm, float>
        {
            { TrollAttackForm.JumpAttack, 10f},
            { TrollAttackForm.Smash, 4.3f},
            { TrollAttackForm.DoubleStrike, 4.5f}
        };

        dict_attackDamage = new Dictionary<TrollAttackForm, Func<Action<float>, float, IEnumerator>>
        {
            { TrollAttackForm.JumpAttack, JumpAttack },
            { TrollAttackForm.Smash,  Smash  },
            { TrollAttackForm.DoubleStrike, DoubleStrike  }
        };

        dict_alertbehaviour = new Dictionary<MobAlertBehaviour, Action>
        {
            {MobAlertBehaviour.STAND, AlertStand },
            {MobAlertBehaviour.MOVEAROUND, AlertMoveAround},
            //{MobAlertBehaviour.MOVETO, },
            {MobAlertBehaviour.THREATEN, AlertThreaten},
        };

        animator = GetComponent<Animator>();

        troll = new Mob(gameObject);
        troll.Ccollider = GetComponent<Collider>();
        troll.InitialHpMana(8f, 0f);
        troll.characterName = CharacterName.Troll;
        troll.gravity = 3f;
        troll.Ccontroller = GetComponent<CharacterController>();
        troll.moveSpeedDict = new Dictionary<MobState, float>()
        {
            { MobState.WANDER, 2.6f },
            { MobState.CHASE, 8f },
            { MobState.RETURN, 10f },
        };
        troll.m_strDefenseSoundEffect = "16_Player_SwordHitBody2";
        troll.m_strUnderAttackSoundEffect = "16_Player_SwordHitBody2";

        trollai = new MobAI(troll);
        trollai.opposingGoTag = new HashSet<string> { "Player" };
        trollai.mobLocomotionType = MobNormalStateType.Wander;
        trollai.initialPosition = troll.CgameObject.transform.position;
        trollai.idleRestTime = 1.5f;
        trollai.wanderRadius = 8f;
        trollai.SightRangeAngle = 100f;
        trollai.attackRadius = dict_attackRadius[attackForm];
        trollai.returnRadius = 60f;
        trollai.alertRadius = 15f;

        trollai.maxAttackTimeSpan = 2.3f;
        trollai.minAttackTimeSpan = 0.2f;
        trollai.attackTimeSpan = 1.6f;

        trollai.TurnOnCompaniesAviodance = true;

        stateActionDict = new Dictionary<MobState, stateAction>()
        {
            { MobState.IDLE ,trollai.Idle },
            { MobState.WANDER ,trollai.Wander },
            { MobState.ALERT ,trollai.Alert },
            { MobState.CHASE ,trollai.Chase },
            { MobState.FIGHT ,trollai.Fight },
            { MobState.DEFENSE , trollai.Defense },
            { MobState.DAMAGED ,trollai.Damaged },
            { MobState.RETURN ,trollai.Return },
            { MobState.DIE ,trollai.Die }
        };
        AnimActionDict = InitializeAnimActionDict();
        TargetSystemGO = GameObject.Find("EnemySearchSphere");
        GetDieingComponent = transform.GetComponent<GetDieing>();

    }
    // Start is called before the first frame update
    void Start()
    {
    }
    // Update is called once per frame
    void Update()
    {
        if (!troll.Ccontroller.isGrounded) troll.Ccontroller.Move(new Vector3(0f, -troll.gravity * 2f ,0f)*Time.deltaTime);
        currentAnimState = animator.GetCurrentAnimatorStateInfo(0);
        AnimActionDict[trollai.currentState]();
        MobState nextState = stateActionDict[trollai.currentState]();
        trollai.lastState = trollai.currentState;
        if (trollai.cutinState == MobState.NOTSTATE) trollai.currentState = nextState;
        else
        {
            trollai.currentState = trollai.cutinState;
            trollai.cutinState = MobState.NOTSTATE;
        }
        //Debug.Log(gameObject.name+trollai.currentState);
    }
    private IEnumerator JumpAttack(Action<float> attact, float reduction)
    {
        float originaldamage = 1f;
        float damage = Mathf.Clamp(0f, originaldamage*(1 - reduction), originaldamage);
        attact(damage);
        yield break;
    }
    private IEnumerator Smash(Action<float> attact, float reduction )
    {
        float originaldamage = 1f;
        float damage = Mathf.Clamp(0f, originaldamage*(1 - reduction), originaldamage);
        attact(damage);
        yield break;
    }
    private IEnumerator DoubleStrike(Action<float> attact, float reduction )
    {
        float originaldamage = 1f;
        float damage = Mathf.Clamp(0f, originaldamage*(1 - reduction), originaldamage);
        attact(damage);
        yield break;
    }
    private void AlertStand()
    {
        if (trollai.currentState != MobState.ALERT) return;

        if(AlertTimer == 0)
        {
            animator.SetBool("Alert", true);
            AlertTimer += 0.0001f; //加一個微小時間避免animator偵測狀態卡住
        }

        if (AlertTimer >= trollai.attackTimeSpan) 
        {
            trollai.isActionDone = true;

            animator.SetBool("Alert", false);
        }
    }
    private void AlertMoveAround()
    {
        if (trollai.currentState != MobState.ALERT) return;

        if (AlertTimer == 0)
        {
            animator.SetBool("Alert", true);
            AlertTimer += 0.001f; //加一個微小時間避免animator偵測狀態卡住
        }

        if (currentAnimState.IsName("AlertMode") && !animator.IsInTransition(0))
        {
            Vector3 V3move = Vector3.Cross((trollai.aimingGo.transform.position - transform.position).normalized, Vector3.up);
            troll.Ccontroller.Move(V3move * 0.9f * Time.deltaTime);
        }

        if (AlertTimer >= trollai.attackTimeSpan)
        {
            trollai.isActionDone = true;
            animator.SetBool("Alert", false);
        }
    }
    private void AlertThreaten()
    {
        if (trollai.currentState != MobState.ALERT) return;
        if (AlertTimer == 0)
        {
            animator.SetBool("Alert", true);
            animator.SetTrigger("Threaten");
            StartCoroutine(AuxiliaryTool.Instance.CoroutineWaiter(0.5f, () => AudioManager.instance.Play("32_Troll_Scream")));
            AlertTimer += 0.001f; //加一個微小時間避免animator偵測狀態卡住
        }
        if (AlertTimer >= trollai.attackTimeSpan)
        {
            trollai.isActionDone = true;
            animator.SetBool("Alert", false);
        }
    }
    private Dictionary<MobState, Action> InitializeAnimActionDict()
    {
        return new Dictionary<MobState, Action>
        {
            { MobState.IDLE, () => //Idle
                {
                    if(trollai.lastState != trollai.currentState)
                    {
                        animator.SetBool("Alert", false);
                        animator.SetBool("Defense", false);
                        animator.SetFloat("Speed", animSpeedParam);
                        IdleTimer = 0f;
                    }
                    if(currentAnimState.IsName("Base.Locomotion") && !animator.IsInTransition(0))
                    {
                        IdleTimer += Time.deltaTime;
                        if(IdleTimer >= trollai.idleRestTime)
                        {
                            animator.SetTrigger("Lookaround");
                            IdleTimer = 0f;
                        }
                    }
                }
            },
            { MobState.WANDER, () => //Wander
                {
                    if(trollai.lastState != trollai.currentState)
                        AuxiliaryTool.Instance.ResetAnimatorTriggers(animator,"Lookaround");

                    animSpeedParam += 0.05f;
                    animator.SetFloat("Speed", animSpeedParam);
                }
            },
            { MobState.ALERT, () => //Alert
                {
                    if(trollai.lastState != trollai.currentState)
                    {
                        AlertTimer = 0f;
                        animator.SetFloat("Speed", animSpeedParam);
                        AuxiliaryTool.Instance.ResetAnimatorTriggers(animator,"Lookaround","getHit");
                        if(trollai.aimingGo != null)
                        {
                            StartCoroutine(AuxiliaryTool.Instance.CoroutineWaiter(0.6f, trollai.CallCompanies));
                            SelectAlertForms();
                            SelectAttackMoves();
                        }
                        else alertbehaviour = MobAlertBehaviour.STAND;
                    }
                    dict_alertbehaviour[alertbehaviour]();

                    if (currentAnimState.IsName("AlertMode") && !animator.IsInTransition(0)) 
                        AlertTimer += Time.deltaTime;
                    
                    //Debug.Log(alertbehaviour);
                }
            },
            { MobState.CHASE, () => //Chase
                {
                    if(trollai.lastState != trollai.currentState)
                    {
                         AuxiliaryTool.Instance.ResetAnimatorTriggers(animator,"Threaten","Lookaround","getHit");
                    }
                    animator.SetBool("Alert", false);
                    animator.SetBool("Defense", false);
                    animSpeedParam += 0.2f;
                    animator.SetFloat("Speed", animSpeedParam);
                }
            },
            { MobState.FIGHT, () => //Fight
                {
                    if(trollai.lastState != trollai.currentState)
                    {
                         trollai.attackTimeSpan = trollai.maxAttackTimeSpan;
                         AuxiliaryTool.Instance.ResetAnimatorTriggers(animator,"Threaten","Lookaround","getHit");
                    }
                    if(!currentAnimState.IsTag("Attack") && !animator.IsInTransition(0))
                        animator.SetTrigger(attackForm.ToString());
                }
            },
            { MobState.DEFENSE, () => //Defense
                {
                    if(trollai.lastState != trollai.currentState)
                    {
                        DefenseTimer = 0f;
                        AuxiliaryTool.Instance.ResetAnimatorTriggers(animator,"Threaten","getHit");
                        animator.SetBool("Alert",false);
                        animator.SetFloat("Speed", animSpeedParam);
                        EnableWeapon(false);
                        trollai.attackTimeSpan -= 0.6f;
                        animator.SetBool("Defense", true);
                    }
                    if (currentAnimState.IsName("Defense") && !animator.IsInTransition(0))
                    {
                        DefenseTimer += Time.deltaTime;

                        if(DefenseTimer >= 2.5f)
                        {
                            trollai.isActionDone = true;
                            animator.SetBool("Defense", false);
                        }
                    }
                }
            },
            { MobState.DAMAGED, () => //Damaged
                {
                    if(trollai.lastState != trollai.currentState)
                    {
                        AuxiliaryTool.Instance.ResetAnimatorTriggers(animator,"Threaten","Lookaround");
                        animator.SetBool("Alert",false);
                        animator.SetBool("Defense",false);
                        animator.SetTrigger("getHit");
                        EnableWeapon(false);
                        //AudioManager.instance.Play("22_Player_MagicShield");
                    }
                }
            },
            { MobState.RETURN, () => //Return
                {
                    animator.SetBool("Alert",false);
                    animator.SetBool("Defense",false);
                    AuxiliaryTool.Instance.ResetAnimatorTriggers(animator,"Threaten","Lookaround","getHit");
                    animSpeedParam += 0.1f;
                    animator.SetFloat("Speed", animSpeedParam);
                }
            },
            { MobState.DIE, () => //Die
                {
                    if(trollai.lastState != trollai.currentState)
                    {
                        EnableWeapon(false);
                        AuxiliaryTool.Instance.ResetAnimatorTriggers(animator,"Threaten","Lookaround","getHit");
                       // if(!currentAnimState.IsTag("Die") && !animator.IsInTransition(0))
                        animator.SetBool("Alert",false);
                        animator.SetBool("Defense",false);
                        animator.SetBool("isDead",true);
                        TargetSystemGO.SendMessage("RemoveMob", troll);
                        removeBurning(1f);
                        AudioManager.instance.Play("35_Troll_Dead");
                        StopAllCoroutines();
                        StartCoroutine( AuxiliaryTool.Instance.CoroutineWaiter(1f,() => GetDieingComponent.enabled = true));
                        troll.Ccollider.enabled = false;
                       
                        troll.Ccontroller.enabled = false;
                        this.enabled = false;
                    }
                }
            },
        };
    }
    private void OnControllerColliderHit(ControllerColliderHit hit)
    {
        if (hit.gameObject.layer == 8)
        {
            float dy = Mathf.Abs(troll.Ccollider.bounds.center.y - hit.point.y);
            if (dy > (troll.Ccollider.bounds.center.y / 4)) return;
            else trollai.isHitWall = true;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
    }
    private void UnderAttack(object[] obj) 
    {
        Vector3 HitDir = (obj[0] as Transform).position;
        Func<Action<float>, float, IEnumerator> HitDel = obj[1] as Func<Action<float>, float, IEnumerator>;
        float reduction = 0f;

        if (trollai.currentState != MobState.DEFENSE)
        {
            trollai.cutinState = MobState.DAMAGED;
            if (underAttackCounter == 0)
            {
                underAttackCounter++;
            }
            else
            {
                int diceNo = UnityEngine.Random.Range(underAttackCounter, 7);

                if (diceNo == underAttackCounter)
                {
                    trollai.cutinState = MobState.DEFENSE;
                    underAttackCounter = 0;
                }
                else underAttackCounter += 2;
            }
        }
        else reduction = 0.8f;

        StartCoroutine( HitDel( damage => troll.GetHit( damage ), reduction) );
        trollai.GetHitDir = HitDir - transform.position;
        //Debug.Log(troll.currentHP);
    }
    private void OnAnimationAttackStart() => EnableWeapon(true);

    private void OnAnimationAttackEnd() => EnableWeapon(false);

    private void OnAnimationDone(string state)
    {
        if (state != trollai.currentState.ToString()) return;
        trollai.isActionDone = true;
    }
    private void OnHitEffect(Collider col)
    {
        string HitSoundEffect = col.GetComponent<IAttackableCharacter>().UnderAttackSoundEffect();
        AudioManager.instance.Play(HitSoundEffect);
    }
    private void AttackOnHit(object[] obj)
    {
        Collider col = obj[0] as Collider;
        string Hittag = obj[1] as string;

        if (!trollai.opposingGoTag.Contains(col.tag)) return;
        if(Hittag == "Weapon")
        {
            col.SendMessage("UnderAttack", new object[2] { transform, dict_attackDamage[attackForm] });
            OnHitEffect(col);
            EnableWeapon(false);
        }
    }
    private void SelectAlertForms()
    {
        if (trollai.aimingGo == null) return;

        if (trollai.lastState == MobState.IDLE || trollai.lastState == MobState.WANDER)
        {
            alertbehaviour = MobAlertBehaviour.THREATEN;
            return;
        }

        int rnd = UnityEngine.Random.Range(0, 7);

        switch (rnd)
        {
            case 0:
                alertbehaviour = MobAlertBehaviour.THREATEN;
                break;
            case 1:
                alertbehaviour = MobAlertBehaviour.STAND;
                break;
            default:
                alertbehaviour = MobAlertBehaviour.MOVEAROUND;
                break;
        }
    }
    private void SelectAttackMoves()
    {
        if (trollai.aimingGo == null) return;

        float distance = Vector3.Distance(transform.position, trollai.aimingGo.transform.position);

        if (distance >= 8f) attackForm = (TrollAttackForm)(UnityEngine.Random.Range(0, 7) % 3);
        else attackForm = (TrollAttackForm)UnityEngine.Random.Range(1, 3);

        trollai.attackRadius = dict_attackRadius[attackForm];
    }
    private void AnimationStartJump()
    {
        //AudioManager.instance.Play("33_Troll_Jump");
        StartCoroutine(JumpAttackMove());
    }
       
    private IEnumerator JumpAttackMove()
    {
        for(int i = 0; i < 32; i++)
        {           
            troll.Ccontroller.Move(transform.forward * 20f * Time.deltaTime);
            yield return null;
        }
    }
    public void CallCharacterRegistrar(object[] obj)
    {
        GameCharacter gc = obj[0] as GameCharacter;
        bool bo = (bool)obj[1];
        if (bo) troll.Register(gc);
        else troll.Unregister(gc);
    }
    private void AddWeaponGameObject(Collider col) => weaponcollider.Add(col);
    private void EnableWeapon(bool bo)
    {
        foreach (Collider col in weaponcollider)
        {
            col.enabled = bo;
        }
    }
    private void RetriveCompanyAlert(object obj)
    {
        StartCoroutine(AuxiliaryTool.Instance.CoroutineWaiter(0.5f, () =>
        {
            if (trollai.aimingGo != null) return;
            if (troll.isDead) return;

            trollai.aimingGo = obj as GameObject;
            trollai.aimingGo.SendMessage("CallCharacterRegistrar", new object[2] { troll, true });
            Vector3 aimingGodir = trollai.aimingGo.transform.position - transform.position;
            Vector3 lookatdir = new Vector3(aimingGodir.x, 0f, aimingGodir.z).normalized;
            for(int i = 0; i < 3; i++)
            {
                transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(lookatdir), 0.33f);
            }
            trollai.cutinState = MobState.ALERT;
            AuxiliaryTool.Instance.ResetAnimatorTriggers(animator, "Lookaround");
        }));
    }
    private void OnOpposingGoDie(GameObject go)
    {
        StartCoroutine( AuxiliaryTool.Instance.CoroutineWaiter(0.5f,
            () =>
            {
                if (trollai.aimingGo == go) trollai.aimingGo = null;
                trollai.cutinState = MobState.IDLE;
                animator.SetBool("Alert", false);
                animator.SetBool("Defense", false);
                AuxiliaryTool.Instance.ResetAnimatorTriggers(animator, "Threaten", "Lookaround", "getHit");
                animator.SetTrigger("Lookaround");
            }));
    }
    private void GetBurning()
    {
        GameObject go = Instantiate(EffectManager.Instance.GetBurning);
        BurnigGos.Add(go);
        go.transform.parent = transform;
        go.transform.position = troll.Ccollider.bounds.center;

        StartCoroutine(AuxiliaryTool.Instance.CoroutineWaiter(5f, ()=> 
        {
            if (BurnigGos.Contains(go))
            {
                BurnigGos.Remove(go);
                Destroy(go);
            }
        }));
    }
    private void removeBurning(float destorytime = 0f)
    {
        if (BurnigGos.Count > 0)
        {
            foreach (var go in BurnigGos)
            {
                if (go != null) Destroy(go, destorytime);
            }
            BurnigGos.Clear();
        }
    }
    public GameCharacter ReturnGameCharacter()
    {
        return troll;
    }

    public string UnderAttackSoundEffect()
    {
        if (trollai.currentState == MobState.DEFENSE) return troll.m_strDefenseSoundEffect;
        else return troll.m_strUnderAttackSoundEffect;
    }
}