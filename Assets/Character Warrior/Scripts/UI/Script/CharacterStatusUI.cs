﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CharacterStatusUI : MonoBehaviour
{
    public static CharacterStatusUI Instance;
    public Image HpBar;
    public Image HpBar2;
    public float NowHpf;
    private float _MaxHp;
    public float MaxHpf {
        get
        {
            return _MaxHp;
        }
        set
        {
            _MaxHp = value >= 1 ? value: 1 ;
        }
    }

    public Image ManaBar;
    public float NowManaf;
    public float _MaxManaf;
    public float MaxManaf
    {
        get
        {
            return _MaxManaf;
        }
        set
        {
            _MaxManaf = value >= 1 ? value : 1;
        }
    }
    public Image BloodScreen;
    //Text HpText;
    //Text ManaText;

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        BloodScreen = transform.Find("BloodScreen").GetComponent<Image>();

        Color tempColor = BloodScreen.color;
        tempColor.a = 0f;
        BloodScreen.color = tempColor;

    }
    // Start is called before the first frame update
    void Start()
    {
        //HpText =HpBar.GetComponentInChildren<Text>();

        //ManaText = ManaBar.GetComponentInChildren<Text>();

        //setBarState(100f,10f);// set hp = 100, mana =10  and setup
    }

    // Update is called once per frame
    void Update()
    {
        //NowHpf -= Time.deltaTime;
        //NowManaf -= Time.deltaTime * 0.5f;
        //updateBars();
    }
   //-----  外部呼叫函式 -----///
    public void SetNowHp(float hp)
    {
        NowHpf = hp;
        updateBars();
        GameManager.g_instance.PlayerCurrentHp = hp;
    }
    public void SetNowMana(float Mana)
    {
        NowManaf = Mana;
        updateBars();
    }
    public void InitialHpMana(float maxHp, float maxMana)
    {
        setBarState(maxHp, maxMana);

        GameManager.g_instance.PlayerMaxHp = maxHp;

        if(GameManager.g_instance.isGameStart) GameManager.g_instance.PlayerCurrentHp = maxHp;
    }
    //-------------------------///

    void updateBars()
    {
        updateHpBarGague();
        updateManaBarGague();
    }
    float updateHpBarGague()
    {
        //updateBarNumber(HpText, NowHpf, MaxHpf);

        float p = (NowHpf / MaxHpf);
        HpBar.fillAmount = p;
        HpBar2.fillAmount = p;
        return p;
    }

    float updateManaBarGague()
    {
        //updateBarNumber(ManaText, NowManaf, MaxManaf);

        float p = (NowManaf / MaxManaf);// [1.0,0.5] remove if the container is changed
        ManaBar.fillAmount = p;
        return p;
    }
    //void updateBarNumber(Text in_text,float in_now,float in_Max)
    //{
    //    in_now = (in_now < 0) ? 0 : in_now;//ignore real nowhp/mana number
    //    in_text.text = $"{in_now:f1}/{in_Max}";
    //}

    void setBarState(float _maxHp, float _maxMana)
    {
        MaxHpf = _maxHp;
        MaxManaf = _maxMana;
        recoverBarGagues();
        updateBars();
    }

    void recoverBarGagues()
    {
        recoverHpBarGague();
        recoverManaBarGague();
    }

    void recoverHpBarGague()
    {
        HpBar.fillAmount = 1;
        HpBar2.fillAmount = 1;
        NowHpf = MaxHpf;
    }
    void recoverManaBarGague()
    {
        ManaBar.fillAmount = 1;
        NowManaf = MaxManaf; 
    }

}