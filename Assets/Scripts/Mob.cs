﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mob : NonPlayer
{
    public string m_strUnderAttackSoundEffect;
    public string m_strDefenseSoundEffect;

    public Mob(GameObject go) : base(go)
    {
        npcType = EnumsManager.NPCType.Mob;
    }
}
