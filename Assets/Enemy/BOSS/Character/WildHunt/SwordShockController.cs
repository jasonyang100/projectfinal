﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwordShockController : MonoBehaviour
{
    private GameObject WildHunt;
    private float _speed = 7f;
    private float _accerlation = 0.4f;
    private float Speed { get => _speed += _accerlation; }

    private void Awake()
    {
        WildHunt = GameObject.Find("WildHunt");
        Destroy(gameObject, 3.5f);
    }
    // Update is called once per frame
    void Update()
    {
        transform.position += transform.forward * Time.deltaTime * Speed;
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag != "Player") return;
        else
        {
            WildHunt.SendMessage("AttackOnHit", new object[2] { other, gameObject.tag });

            Destroy(gameObject);
        }
    }
}
