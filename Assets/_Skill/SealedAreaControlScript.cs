﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SealedAreaControlScript : MonoBehaviour
{
    private Renderer rend;
    private Collider col;
    private MeshRenderer meshrend;
    private void Awake()
    {
        rend = GetComponent<Renderer>();
        col = GetComponent<Collider>();
        meshrend = GetComponent<MeshRenderer>();
    }
    private void Start()
    {
        meshrend.enabled = false;
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            Vector3 playerDir = other.transform.position - transform.position;

            if(Mathf.Sign(Vector3.Dot(playerDir, transform.forward)) > 0)
            {
                col.isTrigger = false;
                meshrend.enabled = true;
                StartCoroutine(AuxiliaryTool.Instance.CoroutineWaiter(0.5f, () => rend.material.SetInt("_IsActive", 0)));
            }
        }
    }
}