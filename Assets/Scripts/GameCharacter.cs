﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using EnumsManager;
using System;

public abstract class GameCharacter
{
    public CharacterName characterName;

    public CharacterAI characterAI;
    public GameObject CgameObject { set; get; }
    public Transform Ctransform { set; get; }
    public Collider Ccollider { set; get; }
    public float MaxHP {private set; get; }
    public float currentHP { protected set; get; }
    public float Mana { set; get; }
    public float baseAD { set; get; }
    public float baseArmor { set; get; }
    public float gravity { set; get; }

    public CharacterController Ccontroller;

    public HashSet<GameCharacter> fightObservers = new HashSet<GameCharacter>();

    public Dictionary<MobState,float> moveSpeedDict;

    public float ManaRecoveryTimeSpan = 0.1f; 

    public Vector3 moveVector(MobState state)
    {
        if (!moveSpeedDict.ContainsKey(state)) return Vector3.zero;
        return CgameObject.transform.forward * moveSpeedDict[state];
    }
    public virtual void InitialHpMana(float hp, float Mana)
    {
        MaxHP = hp;
        currentHP = hp;
        this.Mana = Mana;
    }
    public void RestoreHP(float hp)
    {
        if (hp >= MaxHP) currentHP = MaxHP;
        else currentHP = hp;
    }
    public bool isDead 
    {
        get 
        {
            if (currentHP <= 0) return true;
            return false;                
        } 
    }
    public GameCharacter(GameObject go)
    {
        CgameObject = go;
        this.Ctransform = go.transform;
    }
    public class Equipment
    {
        public EquipmentType equipmentType;
        public float value;
        public float durability;
        public bool isActive;
    }
    protected List<Equipment> weaponList;

    protected List<Equipment> armorList;

    public Equipment GetActiveWeapon()
    {
        foreach(var w in weaponList)
        {
            if (w.isActive == true) return w;
        }
        return null;
    }
    public void Register(GameCharacter character)
    {
        fightObservers.Add(character);
    }
    public void Unregister(GameCharacter character)
    {
        fightObservers.Remove(character);
    }
    public void ObserverDirectDamage(float damage, GameObject go)
    {
        foreach (var obs in fightObservers)
        {
            if (obs.CgameObject != go) continue;
            obs.GetHit(damage);
        }
    }
    public virtual void GetHit(float damage)
    {
        currentHP -= damage;
    }
}