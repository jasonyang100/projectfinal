﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyShaderController : MonoBehaviour
{
    SkinnedMeshRenderer shaderController;
    PlayerMove playerMove;
    GameObject player;
    public float power;
    public float edge;
    public float delayTime = 2f;

    // Start is called before the first frame update
    void Start()
    {
        if (GameObject.Find("Character Warrior"))
            player = GameObject.Find("Character Warrior");

        playerMove = player.GetComponent<PlayerMove>();
        playerMove = GetComponent<PlayerMove>();
        shaderController = GetComponent<SkinnedMeshRenderer>();
        power = 10f;
        edge = 10f;
    }

    // Update is called once per frame
    void Update()
    {
        StartCoroutine(ShaderController());
    }
    IEnumerator ShaderController()
    {
        shaderController.material.SetFloat("Vector1_Power", power);
        shaderController.material.SetFloat("Vector1_Edge", edge);

        if (Input.GetKey(KeyCode.E) && playerMove.attackMode == false)
        {
            power -= 0.05f;
            if (power <= 5)
                power = 5;
            edge -= 0.1f;
            if (edge <= 0.1f)
                edge = 0.1f;
        }
        else
        {
            yield return new WaitForSecondsRealtime(delayTime);
            power += 0.02f;
            if (power >= 10) 
                power = 10;
            edge += 0.02f;
            if (edge >= 10f)
                edge = 10f;
        }
    }
}
