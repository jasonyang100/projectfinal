﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RamproomScript : MonoBehaviour
{
    public Transform Player;
    public Transform RampRoom;

    public GameObject SealedGate1;
    public GameObject SealedGate2;
    public GameObject SealedGate3;
    public GameObject StairToNextRoom;
    public GameObject SightLimitter;
    public GameObject FakeFloor;
    public GameObject ArenaLightGroup;
    public Material MagicTile;
    public Material SightMasker;
    public float MagicTileRadius;
    public bool isArenaRoomClear;

    public Vector3 PlayerPostion;
    bool IsMagicTileActive;
    bool IsSightLimitterActive;
    bool[] SealedGatesIsOpened;



    //GameObject[] TorchLightsGroup;
    [SerializeField]GameObject TorchLightsGroup;



    // Start is called before the first frame update
    void Start()
    {
        // MagicTile = Resources.Load<Material>("MagicTile_Material");
        SightLimitterAttach();
        StairToNextRoom.SetActive(false);
        TorchGroupSwitch(false);
    }


// Update is called once per frame
void Update()
    {
        StepFloorPhase();
        ArenaPhase();
        MagicTile.SetVector("_PlayerPositon", Player.position);

    }

    


    void StepFloorPhase()
    {
        if (Player.position.z > SealedGate2.transform.position.z + 2&& Player.position.z< SealedGate3.transform.position.z)
        {
            if(Player.position.z > SealedGate2.transform.position.z + 10)
            {
                MagicTileEffectOn();
                SetSightMaskerColor();
                SetFakeFloor(false);
            }
            SetSpookyTileColor();
            SightLimitterAttach();
            
        }
        else
        {
            SetOriginTileColor();
            MagicTileEffectOff();
            SightLimitterDettach();
            

        }
    }


    void ArenaPhase()
    {
        if (Player.position.z > SealedGate3.transform.position.z +4)
        {
            MagicTileEffectOff();
            SightLimitterDettach();
            TorchGroupSwitch(true);
        }
        ArenaRoomClear();



    }


    void SightLimitterAttach()
    {
        if(SightLimitter.transform.parent!=Player)
        {
            SightLimitter.SetActive(true);
            SightLimitter.transform.position = Player.position;
            SightLimitter.transform.parent = Player;
        }
        
       

    }
    void SightLimitterDettach()
    {
        ResetSightMaskerColor();
        SightLimitter.transform.parent = null;
        SightLimitter.SetActive(false);
    }


    void MagicTileEffectOn()
    {
        MagicTile.SetInt("IsActive",1);
        //if (MagicTile.GetFloat("_Radius") -13f>0.1f)
        //{
        //    MagicTile.SetFloat("_Radius", Mathf.Lerp(MagicTile.GetFloat("_Radius"), 13f, 1.2f * Time.deltaTime));
        //}


    }

    void MagicTileEffectOff()
    {
        MagicTile.SetInt("IsActive", 0);
        MagicTile.SetFloat("_Radius",50f);
    }

    void SetFakeFloor(bool b)
    {
        FakeFloor.SetActive(b);
    }



    void RaiseStairToNextRoom()
    {
        // StairToNextRoom.transform.localPosition = Vector3.Lerp(StairToNextRoom.transform.localPosition,new Vector3(StairToNextRoom.transform.localPosition.x, StairToNextRoom.transform.parent.position.y, StairToNextRoom.transform.localPosition.z),0.1f);
        StairToNextRoom.SetActive(true);
    }

    void ArenaRoomClear()
    {
        
            isArenaRoomClear = true;
        

        if (isArenaRoomClear)
        {
            RaiseStairToNextRoom();
        }
    }

    void SetSpookyTileColor()
    {
        //MagicTile.SetColor("_BaseBlendColor", new Color(0.11f,0.21f,0.7f));// water blue
        MagicTile.SetColor("_BaseBlendColor", Color.Lerp(MagicTile.GetColor("_BaseBlendColor"), new Color(0.11f, 0.21f, 0.7f), 0.05f*Time.deltaTime)) ;


    }

    void SetOriginTileColor()
    {
        MagicTile.SetColor("_BaseBlendColor", Color.black);
    }

    void SetSightMaskerColor()
    {
        SightMasker.SetColor("_BaseColor", Color.Lerp(SightMasker.GetColor("_BaseColor"), new Color(0.7f, 0.7f, 0.7f, 1f), 0.3f * Time.deltaTime));
        SightMasker.SetColor("_EmissionColor", Color.Lerp(SightMasker.GetColor("_EmissionColor"),new Color(0.2f,0.4f,0.7f), 0.3f * Time.deltaTime));
        //SightMasker.SetColor("_BaseColor", new Color(0f, 0f, 0f, 1f));
        //SightMasker.SetFloat("EmissiveValue", 1);
    }

    void ResetSightMaskerColor()
    {
        SightMasker.SetColor("_BaseColor", new Color(0f,0f,0f,0f));
        SightMasker.SetColor("_EmissionColor",new Color(0f,0f,0f));
    }

    void TorchGroupSwitch(bool _switch)
    {
        TorchLightsGroup.SetActive(_switch);

    }

    IEnumerator ShirnkFloorRaius()//not used
    {
        while (MagicTile.GetFloat("_Radius") - 3f > 0.1f)
        {
            MagicTile.SetFloat("_Radius", Mathf.Lerp(MagicTile.GetFloat("_Radius"), 3f, 0.5f * Time.deltaTime));
        }

        yield return null;
    }
}
