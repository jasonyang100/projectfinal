﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : GameCharacter
{
    public string m_strUnderAttackSoundEffect;
    public string m_strShieldSoundEffect;

    private float _shieldreduction;
    public float ShieldReduction 
    {
        set => _shieldreduction = Mathf.Clamp(value, 0f, 100f);
        get => _shieldreduction;
    }

    public Player(GameObject go) : base(go)
    {
        base.characterName = EnumsManager.CharacterName.Player;
    }
    public override void InitialHpMana(float hp, float Mana)
    {
        base.InitialHpMana(hp, Mana);

        CharacterStatusUI.Instance.InitialHpMana(base.MaxHP, base.Mana);
    }
    public void UpdateCurrentHp(float hp)
    {
        currentHP = hp;
        CharacterStatusUI.Instance.SetNowHp(currentHP);
    }
    public override void GetHit(float damage)
    {
        base.GetHit(damage);
        CharacterStatusUI.Instance.SetNowHp(currentHP);
    }
}
