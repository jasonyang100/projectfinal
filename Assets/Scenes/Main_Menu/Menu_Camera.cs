﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;

public class Menu_Camera : MonoBehaviour
{
    Volume bloom;
    // Start is called before the first frame update
    void Start()
    {
        bloom = GetComponent<Volume>();
        bloom.weight = 1.0f;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
