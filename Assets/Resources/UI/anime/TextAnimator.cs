﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class TextAnimator : MonoBehaviour
, IPointerExitHandler
, IEventSystemHandler
, IPointerEnterHandler
{
    Animator animator;
    public bool IsSelected;    
    // Start is called before the first frame update
    void Start()
    {
        animator = GetComponent<Animator>();
        animator.SetBool("IsSelected", false);
    }

    // Update is called once per frame
    void Update()
    {
        
        setbool(IsSelected);
        
    }
    void setbool(bool b)
    {
        if (b)
        {
            animator.SetBool("IsSelected", true);
        }
        else
        {
            animator.SetBool("IsSelected", false);
        }
               
    }


    public void OnPointerEnter(PointerEventData eventData)
    {
        
        IsSelected = true;
    }
    public void OnPointerExit(PointerEventData eventData)
    {
        
        IsSelected = !true; 
    }
}
