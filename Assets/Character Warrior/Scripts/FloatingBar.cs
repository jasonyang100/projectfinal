﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using EnumsManager;

public class FloatingBar : MonoBehaviour
{
    TargetingSystem targetingSystem;
    Camera cam;
    GameCharacter target;
    [SerializeField] Image Container;
    [SerializeField] Image HpBar;
    [SerializeField] Image image;
    public float currentHp;
    private float maxHp;
    public bool isDisplaytalkicon = false;

    void Start()
    {
        targetingSystem = FindObjectOfType<TargetingSystem>();

        Container = transform.Find("Container").GetComponent<Image>();
        HpBar = transform.Find("Container").Find("HpBar").GetComponent<Image>();
        image = transform.Find("Container").Find("HpBar").Find("Image").GetComponent<Image>();

        Container.enabled = false;
        HpBar.enabled = false;
        image.enabled = false;
        cam = Camera.main;
    }
    void LateUpdate()
    {
        DisplaySelectedMobHpBar();
    }
    public void InitialHpBar(float maxHp, float currentHp)
    {
        this.maxHp = maxHp;
        this.currentHp = currentHp;
        UpdateHpBar();
    }

    float UpdateHpBar()
    {
        float p = currentHp / maxHp;
        HpBar.fillAmount = p;
        return p;
    }
    private void DisplaySelectedMobHpBar()
    {
        if (targetingSystem.GetNowMode() != TargetingMode.Fight)
        {
            Container.enabled = false;
            HpBar.enabled = false;
            image.enabled = false;
        }
        else
        {
            Container.enabled = true;
            HpBar.enabled = true;
            image.enabled = true;
            target = targetingSystem.selectedMobTargetCharacter();
            if (target == null) return;
            (maxHp, currentHp) = targetingSystem.SetMobHpToFlotingBar();
            UpdateHpBar();
            Vector3 pos = cam.WorldToScreenPoint(target.Ctransform.position + new Vector3(0, target.Ccontroller.height, 0));
            Container.transform.position = pos;
        }
    }
}