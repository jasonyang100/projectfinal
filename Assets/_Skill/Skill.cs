﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Skill 

{




    public KeyCode m_keycode;//49~57 = 1~9
    public bool IsActive;
    public bool IsRecastable;
    public GameObject TragetGameObject;
    public GameObject MeGameObject;
    public GameObject SkillCarrierGameObject;
    
    



    public float InputDelayTime = 5;
    [SerializeField] float _InputDelayTime = 0;
    public float DeployTime = 10;
    [SerializeField] float _DeployTime = 10;

    // Start is called before the first frame update
    void Start()
    {
        
        //if (m_keycode == null)
        //{
        //    m_keycode = KeyCode.Alpha9;// have to search a list of unused key
        //}

        
        IsActive = false;
        SkillCarrierGameObject.SetActive(false);

        for (int i = 0; i <= MeGameObject.transform.childCount; i++)
        {
            if (SkillCarrierGameObject.transform.GetChild(i).gameObject.tag == "Skill")
            {
                SkillCarrierGameObject = SkillCarrierGameObject.transform.GetChild(i).gameObject;
                break;
            }
               
        }
        SkillCarrierGameObject.SetActive(false);




    }

    // Update is called once per frame
    void Update()
    {
        //Clock += Time.deltaTime;
        
        deploySkill(triggered());

    }

    public bool triggered()
    {
        if (_InputDelayTime <= 0)
        {
            if (Input.GetKeyDown(m_keycode))
            {
                if (!IsRecastable)
                {
                    IsActive = !IsActive;
                }
                else if(IsRecastable)
                {
                    _DeployTime = DeployTime;
                    IsActive = true;
                    
                }
                
                _InputDelayTime = InputDelayTime;
            }
            
        }
        _InputDelayTime -= Time.deltaTime;
        return IsActive;
    }


    public void deploySkill(bool _isActive)
    {
        if (_isActive)
        {

            _DeployTime -= Time.deltaTime;
            if (_DeployTime < 0)
            {
                IsActive = !_isActive;
                
            }
        }
        else _DeployTime = DeployTime;
        SkillCarrierGameObject.SetActive(IsActive);

    }
    public void setTarget()
    {
        // select a target to do something
    }

    







}
