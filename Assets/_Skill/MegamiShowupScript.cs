﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MegamiShowupScript : MonoBehaviour
{
    [SerializeField] Material m_targetMaterial;

    public bool IsActive;
    public float MaxValue=5;
    float initialValue=-1f;
    public float speed;
    public float Timer=5;
    float _Timer;

    public GameObject Go1;
    public GameObject Go2;

    [SerializeField] float i=0;

    public bool EnableToShow { get; private set; } = false;


    // Start is called before the first frame update
    void Start()
    {
       
        m_targetMaterial=Resources.Load<Material>("Material/HoloGrapgh");
        //initialValue = m_targetMaterial.GetFloat("_MaskHeight");
        RestMaskHeitght();
        speed = MaxValue -initialValue / Timer;


    }
    private void Update()
    {
        if (!EnableToShow) return;
        if (Input.GetKeyDown(KeyCode.C))
        {
            AerithShow();
            SendMessage("ActivateInteraction");
        }
    }
    // Update is called once per frame
    void FixedUpdate()
    {
        if (IsActive)
        {
            m_targetMaterial.SetFloat("_MaskHeight", i);
        }
       
    }

    void Actived(bool isactive)
    {
        if (isactive)
        {
            IncreaseMaskHeight();
        }
    }

    void IncreaseMaskHeight()
    {
        if (m_targetMaterial != null)
        {
            float value = m_targetMaterial.GetFloat("_MaskHeight");
            m_targetMaterial.SetFloat("_MaskHeight", value+speed*Time.deltaTime);
        }
    }
    
    void RestMaskHeitght()
    {
        m_targetMaterial.SetFloat("_MaskHeight", initialValue);
    }


    

    private void OnEnable()
    {
        RestMaskHeitght();
        IsActive = true;
    }

    private void OnDisable()
    {
        IsActive = false;
    }

    public void ChangeMaskHeight(int i)
    {
        m_targetMaterial.SetFloat("_MaskHeight", i);
    }
    private void AerithShow()
    {
        if (EnableToShow)
        {
            Go1.SetActive(true);
            Go2.SetActive(true);
        }

    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player")) EnableToShow = true;
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("Player")) EnableToShow = false;
    }
}
