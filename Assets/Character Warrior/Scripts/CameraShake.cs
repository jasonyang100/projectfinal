﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraShake : MonoBehaviour
{
    private float shakeTimeRemaining, shakePower,shakeFadeTime;
    private void LateUpdate()
    {
        if (shakeTimeRemaining > 0)
        {
            shakeTimeRemaining -= Time.deltaTime;
            float xAmout = Random.Range(- 1f, 1f) * shakePower;
            float yAmout = Random.Range(- 1f,  1f) * shakePower;

            transform.position += new Vector3(xAmout, yAmout, 0f);

            shakePower = Mathf.MoveTowards(shakePower, 0f, shakeFadeTime * Time.deltaTime);
        }
    }
    public void StartShake(object[] obj)
    {
        float length = (float)obj[0];
        float power = (float)obj[1];
        //yield return new WaitForSeconds(delayTime);
        shakeTimeRemaining = length;
        shakePower = power;

        shakeFadeTime = power / length;
    }

    //public IEnumerator StartShake(float delayTime, float length, float power)
    //{
    //    yield return new WaitForSeconds(delayTime);
    //    shakeTimeRemaining = length;
    //    shakePower = power;

    //    shakeFadeTime = power / length;
    //}
}
